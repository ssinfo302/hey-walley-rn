import React from 'react';
import { View, StatusBar, PermissionsAndroid } from 'react-native';
import Route from './src/navigation/routes';
// import { routeName } from './src/containers/App';
import { MenuProvider } from 'react-native-popup-menu';
import { Provider } from 'react-redux';
import store from './src/store/index';
import { isAndroid } from './src/helper/responsiveScreen';
import './src/api';


// create our root router app
export default class App extends React.Component {
  componentDidMount() {
    // SplashScreen.hide();
    if (isAndroid) {
      this.requestStoragePermission();
    }
  }

  async requestStoragePermission() {
    try {
      const granted = await PermissionsAndroid.requestMultiple(
        [ PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        PermissionsAndroid.PERMISSIONS.RECORD_AUDIO ],
        {
          title: 'HeyWallet Permission',
          message:
            'HeyWallet App needs access to your storage and record audio',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      console.log('granted', granted, PermissionsAndroid.RESULTS.GRANTED)
    } catch (err) {
      console.warn(err);
    }
  }

  render() {
    return (
      <MenuProvider>
        <Provider store={store}>
          <View style={{ flex: 1 }}>
            <StatusBar
              barStyle={'light-content'}
            />
            <Route />
          </View>
        </Provider>
      </MenuProvider>
    );
  }

}

console.disableYellowBox = true;
