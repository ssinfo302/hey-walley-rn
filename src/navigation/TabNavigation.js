import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Home, { routeName as homeRouteName } from '../containers/Home'
import Search, { routeName as searchRouteName } from '../containers/Search'
import Wallet, { routeName as walletRouteName } from '../containers/Wallet'
import TabBar from '../components/TabBar'

export const routeName = 'tabNavigation';

const Tab = createBottomTabNavigator();

class TabNavigation extends Component {

  render() {
    return (
      <Tab.Navigator
        initialRouteName={homeRouteName} tabBar={(props) => <TabBar {...props} />}>
        <Tab.Screen
          name={homeRouteName}
          component={Home}
          initialParams={{ tabTitle: this.props.route.params && this.props.route.params.tabTitle }}
        />
        <Tab.Screen
          name={searchRouteName}
          component={Search}
        />
        <Tab.Screen
          name={walletRouteName}
          component={Wallet}
        />
      </Tab.Navigator>
    );
  }
}

export default TabNavigation;
