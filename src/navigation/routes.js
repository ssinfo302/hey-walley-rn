import React, { Component } from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Splash, { routeName as splashRouteName } from '../containers/Splash';
import Welcome, { routeName as welcomeRouteName } from '../containers/Welcome';
import TabNavigation, { routeName as tabNavigationRouteName } from './TabNavigation'
import ChatDetail, { routeName as chatDetailRouteName } from '../containers/ChatDetail';

const Stack = createStackNavigator();

export class routes extends Component {

  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator screenOptions={{ headerShown: false }}>
          <Stack.Screen name={splashRouteName} component={Splash} />
          <Stack.Screen name={welcomeRouteName} component={Welcome} />
          <Stack.Screen name={tabNavigationRouteName} component={TabNavigation} />
          <Stack.Screen name={chatDetailRouteName} component={ChatDetail} />
        </Stack.Navigator>
      </NavigationContainer>
    )
  }
}

// get the navigator passing the initial route name
export default routes;