import axios from 'axios';
import { isAndroid } from '../helper/responsiveScreen';

export function add_wallet(data = {}, config = {}) {
  return {
    request: () => axios.post('addWallet', data),
    cancel: (msg) => source.cancel(msg),
  };
}

export function check_wallet(data = {}, config = {}) {
  return {
    request: () => axios.post('checkWallet', data),
    cancel: (msg) => source.cancel(msg),
  };
}

export function search_wallet(data = {}, config = {}) {
  console.log('data.........', 'search?searchString=' + data.search)

  const headers = {
    'Authorization': `Bearear ${data.token}`
  }
  return {
    request: () => axios.get('wallet/search?searchString=' + data.search, { headers }),
    cancel: (msg) => source.cancel(msg),
  };
}

export function remove_wallet(data = {}, config = {}) {
  return {
    request: () => axios.post('wallet/remove', data),
    cancel: (msg) => source.cancel(msg),
  };
}

export function search_dapp(data = {}, config = {}) {
  const headers = {
    'Authorization': `Bearear ${data.token}`
  }
  return {
    request: () => axios.get('dapp/search?searchString=' + data.search, { headers }),
    cancel: (msg) => source.cancel(msg),
  };
}

export function chat_thread(data = {}, config = {}) {
  const headers = {
    'Authorization': `Bearear ${data}`
  }

  console.log('header', headers)

  return {
    request: () => axios.get('getthread', { headers }),
    cancel: (msg) => source.cancel(msg),
  };
}

export function get_chat(data = {}, config = {}) {
  const headers = {
    'Authorization': `Bearear ${data.token}`
  }

  return {
    request: () => axios.get('getchat?threadId=' + data.threadId, { headers }),
    cancel: (msg) => source.cancel(msg),
  };
}

export function add_labels(data = {}, config = {}) {
  const headers = {
    'Authorization': `Bearear ${data.token}`
  }

  const payload = {
    text: data.text,
    threadId: data.threadId
  }

  return {
    request: () => axios.post('addlabels', payload, { headers }),
    cancel: (msg) => source.cancel(msg),
  };
}

export function remove_labels(data = {}, config = {}) {
  const headers = {
    'Authorization': `Bearear ${data.token}`
  }

  const payload = {
    text: data.text,
    threadId: data.threadId
  }

  return {
    request: () => axios.post('removelabels', payload, { headers }),
    cancel: (msg) => source.cancel(msg),
  };
}

export function add_notes(data = {}, config = {}) {
  const headers = {
    'Authorization': `Bearear ${data.token}`
  }

  const payload = {
    text: data.text,
    threadId: data.threadId
  }

  return {
    request: () => axios.post('addnotes', payload, { headers }),
    cancel: (msg) => source.cancel(msg),
  };
}

export function remove_chat(data = {}, config = {}) {
  const headers = {
    'Authorization': `Bearear ${data.token}`
  }

  const payload = {
    threadId: data.threadId
  }

  return {
    request: () => axios.post('/removechat', payload, { headers }),
    cancel: (msg) => source.cancel(msg),
  };
}

export function remove_message(data = {}, config = {}) {
  const headers = {
    'Authorization': `Bearear ${data.token}`
  }

  const payload = {
    messageId: data.messageId
  }

  return {
    request: () => axios.post('/removemessage', payload, { headers }),
    cancel: (msg) => source.cancel(msg),
  };
}

export function edit_message(data = {}, config = {}) {
  const headers = {
    'Authorization': `Bearear ${data.token}`
  }

  const payload = {
    messageId: data.messageId,
    text: data.text
  }

  return {
    request: () => axios.post('/editmessage', payload, { headers }),
    cancel: (msg) => source.cancel(msg),
  };
}

export function image_send(data = {}) {

  const headers = {
    'Authorization': `Bearear ${data.token}`,
    'Content-Type': 'multipart/form-data'
  }

  const formData = new FormData();
  formData.append('photo', {
    uri: data.uri,
    name: data.name,
    type: data.type,
  });
  formData.append('address', data.address);
  console.log("data>>>>>>>", JSON.stringify(formData), data.token)
  return {
    request: () => axios.post('https://heywallet.axislabs.company/media/api/v1/chat/images', formData, { headers }),
    // .then(response => response)
    // .catch(err => {
    //   // handle err
    //   console.log('errres...', err)
    // }),
    cancel: (msg) => source.cancel(msg),
  };
}

export function audio_send(data = {}) {

  const headers = {
    'Authorization': `Bearear ${data.token}`,
    'Content-Type': 'multipart/form-data'
  }

  const formData = new FormData();
  formData.append('audio', {
    uri: data.audioFile,
    name: isAndroid ? 'audio.mp3' : 'audio.m4a',
    type: isAndroid ? 'audio/mpeg' : 'audio/x-m4a',
  });
  formData.append('address', data.address);
  formData.append('totalTime', data.recordTime);
  console.log("data>>>>>>>", JSON.stringify(formData))
  return {
    request: () => axios.post('https://heywallet.axislabs.company/media/api/v1/chat/audio', formData, { headers }),
    cancel: (msg) => source.cancel(msg),
  };
}

export function file_send(data = {}) {

  const headers = {
    'Authorization': `Bearear ${data.token}`,
    'Content-Type': 'multipart/form-data'
  }

  const formData = new FormData();
  formData.append('file', {
    uri: data.uri,
    name: data.name,
    type: data.type,
  });
  formData.append('address', data.address);
  formData.append('fileSize', data.fileSize);
  console.log("data>>>>>>>", JSON.stringify(formData))
  return {
    request: () => axios.post('https://heywallet.axislabs.company/media/api/v1/chat/file', formData, { headers }),
    cancel: (msg) => source.cancel(msg),
  };
}

export function get_media(data = {}, config = {}) {

  const headers = {
    'Authorization': `Bearear ${data.token}`
  }
  return {
    request: () => axios.get('getmedia?threadId=' + data.threadId, { headers }),
    cancel: (msg) => source.cancel(msg),
  };
}

export function get_file(data = {}, config = {}) {

  const headers = {
    'Authorization': `Bearear ${data.token}`
  }
  return {
    request: () => axios.get('getfile?threadId=' + data.threadId, { headers }),
    cancel: (msg) => source.cancel(msg),
  };
}

export function get_audio(data = {}, config = {}) {

  const headers = {
    'Authorization': `Bearear ${data.token}`
  }
  return {
    request: () => axios.get('getaudio?threadId=' + data.threadId, { headers }),
    cancel: (msg) => source.cancel(msg),
  };
}

export function get_links(data = {}, config = {}) {

  const headers = {
    'Authorization': `Bearear ${data.token}`
  }
  return {
    request: () => axios.get('getlinks?threadId=' + data.threadId, { headers }),
    cancel: (msg) => source.cancel(msg),
  };
}

export function subscribe_dapp(data = {}, config = {}) {
  const headers = {
    'Authorization': `Bearear ${data.token}`
  }

  const payload = {
    dappId: data.dappId,
    type: data.type
  }

  return {
    request: () => axios.post('dapp/subscribe', payload, { headers }),
    cancel: (msg) => source.cancel(msg),
  };
}

export function dapp_list(data = {}, config = {}) {

  const headers = {
    'Authorization': `Bearear ${data}`
  }
  return {
    request: () => axios.get('dapp/list', { headers }),
    cancel: (msg) => source.cancel(msg),
  };
}