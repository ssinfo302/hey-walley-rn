import axios from 'axios';

let serverBaseUrl;
let hostUrl;

// Dev Server
if (__DEV__) {
  hostUrl = 'https://heywallet.axislabs.company/api/v1/';
  serverBaseUrl = hostUrl;
} else {
  // Production Server
  hostUrl = 'https://heywallet.axislabs.company/api/v1/';
  serverBaseUrl = hostUrl;
}


export const SERVER_BASE_URL = serverBaseUrl;
export const HOST_URL = hostUrl;

const axiosUrlRetriesCount = 5;
const axiosUrlRetries = {};
const axiosRetry = (error) => {
  if (!error) return;
  const config = error.config || {};
  const message = typeof error.data === 'string' ? error.data : error.message || config.message || '';
  // check for error in axios where it can't connect to server for sometime
  if (message.search(/Network Error|The network connection was lost|The request timed out|connect|abort/i) > -1) {
    const url = config.url;
    if (!url) return;
    if (!axiosUrlRetries[ url ]) axiosUrlRetries[ url ] = 0;
    axiosUrlRetries[ url ]++;
    if (axiosUrlRetries[ url ] <= axiosUrlRetriesCount) return axios.request(config);
  }
};


const loggerAxios = axios.create({
  baseURL: serverBaseUrl,
  // headers: { api_key: CREDENTIALS.API_KEY },
});

// base url to be used across the axios api call
axios.defaults.baseURL = SERVER_BASE_URL;

// headers
// axios.defaults.headers[ 'Content-Type' ] = 'application/json';
// axios.defaults.headers[ 'api_key' ] = CREDENTIALS.API_KEY;

// loggerAxios.interceptors.request.use((config) => {
//   return config;
// });

loggerAxios.interceptors.response.use((response) => {
  return response.data;
}, (error) => {
  return Promise.reject(error);
});

export default loggerAxios;