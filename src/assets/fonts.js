import { Platform } from 'react-native';

const isIOS = Platform.OS === 'ios';

/** Define all your fonts here */
export default {
  sfprotext: 'Archivo-Regular',
  [ 'archivo-regular' ]: 'Archivo-Regular',
};
