const colors = {
  transparent: 'transparent',
  white: '#ffffff',
  black: '#000000',
  gray: '#C4C4C4',
  [ 'gray-dark' ]: '#121212',
  [ 'gray-medium' ]: '#1A1A1E',
  [ 'gray-light' ]: '#222222',
  blue: '#368EFF',
  [ 'blue-dark' ]: '#204FC6',
  [ 'blue-light' ]: '#1C2438',
  text: '#919399',
  [ 'text-dark' ]: '#2C2D2E',
  [ 'text-gray' ]: '#646464',
  [ 'text-light' ]: '#919399',
  border: '#EC8F22',
  [ 'border-light' ]: '#838590',
  [ 'border-dark' ]: '#25252B',
  red: '#F34D36'
};


export default colors;
