module.exports = {
  blockChainData: [
    {
      image: require('../assets/images/eth_image.png'),
      title: `ETH`,
      subTitle: 'Etherum',
      wallet: [{walletAddress:'ab33423ffc8836dcab33423f'}, {walletAddress:'ab33423ffc8836dcab33423f'}]
    },
    {
      image: require('../assets/images/btc_image.png'),
      title: `BTC`,
      subTitle: 'Bitcoin',
      wallet: [{walletAddress:'ab33423ffc8836dcab33423f'}]
    },
    {
      image: require('../assets/images/bsc_image.png'),
      title: `BSC`,
      subTitle: 'BSC',
      wallet: [{walletAddress:'ab33423ffc8836dcab33423f'}, {walletAddress:'ab33423ffc8836dcab33423f'}, {walletAddress:'ab33423ffc8836dcab33423f'}]
    },
    {
      image: require('../assets/images/dot_image.png'),
      title: `POLKADOT`,
      subTitle: 'Polkadot',
      wallet: [{walletAddress:'ab33423ffc8836dcab33423f'}, {walletAddress:'ab33423ffc8836dcab33423f'}]
    },
    {
      image: require('../assets/images/solana_icon.png'),
      title: `SOLANA`,
      subTitle: 'Solana',
      wallet: []
    },
  ],

  chatListData: [
    {
      image: require('../assets/images/btc_image.png'),
      title: `bc13123ffc883900`,
      label: [{ lableName: 'Alfred' }, { lableName: 'Work' }, { lableName: 'Club' }],
      time: '3:50 PM',
      message: 'Hi there!',
      count: 2,
      countStatus: true
    },
    {
      image: require('../assets/images/bsc_image.png'),
      title: `aab7474ffcze8380`,
      label: [{ lableName: 'Work' }, { lableName: 'Family' }],
      time: '3:50 PM',
      message: 'Hi there!',
      count: 2,
      countStatus: false
    },
    {
      image: require('../assets/images/dot_image.png'),
      title: `aab7474ffcze8380`,
      label: [{ lableName: 'Work' }, { lableName: 'Family' }],
      time: '3:50 PM',
      message: 'Hi there!',
      count: 2,
      countStatus: false
    },
    {
      image: require('../assets/images/eth_image.png'),
      title: `aab7474ffcze8380`,
      label: [{ lableName: 'Work' }, { lableName: 'Family' }],
      time: '3:50 PM',
      message: 'Hi there!',
      count: 2,
      countStatus: false
    },
    {
      image: require('../assets/images/btc_image.png'),
      title: `bc13123ffc883900`,
      label: [{ lableName: 'Alfred' }, { lableName: 'Work' }, { lableName: 'Club' }],
      time: '3:50 PM',
      message: 'Hi there!',
      count: 2,
      countStatus: false
    },
    {
      image: require('../assets/images/bsc_image.png'),
      title: `aab7474ffcze8380`,
      label: [{ lableName: 'Work' }, { lableName: 'Family' }],
      time: '3:50 PM',
      message: 'Hi there!',
      count: 2,
      countStatus: false
    },
    {
      image: require('../assets/images/dot_image.png'),
      title: `aab7474ffcze8380`,
      label: [{ lableName: 'Work' }, { lableName: 'Family' }],
      time: '3:50 PM',
      message: 'Hi there!',
      count: 2,
      countStatus: false
    },
    {
      image: require('../assets/images/eth_image.png'),
      title: `aab7474ffcze8380`,
      label: [{ lableName: 'Work' }, { lableName: 'Family' }],
      time: '3:50 PM',
      message: 'Hi there!',
      count: 2,
      countStatus: false
    },
    {
      image: require('../assets/images/btc_image.png'),
      title: `bc13123ffc883900`,
      label: [{ lableName: 'Alfred' }, { lableName: 'Work' }, { lableName: 'Club' }],
      time: '3:50 PM',
      message: 'Hi there!',
      count: 2,
      countStatus: true
    },
    {
      image: require('../assets/images/bsc_image.png'),
      title: `aab7474ffcze8380`,
      label: [{ lableName: 'Work' }, { lableName: 'Family' }],
      time: '3:50 PM',
      message: 'Hi there!',
      count: 2,
      countStatus: false
    },
    {
      image: require('../assets/images/dot_image.png'),
      title: `aab7474ffcze8380`,
      label: [{ lableName: 'Work' }, { lableName: 'Family' }],
      time: '3:50 PM',
      message: 'Hi there!',
      count: 2,
      countStatus: false
    },
    {
      image: require('../assets/images/eth_image.png'),
      title: `aab7474ffcze8380`,
      label: [{ lableName: 'Work' }, { lableName: 'Family' }],
      time: '3:50 PM',
      message: 'Hi there!',
      count: 2,
      countStatus: false
    },
  ],

  searchListData: {
    users: [{ image: require('../assets/images/bsc_image.png'), title: 'wob7474ffcze8380' }],
    labels: [{ image: require('../assets/images/bsc_image.png'), title: 'wob7474ffcze8380' }, { image: require('../assets/images/bsc_image.png'), title: 'wob7474ffcze8380' }],
    messages: []
  },

  messageData: [
    {
      message: 'Hi there! 😀',
      time: `Yesterday, 3:50 PM`,
      isSender: false
    },
    {
      message: 'Hi there! ✋',
      time: `Yesterday, 3:50 PM`,
      isSender: true
    },
    {
      message: 'Hi there! 😀',
      time: `Yesterday, 3:50 PM`,
      isSender: false
    },
    {
      message: 'Hi there! ✋',
      time: `Yesterday, 3:50 PM`,
      isSender: true
    },
    {
      message: 'Hi there! 😀',
      time: `Yesterday, 3:50 PM`,
      isSender: false
    },
    {
      message: 'Hi there! ✋',
      time: `Yesterday, 3:50 PM`,
      isSender: true
    },
    {
      message: 'Hi there! ✋',
      time: `Yesterday, 3:50 PM`,
      isSender: true
    },
  ],

};
