


export function addWalletToParams(props, state) {
  const {
    walletAddress,
    blockChain,
  } = state;
  return {
    address: walletAddress,
    chain: blockChain == 'SOLANA' ? 'SOL' : blockChain == 'POLKADOT' ? 'DOT' : blockChain
  };
}


// export function checkWalletToParams(props, state) {
//   const {
//     walletAddress,
//     blockChain,
//     uid
//   } = state;
//   return {
//     address: '0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2',
//     chain: blockChain,
//     uid: '373163f1-d617-4cc1-972a-da02b8e49aac'
//   };
// }

export function checkWalletToParams(props, state) {
  const {
    walletAddress,
    blockChain,
    uid
  } = state;
  return {
    address: walletAddress,
    chain: blockChain,
    uid: uid
  };
}

export function addLabelsToParams(props, state) {
  const {
    id,
    token,
    value
  } = state;
  return {
    text: value,
    threadId: id,
    token: token
  };
}

export function removeLabelsToParams(props, state) {
  const {
    id,
    token,
    value
  } = state;
  return {
    text: value,
    threadId: id,
    token: token
  };
}

export function addNotesToParams(props, state) {
  const {
    id,
    token,
    notes
  } = state;
  return {
    text: notes,
    threadId: id,
    token: token
  };
}

export function removeChatToParams(props, state) {
  const {
    id,
    token,
  } = state;
  return {
    threadId: id,
    token: token
  };
}

export function removeMessageToParams(props, state) {
  const {
    id,
    token,
  } = state;
  return {
    messageId: id,
    token: token
  };
}

export function editMessageToParams(props, state) {
  const {
    id,
    value,
    token,
  } = state;
  return {
    messageId: id,
    text: value,
    token: token
  };
}

export function imageSendToParams(props, state) {
  const {
    uri,
    name,
    type,
    token,
    address
  } = state;
  return {
    uri: uri,
    name: name,
    type: type,
    token: token,
    address: address
  };
}

export function audioSendToParams(props, state) {
  const {
    audioFile,
    token,
    address,
    recordTime
  } = state;
  return {
    audioFile: audioFile,
    token: token,
    address: address,
    recordTime: recordTime
  };
}

export function fileSendToParams(props, state) {
  const {
    uri,
    name,
    type,
    token,
    address,
    fileSize
  } = state;
  return {
    uri: uri,
    name: name,
    type: type,
    token: token,
    address: address,
    fileSize: fileSize
  };
}

export function getMediaToParams(props, state) {
  const {
    id,
    token,
  } = state;
  return {
    threadId: id,
    token: token,
  };
}