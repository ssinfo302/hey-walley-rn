import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, Image, TouchableOpacity, ActivityIndicator } from 'react-native';
import FontText from '../FontText';
import { wp, hp, normalize } from '../../helper/responsiveScreen';
import colors from '../../assets/colors';
import { time } from '../../util/validation';
import { remove_chat } from '../../api/wallet';

var id
const DappListItem = (props) => {
  const {
    data,
    onItemPress,
    walletId,
    processing,
    onDappsPress
  } = props;

  let status = false
  if (data.subscribeDapp.length != 0) {
    let exitData = data.subscribeDapp.find(x => x === walletId)
    if (exitData) {
      status = true
    } else {
      status = false
    }
  }

  console.log('id', id)
  return (
    <View style={styles.container} >
      <View style={styles.childContainer}>
        {data?.projectLogo ?
          <Image source={{ uri: data?.projectLogo }} style={styles.imageView} resizeMode="contain" />
          :
          <Image source={require('../../assets/images/dapps_placeholder.png')} style={styles.imageView} resizeMode="contain" />
        }
        <FontText size={normalize(14)} name={'archivo-regular'} color="white" style={{ width: wp(40), marginLeft: wp(3) }} >
          {data?.projectName ? data?.projectName : data.address}</FontText>
      </View>

      <TouchableOpacity
        onPress={() => {
          onItemPress(data, status)
          id = data._id
        }}
        style={{
          backgroundColor: status ? colors[ 'blue-light' ] : colors[ 'blue-dark' ],
          borderWidth: status ? 1 : 0,
          ...styles.subscribeView
        }}>
        {(processing && id == data._id) ?
          <ActivityIndicator
            animating={processing} size="small" color='#ffffff' />
          :
          <FontText size={normalize(14)} name={'archivo-regular'} color="white"  >{status ? 'Unsubscribe' : 'Subscribe'}</FontText>
        }
      </TouchableOpacity>
    </View>
  );
};

DappListItem.propTypes = {
  data: PropTypes.object,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: wp(5),
    marginBottom: hp(2),
    paddingHorizontal: wp(4),
    paddingVertical: hp(1.5),
    backgroundColor: colors[ 'gray-medium' ],
  },
  childContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  imageView: {
    width: wp(10),
    height: wp(10)
  },
  subscribeView: {
    borderColor: colors[ 'blue-dark' ],
    width: wp(27),
    height: wp(10),
    paddingVertical: wp(2),
    borderRadius: wp(5),
    alignItems: 'center',
    justifyContent: 'center'
  }

});

export default DappListItem;