import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, FlatList, View } from 'react-native';
import DappListItem from './DappListItem';
import { wp, hp, normalize } from '../../helper/responsiveScreen';

const DappList = (props) => {
  const {
    dappListData,
    onItemPress,
    walletId,
    processing,
    onDappsPress
  } = props;


  _renderItem = ({ item, index }) => {
    return (
      <DappListItem
        data={item}
        walletId={walletId}
        processing={processing}
        onItemPress={(item, status) => onItemPress(item, status)}
        onDappsPress={(item) => onDappsPress(item)} />
    );
  };


  return (
    <FlatList
      keyboardShouldPersistTaps={'handled'}
      data={dappListData}
      renderItem={this._renderItem}
      showsVerticalScrollIndicator={false}
      removeClippedSubviews={false}
      style={styles.container}
      contentContainerStyle={{ paddingBottom: hp(10.5) }}
    />
  );
};

DappList.defaultProps = {
};

DappList.propTypes = {
  dappListData: PropTypes.array,
};

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

export default DappList;