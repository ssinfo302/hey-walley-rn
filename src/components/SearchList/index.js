import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, FlatList, View } from 'react-native';
import SearchListItem from './SearchListItem';
import { wp, hp, normalize } from '../../helper/responsiveScreen';

const SearchList = (props) => {
  const {
    searchListData,
    navigation,
    onItemPress,
    address,
    tabTitle
  } = props;


  _renderItem = ({ item, index }) => {
    return (
      <SearchListItem data={item} tabTitle={tabTitle} address={address} onItemPress={(item) => onItemPress(item)} navigation={navigation} />
    );
  };


  return (
    <FlatList
      bounces={false}
      data={searchListData}
      renderItem={this._renderItem}
      showsVerticalScrollIndicator={false}
      removeClippedSubviews={false}
      style={styles.container}
      contentContainerStyle={{ paddingBottom: hp(10.5) }}
    />
  );
};

SearchList.defaultProps = {
};

SearchList.propTypes = {
  searchListData: PropTypes.object,
  address: PropTypes.string
};

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

export default SearchList;