import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import FontText from '../FontText';
import { hp, isX, wp, normalize } from '../../helper/responsiveScreen';
import colors from '../../assets/colors';

class TabView extends Component {

    render() {
        const { title, count, titleStyle, onTabPress } = this.props;
        return (
            <TouchableOpacity style={styles.tabView}
                onPress={() => onTabPress()}>
                <FontText size={normalize(16)} name={'archivo-regular'} color="white" style={titleStyle} >{title}</FontText>
                <FontText size={normalize(12)} name={'archivo-regular'} color="white" style={styles.countText} >{count}</FontText>
            </TouchableOpacity>

        )
    }
}

const styles = StyleSheet.create({
    tabView: {
        flexDirection: 'row',
        width: wp(25),
        marginRight: wp(3),
        paddingVertical: hp(1),
    },
    countText: {
        fontWeight: '700',
        marginLeft: wp(1),
        alignSelf: 'flex-end',
        opacity: 0.3
    }
});


export default TabView;
