import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, ImageBackground, Image, TouchableOpacity } from 'react-native';
import FontText from '../FontText';
import { wp, hp, normalize } from '../../helper/responsiveScreen';
import colors from '../../assets/colors';

const SearchListItem = (props) => {
  const {
    data,
    navigation,
    onItemPress,
    address,
    tabTitle
  } = props;

  // if (data.chain == "ETH" || data.wallet.chain == 'ETH' || data.senderWallet.chain == 'ETH') {
  //   image = require('../../assets/images/eth_image.png')
  // } else if (data.chain == "BTC" || data.wallet.chain == 'BTC' || data.senderWallet.chain == 'BTC') {
  //   image = require('../../assets/images/btc_image.png')
  // } else if (data.chain == "BSC" || data.wallet.chain == 'BSC' || data.senderWallet.chain == 'BSC') {
  //   image = require('../../assets/images/bsc_image.png')
  // } else if (data.chain == "POLKADOT" || data.wallet.chain == 'POLKADOT' || data.senderWallet.chain == 'POLKADOT') {
  //   image = require('../../assets/images/dot_image.png')
  // }

  console.log('tabTitle', tabTitle)

  let walletAddress, image
  if (tabTitle == 'Users') {
    walletAddress = data.address
    if (data.chain == "ETH") {
      image = require('../../assets/images/eth_image.png')
    } else if (data.chain == "BTC") {
      image = require('../../assets/images/btc_image.png')
    } else if (data.chain == "BSC") {
      image = require('../../assets/images/bsc_image.png')
    } else if (data.chain == "DOT") {
      image = require('../../assets/images/dot_image.png')
    } else if (data.chain == "SOL") {
      image = require('../../assets/images/solana_icon.png')
    }
  } else if (tabTitle == 'Labels') {
    walletAddress = data.threadId
    if (data.wallet.chain == 'ETH') {
      image = require('../../assets/images/eth_image.png')
    } else if (data.wallet.chain == 'BTC') {
      image = require('../../assets/images/btc_image.png')
    } else if (data.wallet.chain == 'BSC') {
      image = require('../../assets/images/bsc_image.png')
    } else if (data.wallet.chain == 'DOT') {
      image = require('../../assets/images/dot_image.png')
    } else if (data.wallet.chain == "SOL") {
      image = require('../../assets/images/solana_icon.png')
    }
  } else {
    walletAddress = data.threadId
    if (data.senderWallet.chain == 'ETH') {
      image = require('../../assets/images/eth_image.png')
    } else if (data.senderWallet.chain == 'BTC') {
      image = require('../../assets/images/btc_image.png')
    } else if (data.senderWallet.chain == 'BSC') {
      image = require('../../assets/images/bsc_image.png')
    } else if (data.senderWallet.chain == 'DOT') {
      image = require('../../assets/images/dot_image.png')
    } else if (data.senderWallet.chain == "SOL") {
      image = require('../../assets/images/solana_icon.png')
    }
  }

  return (
    // address != data.address &&
    <TouchableOpacity style={styles.container}
      onPress={() => onItemPress(data)}>

      <View style={styles.childContainer}>
        <Image source={image} resizeMode="contain" />

        <View style={styles.title}>
          <FontText size={normalize(16)} name={'archivo-regular'} color="white" >{walletAddress}</FontText>
          {tabTitle == 'Labels' &&
            <View style={styles.lableView}>
              {data.label && data.label.map((item, index) =>
                <View key={index} style={styles.lableChildView}>
                  <FontText size={normalize(10)} name={'archivo-regular'} color={"white"} >{item.text}</FontText>
                </View>
              )}
            </View>
          }
          {tabTitle == 'Messages' &&
            <FontText size={normalize(16)} name={'archivo-regular'} color="white" >{data.text}</FontText>
          }
        </View>
      </View>

      <Image source={require('../../assets/images/right_arrow_icon.png')} resizeMode="contain" />

    </TouchableOpacity>
  );
};

SearchListItem.propTypes = {
  data: PropTypes.object
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: hp(1.5),
    paddingHorizontal: wp(2),
    paddingVertical: hp(0.5),
  },
  childContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  title: {
    width: wp(60),
    marginLeft: wp(3)
  },
  lableView: {
    flexWrap: 'wrap',
    flexDirection: 'row',
  },
  lableChildView: {
    marginRight: wp(2),
    paddingHorizontal: wp(3),
    paddingVertical: hp(0.5),
    borderRadius: wp(5),
    marginTop: hp(0.8),
    marginBottom: hp(0.5),
    backgroundColor: colors[ 'blue-dark' ]
    // marginBottom: hp(1),
  },
});

export default SearchListItem;