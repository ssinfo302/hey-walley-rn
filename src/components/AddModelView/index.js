import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, Image, TouchableOpacity, Modal, Keyboard } from 'react-native';
import FontText from '../FontText';
import Input from '../Input';
import { wp, hp, normalize, isAndroid, isX, isIOS } from '../../helper/responsiveScreen';
import colors from '../../assets/colors';
import SmartScrollView from '../../components/SmartScrollView';

class AddModelView extends Component {

  focus = () => this.input && this.input.focus();

  render() {
    const { title, placeholder, isAddLabel, onInputChange, value, onLableCancelPress, onAddPress, style } = this.props;

    return (
      <Modal
        transparent={true}
        animationType={'none'}
        visible={isAddLabel} >
        <SmartScrollView
          showsVerticalScrollIndicator={false}
          // style={[ styles.modelContainer, style ]}
          contentContainerStyle={[ styles.modelContainer, style ]}
          applyKeyboardCheck={isIOS ? true : false}
          disabled={false}
          alwaysBounceVertical={false} >

          <View style={{ ...styles.modelChildContainer, width: style ? wp(85) : null, marginTop: style ? hp(0) : hp(15), paddingVertical: hp(2.5), borderColor: colors[ 'border-light' ] }}>
            <FontText lines={1} size={normalize(16)} name={'archivo-regular'} color="white" style={{ fontWeight: 'bold' }} >{title}</FontText>
            <View style={styles.inputView}>
              <Input
                ref={r => (this.input = r)}
                placeholder={placeholder}
                defaultValue={value}
                keyboardType={'default'}
                returnKeyType={'done'}
                // blurOnSubmit={true}
                onChangeText={onInputChange('value')}
                fontSize={normalize(14)}
                inputStyle={[
                  styles.input,
                ]}
                onSubmitEditing={() => Keyboard.dismiss()}
                placeholderTextColor="white"
                color="white"
              />
            </View>

            <View style={styles.bottomView}>
              <TouchableOpacity onPress={() => onLableCancelPress()} style={{ ...styles.modelChildContainer, alignSelf: 'flex-start', paddingVertical: hp(1.2), borderColor: colors[ 'gray-medium' ] }}>
                <FontText size={normalize(14)} name={'archivo-regular'} color="text-light" style={{ fontWeight: '400' }} >{'Cancel'}</FontText>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => onAddPress()} style={{ ...styles.modelChildContainer, alignSelf: 'flex-start', paddingVertical: hp(1.2), borderColor: colors[ 'gray-medium' ] }}>
                <FontText size={normalize(14)} name={'archivo-regular'} color="text-light" style={{ fontWeight: '400' }} >{'Add'}</FontText>
              </TouchableOpacity>

            </View>
          </View>
        </SmartScrollView>
      </Modal>
    )
  }
};

const styles = StyleSheet.create({
  modelContainer: {
    flex: 1,
    paddingTop: isX ? hp(4) : hp(0),
    flexDirection: 'column',
    backgroundColor: 'rgba(52, 52, 52, 0.5)',
    paddingHorizontal: wp(8),
  },
  modelChildContainer: {
    paddingHorizontal: wp(5),
    borderRadius: wp(5),
    borderWidth: 1,
    backgroundColor: colors.black
  },
  inputView: {
    marginBottom: hp(2.5),
    marginTop: hp(1),
    borderRadius: wp(5),
    backgroundColor: colors[ 'gray-medium' ],
    paddingHorizontal: wp(5),
    borderRadius: wp(8),
    paddingVertical: hp(0.5)
  },
  input: {
    fontWeight: '400',
    backgroundColor: colors.transparent,
    paddingLeft: 0,
    paddingTop: 0
  },
  bottomView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  }

});

export default AddModelView;