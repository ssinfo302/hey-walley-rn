import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, SectionList, View, FlatList } from 'react-native';
import MessageListItem from './MessageListItem';
import { wp, hp, normalize } from '../../helper/responsiveScreen';

const MessageList = (props) => {
  const {
    messageData,
    navigation,
    address,
    socket,
    chatReadData,
    chatDeliverData,
    removePress,
    extraData,
    editPress,
    imagePress,
    audioPress,
    isPlay,
    audioPausePress,
    currentTimeString,
    durationString,
    playSeconds,
    duration,
    onSliderEditStart,
    onSliderEditEnd,
    onSliderEditing,
    isPause,
    audioLoading,
    onFileDownloadPress,
    allDownloadFile,
    onFilePress
  } = props;


  _renderItem = ({ item, index }) => {
    return (
      <MessageListItem
        data={item}
        address={address}
        socket={socket}
        navigation={navigation}
        chatReadData={chatReadData}
        chatDeliverData={chatDeliverData}
        removePress={(item) => removePress(item)}
        editPress={(item) => editPress(item)}
        imagePress={(item) => imagePress(item)}
        audioPress={(item) => audioPress(item)}
        isPlay={isPlay}
        audioPausePress={() => audioPausePress()}
        currentTimeString={currentTimeString}
        durationString={durationString}
        playSeconds={playSeconds}
        duration={duration}
        onSliderEditStart={() => onSliderEditStart()}
        onSliderEditEnd={() => onSliderEditEnd()}
        onSliderEditing={(value) => onSliderEditing(value)}
        isPause={isPause}
        audioLoading={audioLoading}
        onFileDownloadPress={(value) => onFileDownloadPress(value)}
        allDownloadFile={allDownloadFile}
        onFilePress={(value) => onFilePress(value)} />
    );
  };

  return (
    <FlatList
      // inverted
      extraData={extraData}
      ref={ref => { this.flatList = ref }}
      bounces={false}
      data={messageData}
      renderItem={this._renderItem}
      showsVerticalScrollIndicator={false}
      removeClippedSubviews={false}
      style={styles.container}
      onContentSizeChange={() => this.flatList.scrollToEnd({ animated: true })}
      onLayout={() => this.flatList.scrollToEnd({ animated: true })}
    />
  );
};

MessageList.defaultProps = {
};

MessageList.propTypes = {
  messageData: PropTypes.object,
  address: PropTypes.string,
  isPlay: PropTypes.string,
  currentTimeString: PropTypes.string,
  durationString: PropTypes.string,
  audioLoading: PropTypes.string
};

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

export default MessageList;