import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import FontText from '../FontText';
import { hp, isX, wp, normalize } from '../../helper/responsiveScreen';
import colors from '../../assets/colors';

class MediaTabView extends Component {

    render() {
        const { title, tabTitle, onTabPress } = this.props;
        return (
            <TouchableOpacity style={styles.tabView}
                onPress={() => onTabPress()}>
                <FontText size={tabTitle == title ? normalize(24) : normalize(18)} name={'archivo-regular'} color={tabTitle == title ? "white" : "text-light"}
                    style={{ fontWeight: '700' }} >{title}</FontText>
            </TouchableOpacity>

        )
    }
}

const styles = StyleSheet.create({
    tabView: {
        height: hp(5),
        width: wp(20),
        alignItems: 'center',
        justifyContent: 'center',
    },
});


export default MediaTabView;
