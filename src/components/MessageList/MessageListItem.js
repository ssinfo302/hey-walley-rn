import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, Image, TouchableOpacity, Slider, ActivityIndicator, Linking } from 'react-native';
import FontText from '../FontText';
import { wp, hp, normalize, isIOS, isX } from '../../helper/responsiveScreen';
import colors from '../../assets/colors';
import { time } from '../../util/validation';
import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from 'react-native-popup-menu';
// import { Slider } from 'react-native-elements';
import RNFetchBlob from 'react-native-fetch-blob';
import Hyperlink from 'react-native-hyperlink'


const MessageListItem = (props) => {
  const {
    data,
    navigation,
    address,
    socket,
    chatReadData,
    chatDeliverData,
    removePress,
    editPress,
    imagePress,
    audioPress,
    isPlay,
    audioPausePress,
    currentTimeString,
    durationString,
    playSeconds,
    duration,
    onSliderEditStart,
    onSliderEditEnd,
    onSliderEditing,
    isPause,
    audioLoading,
    onFileDownloadPress,
    allDownloadFile,
    onFilePress
  } = props;

  let readStatus = data.chatRead, deliverdStatus = data.chatDeliver, downloadStatus = false

  if (chatReadData.length != 0) {
    let exitData = chatReadData.find(x => x._id === data._id)
    if (exitData) {
      readStatus = exitData.chatRead
    }
  }

  if (chatDeliverData.length != 0) {
    let exitData = chatDeliverData.find(x => x._id === data._id)
    if (exitData) {
      deliverdStatus = exitData.chatDeliver
    }
  }

  if (allDownloadFile.length != 0 && data.file) {
    let exitData = allDownloadFile.find(x => x === data.file.split('/').splice(-1)[ 0 ])
    if (exitData) {
      downloadStatus = true
    }
  }

  return (
    data.senderAddress != address ?

      <View style={styles.rightContainer}>
        <FontText lines={1} textAlign={'right'} size={normalize(10)} name={'archivo-regular'} color="text-dark" style={{ fontWeight: '400' }} >{time(data.updatedAt)}</FontText>

        {data.image.length != 0 ?

          <TouchableOpacity style={{ ...styles.rightChildContainer, padding: wp(2) }}
            onPress={() => imagePress(data)}>
            <Image
              style={{ width: null, height: hp(20), borderRadius: wp(5), }}
              resizeMode='cover'
              source={{ uri: data.image[ 0 ] }}
            />
            <TouchableOpacity style={styles.circleView}
              onPress={() => removePress(data)} >
              <Image source={require('../../assets/images/delete.png')} style={{ width: wp(3), height: wp(3), tintColor: colors[ 'text-gray' ] }} resizeMode="contain" />
            </TouchableOpacity>
          </TouchableOpacity>
          //      <Menu style={{ backgroundColor: colors.transparent, }}>
          //   <MenuTrigger >
          //   </MenuTrigger>

          //   <MenuOptions
          //     optionsContainerStyle={{ marginTop: hp(8), width: wp(60), backgroundColor: colors.transparent, marginLeft: wp(10) }}
          //     style={{ backgroundColor: colors[ 'gray-medium' ], borderRadius: wp(4), overflow: 'hidden', }}>
          //     <MenuOption onSelect={() => removePress(data)} style={styles.menuView} >
          //       <FontText size={normalize(14)} name={'archivo-regular'} color="red" style={{ fontWeight: '500' }} >{'Delete'}</FontText>
          //       <Image source={require('../../assets/images/delete.png')} resizeMode="contain" />
          //     </MenuOption>
          //   </MenuOptions>

          // </Menu>
          :
          data.audio ?
            <View style={{ width: wp(75), marginTop: hp(0.5) }}>
              <View style={{ flexDirection: 'row' }}>
                {audioLoading == data.audio ?
                  <View style={{ padding: wp(1.5) }}>
                    <View style={styles.PlayerCircleView}>
                      <ActivityIndicator
                        animating={true} size="small" color={colors[ 'text-light' ]} />
                    </View>
                  </View>
                  :
                  <TouchableOpacity
                    onPress={() => isPlay == data.audio ? isPause ? audioPress(data.audio) : audioPausePress() : audioPress(data.audio)} style={{ padding: wp(1.5), }}>
                    <View style={styles.PlayerCircleView}>
                      <Image source={isPlay == data.audio ? isPause ? require('../../assets/images/play.png') : require('../../assets/images/pause.png') :
                        require('../../assets/images/play.png')}
                        resizeMode="contain" style={{ width: wp(4), height: wp(4), }} />
                    </View>
                  </TouchableOpacity>
                }
                <Slider
                  onTouchStart={onSliderEditStart}
                  onTouchEnd={onSliderEditEnd}
                  onValueChange={onSliderEditing}
                  value={isPlay == data.audio ? playSeconds : 0}
                  maximumValue={isPlay == data.audio ? duration : 0}
                  maximumTrackTintColor={isIOS ? colors[ 'gray-medium' ] : 'gray'}
                  minimumTrackTintColor={colors[ 'blue-dark' ]}
                  thumbTintColor={colors[ 'blue-dark' ]}
                  style={{ flex: 1, alignSelf: 'center' }} />

                {/* <Slider
                  onSlidingStart={onSliderEditStart}
                  onSlidingComplete={onSliderEditEnd}
                  onValueChange={onSliderEditing}
                  value={isPlay == data.audio ? playSeconds : 0}
                  maximumValue={isPlay == data.audio ? duration : 0}
                  maximumTrackTintColor={isIOS ? colors[ 'gray-medium' ] : 'gray'}
                  minimumTrackTintColor={colors[ 'blue-dark' ]}
                  thumbTintColor={colors[ 'blue-dark' ]}
                  style={{ flex: 1, alignSelf: 'center' }}

                // thumbStyle={{ height: wp(4), width: wp(4), borderRadius: wp(2), backgroundColor: 'transparent' }}
                /> */}

              </View>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: hp(1), marginTop: hp(-1) }}>
                <FontText lines={1} textAlign={'right'} size={normalize(10)} name={'archivo-regular'} color="text-dark"
                  style={{ fontWeight: '400', marginLeft: wp(16) }} >{isPlay == data.audio ? currentTimeString : '00:00'}</FontText>

                <FontText lines={1} textAlign={'right'} size={normalize(10)} name={'archivo-regular'} color="text-dark"
                  style={{ fontWeight: '400', marginRight: isX ? wp(3) : wp(5) }} >{data.totalTime}</FontText>
              </View>
            </View>
            :
            data.file ?
              <TouchableOpacity style={{ ...styles.rightChildContainer, paddingVertical: hp(2), paddingHorizontal: wp(3) }}
                onPress={() => downloadStatus && onFilePress(data.file)}>

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <TouchableOpacity style={{ ...styles.PlayerCircleView, backgroundColor: colors.black }}
                      onPress={() => downloadStatus ? onFilePress(data.file) : onFileDownloadPress(data.file)} >
                      <Image source={downloadStatus ? require('../../assets/images/document_icon.png') : require('../../assets/images/download_icon.png')}
                        style={{ width: wp(5), height: wp(5), }} resizeMode="contain" />
                    </TouchableOpacity>
                    <View style={{ marginLeft: wp(2), width: wp(45), }}>
                      <FontText lines={1} size={normalize(14)} name={'archivo-regular'} color="white" style={{ fontWeight: '400' }} >
                        {data.file.split('/').splice(-1)[ 0 ]}</FontText>
                      <FontText size={normalize(14)} name={'archivo-regular'} color="white" style={{ fontWeight: '400', marginTop: hp(0.3) }} >{data.fileSize}</FontText>
                    </View>

                  </View>

                  <TouchableOpacity style={styles.fileCircleView}
                    onPress={() => removePress(data)} >
                    <Image source={require('../../assets/images/delete.png')} style={{ width: wp(3), height: wp(3), tintColor: colors[ 'text-gray' ] }} resizeMode="contain" />
                  </TouchableOpacity>
                </View>

              </TouchableOpacity>
              :
              <Menu style={{ backgroundColor: colors.transparent, }}>
                <MenuTrigger >
                  <View style={{
                    ...styles.rightChildContainer, paddingVertical: hp(2), paddingRight: wp(5)
                  }}>
                    {data.link ?
                      <FontText onPress={() => Linking.openURL(data.link.includes('https://') ? data.link : `https://${data.link}`)} textAlign={'right'}
                        size={normalize(14)} name={'archivo-regular'} color={"blue"} style={{ fontWeight: '500', paddingTop: hp(0.3) }} >
                        {data.link}</FontText>
                      :
                      <FontText textAlign={'right'} size={normalize(14)} name={'archivo-regular'} color={"white"} style={{ fontWeight: '500', paddingTop: hp(0.3) }} >
                        {data.text}</FontText>
                    }
                  </View>
                </MenuTrigger>

                <MenuOptions
                  optionsContainerStyle={{ marginTop: hp(8), width: wp(55), backgroundColor: colors.transparent, marginLeft: wp(25) }}
                  style={{ backgroundColor: colors[ 'gray-medium' ], borderRadius: wp(4), overflow: 'hidden' }}>
                  <MenuOption onSelect={() => editPress(data)} style={{ ...styles.menuView, marginBottom: hp(0.5) }} >
                    <FontText size={normalize(14)} name={'archivo-regular'} color="white" style={{ fontWeight: '500' }} >{'Edit Message'}</FontText>
                    <Image source={require('../../assets/images/edit_icons.png')} resizeMode="contain" />
                  </MenuOption>
                  <MenuOption onSelect={() => removePress(data)} style={styles.menuView} >
                    <FontText size={normalize(14)} name={'archivo-regular'} color="red" style={{ fontWeight: '500' }} >{'Delete'}</FontText>
                    <Image source={require('../../assets/images/delete.png')} resizeMode="contain" />
                  </MenuOption>
                </MenuOptions>

              </Menu>

        }
        <Image source={readStatus ? require('../../assets/images/blue_tick.png') : deliverdStatus ? require('../../assets/images/double_tick.png') : require('../../assets/images/single_tick.png')} resizeMode="contain" />
      </View>

      :
      <View style={styles.leftContainer}>
        <FontText lines={1} size={normalize(10)} name={'archivo-regular'} color="text-light" style={{ fontWeight: '400' }} >{time(data.updatedAt)}</FontText>
        {data.image.length != 0 ?
          <TouchableOpacity style={{ ...styles.leftChildContainer, padding: wp(2) }}
            onPress={() => imagePress(data)}>
            <Image
              style={{ width: null, height: hp(20), borderRadius: wp(5), }}
              resizeMode='cover'
              source={{ uri: data.image[ 0 ] }}
            />
          </TouchableOpacity>
          :
          data.audio ?
            <View style={{ width: wp(75), marginTop: hp(0.5) }}>
              <View style={{ flexDirection: 'row' }}>
                {audioLoading == data.audio ?
                  <View style={{ padding: wp(1.5) }}>
                    <View style={styles.PlayerCircleView}>
                      <ActivityIndicator
                        animating={true} size="small" color={colors[ 'text-light' ]} />
                    </View>
                  </View>
                  :
                  <TouchableOpacity
                    onPress={() => isPlay == data.audio ? isPause ? audioPress(data.audio) : audioPausePress() : audioPress(data.audio)} style={{ padding: wp(1.5), }}>
                    <View style={styles.PlayerCircleView}>
                      <Image source={isPlay == data.audio ? isPause ? require('../../assets/images/play.png') : require('../../assets/images/pause.png') : require('../../assets/images/play.png')}
                        resizeMode="contain" style={{ width: wp(4), height: wp(4), }} />
                    </View>
                  </TouchableOpacity>
                }
                <Slider
                  onTouchStart={onSliderEditStart}
                  onTouchEnd={onSliderEditEnd}
                  onValueChange={onSliderEditing}
                  value={isPlay == data.audio ? playSeconds : 0}
                  maximumValue={isPlay == data.audio ? duration : 0}
                  maximumTrackTintColor={isIOS ? colors[ 'gray-medium' ] : 'gray'}
                  minimumTrackTintColor={colors[ 'blue-dark' ]}
                  thumbTintColor={colors[ 'blue-dark' ]}
                  style={{ flex: 1, alignSelf: 'center' }} />

                {/* <Slider
                  onSlidingStart={onSliderEditStart}
                  onSlidingComplete={onSliderEditEnd}
                  onValueChange={onSliderEditing}
                  value={isPlay == data.audio ? playSeconds : 0}
                  maximumValue={isPlay == data.audio ? duration : 0}
                  maximumTrackTintColor={isIOS ? colors[ 'gray-medium' ] : 'gray'}
                  minimumTrackTintColor={colors[ 'blue-dark' ]}
                  thumbTintColor={colors[ 'blue-dark' ]}
                  style={{ flex: 1, alignSelf: 'center' }}
                  thumbStyle={{ height: wp(10), width: wp(10), backgroundColor: 'transparent' }}
                /> */}

              </View>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: hp(1), marginTop: hp(-1) }}>
                <FontText lines={1} textAlign={'right'} size={normalize(10)} name={'archivo-regular'} color="text-dark"
                  style={{ fontWeight: '400', marginLeft: wp(16) }} >{isPlay == data.audio ? currentTimeString : '00:00'}</FontText>

                <FontText lines={1} textAlign={'right'} size={normalize(10)} name={'archivo-regular'} color="text-dark"
                  style={{ fontWeight: '400', marginRight: isX ? wp(3) : wp(5) }} >{data.totalTime}</FontText>
              </View>
            </View>
            :

            data.file ?
              <TouchableOpacity style={{ ...styles.leftChildContainer, paddingVertical: hp(2), paddingHorizontal: wp(3) }}
                onPress={() => downloadStatus && onFilePress(data.file)}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <TouchableOpacity style={{ ...styles.PlayerCircleView, backgroundColor: colors.black }}
                    onPress={() => downloadStatus ? onFilePress(data.file) : onFileDownloadPress(data.file)} >
                    <Image source={downloadStatus ? require('../../assets/images/document_icon.png') : require('../../assets/images/download_icon.png')}
                      style={{ width: wp(5), height: wp(5), }} resizeMode="contain" />
                  </TouchableOpacity>
                  <View style={{ marginLeft: wp(2), width: wp(55) }}>
                    <FontText lines={1} size={normalize(14)} name={'archivo-regular'} color="white" style={{ fontWeight: '400' }} >
                      {data.file.split('/').splice(-1)[ 0 ]}</FontText>
                    <FontText size={normalize(14)} name={'archivo-regular'} color="white" style={{ fontWeight: '400', marginTop: hp(0.3) }} >{data.fileSize}</FontText>
                  </View>
                </View>
              </TouchableOpacity>
              :

              <View style={{ ...styles.leftChildContainer, paddingVertical: hp(2), paddingLeft: wp(5) }}>
                {data.link ?
                  <FontText onPress={() => Linking.openURL(data.link.includes('https://') ? data.link : `https://${data.link}`)}
                    size={normalize(14)} name={'archivo-regular'} color={"blue"} style={{ fontWeight: '500', paddingTop: hp(0.3) }} >
                    {data.link}</FontText>
                  :
                  <FontText size={normalize(14)} name={'archivo-regular'} color={"white"} style={{ fontWeight: '500', paddingTop: hp(0.3) }} >{data.text}</FontText>
                }
              </View>
        }

      </View>

  );
};

MessageListItem.propTypes = {
  data: PropTypes.object
};

const styles = StyleSheet.create({
  rightContainer: {
    flex: 1,
    alignItems: 'flex-end',
    marginVertical: hp(1)
  },
  rightChildContainer: {
    width: wp(75),
    marginTop: hp(1),
    marginBottom: hp(0.5),
    backgroundColor: colors[ 'gray-dark' ],
    borderTopLeftRadius: wp(5),
    borderTopRightRadius: wp(5),
    borderBottomLeftRadius: wp(5),
    borderWidth: 1,
    borderColor: colors[ 'border-dark' ],
  },
  leftContainer: {
    flex: 1,
    marginVertical: hp(1)
  },
  leftChildContainer: {
    width: wp(75),
    marginTop: hp(1),
    marginBottom: hp(0.5),
    backgroundColor: colors[ 'gray-medium' ],
    borderTopRightRadius: wp(5),
    borderBottomLeftRadius: wp(5),
    borderBottomEndRadius: wp(5),
    borderWidth: 1,
    borderColor: colors[ 'border-dark' ]
  },
  touchView: {
    // padding: wp(3.5),
    // backgroundColor: 'red',
    // marginVertical: hp(1)
  },
  menuView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: wp(5),
    paddingVertical: hp(1.5),
    backgroundColor: colors[ 'text-dark' ],
  },
  circleView: {
    width: wp(8),
    height: wp(8),
    borderRadius: wp(4),
    backgroundColor: colors[ 'gray-dark' ],
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
    right: 0,
    marginBottom: wp(3.5),
    marginRight: wp(3.5)
  },
  PlayerCircleView: {
    width: wp(11),
    height: wp(11),
    borderRadius: wp(5.5),
    backgroundColor: colors[ 'gray-medium' ],
    justifyContent: 'center',
    alignItems: 'center',
  },
  fileCircleView: {
    width: wp(8),
    height: wp(8),
    borderRadius: wp(4),
    backgroundColor: colors.black,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default MessageListItem;