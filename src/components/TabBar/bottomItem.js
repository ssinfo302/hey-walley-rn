import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import FontText from '../../components/FontText';
import { hp, isX, wp, normalize } from '../../helper/responsiveScreen';
import colors from '../../assets/colors';

class BottomItem extends Component {

    render() {
        const { title, image } = this.props;
        return (
            <View style={styles.childContainer}>
                <Image source={image} resizeMode="contain" />
                <FontText size={normalize(14)} name={'archivo-regular'} color="white" style={styles.title} >{title}</FontText>
            </View>

        )
    }
}

const styles = StyleSheet.create({
    childContainer: {
        height: wp(12),
        paddingHorizontal: wp(4),
        marginRight: wp(3),
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: colors.black,
        borderRadius: wp(10),
        borderWidth: 1.5,
        borderColor: colors['gray-dark']
    },
    title: {
        marginLeft: wp(1.5)
    },
});


export default BottomItem;
