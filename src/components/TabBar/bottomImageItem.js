import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, Image } from 'react-native';
import { hp, isX, wp, normalize } from '../../helper/responsiveScreen';
import colors from '../../assets/colors';

class BottomImageItem extends Component {

    render() {
        const { image, onItemPress } = this.props;
        return (
            <TouchableOpacity onPress={() => onItemPress()} style={styles.imageView}>
                <Image source={image} resizeMode="contain" />
            </TouchableOpacity>

        )
    }
}

const styles = StyleSheet.create({
    imageView: {
        width: wp(12),
        height: wp(12),
        borderRadius: wp(6),
        marginRight: wp(3),
        backgroundColor: colors['gray-dark'],
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
    },
});


export default BottomImageItem;
