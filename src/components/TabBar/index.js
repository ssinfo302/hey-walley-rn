import React, { Component } from 'react';
import { View, StyleSheet, BackHandler, ToastAndroid } from 'react-native';
import { hp, wp, isAndroid } from '../../helper/responsiveScreen';
import Home, { routeName as homeRouteName } from '../../containers/Home'
import Search, { routeName as searchRouteName } from '../../containers/Search'
import Wallet, { routeName as walletRouteName } from '../../containers/Wallet'
import colors from '../../assets/colors';
import BottomItem from './bottomItem'
import BottomImageItem from './bottomImageItem'
import { routeName as tabNavigationRouteName } from '../../navigation/TabNavigation'
import { resetNavigateTo } from '../../helper/navigationHelper';


class TabBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tabName: 'Home',
      isClosed: false
    };
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this)
  }

  componentDidMount() {

    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    )
  }

  handleBackButtonClick = () => {
    const { tabName, isClosed } = this.state
    const { navigation } = this.props;
    console.log('tab', tabName)
    if (tabName == 'Home') {
      if (isClosed) {
        this.setState({ isClosed: false })
        BackHandler.exitApp()
      } else {
        setTimeout(() => { this.setState({ isClosed: false }) }, 3000);

        if (isAndroid) {
          ToastAndroid.show("Press Again To Exit !", ToastAndroid.SHORT);
        }

        this.setState({ isClosed: true })
      }
    } else {
      this.setState({ tabName: 'Home' })
      resetNavigateTo(navigation, homeRouteName)
    }
    return true
  }


  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    )
  }

  onPressHandler = (tabName, route) => {
    this.setState({ tabName: tabName })
    this.props.navigation.navigate(route);
  }

  render() {
    const { tabName } = this.state
    return (
      <View style={styles.container}>
        {tabName != 'Home' ?
          <BottomImageItem
            onItemPress={() => this.onPressHandler('Home', homeRouteName)}
            image={require('../../assets/images/home.png')} />
          :
          <BottomItem
            title={tabName}
            image={require('../../assets/images/home.png')} />
        }


        {tabName != 'Search' ?
          <BottomImageItem
            onItemPress={() => this.onPressHandler('Search', searchRouteName)}
            image={require('../../assets/images/search.png')} />
          :
          <BottomItem
            title={tabName}
            image={require('../../assets/images/search.png')} />
        }

        {tabName != 'Wallet' ?
          <BottomImageItem
            onItemPress={() => this.onPressHandler('Wallet', walletRouteName)}
            image={require('../../assets/images/wallet.png')} />
          :
          <BottomItem
            title={tabName}
            image={require('../../assets/images/wallet.png')}
          />
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginBottom: hp(2.5),
    paddingVertical: hp(1.5),
    backgroundColor: colors.black,
    borderRadius: wp(10),
    paddingLeft: wp(3),
    alignSelf: 'center',
    flexDirection: 'row',
    position: 'absolute',
    bottom: 0,
    borderWidth: 1.5,
    borderColor: colors[ 'gray-dark' ]
  },
});

export default TabBar;
