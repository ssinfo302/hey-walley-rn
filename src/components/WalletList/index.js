import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, FlatList, View } from 'react-native';
import WalletListItem from './WalletListItem';
import { wp, hp, normalize } from '../../helper/responsiveScreen';

const WalletList = (props) => {
  const {
    walletListData,
    navigation,
    addressData,
    addWallet,
    removeWallet
  } = props;


  _renderItem = ({ item, index }) => {
    return (
      <WalletListItem data={item} addressData={addressData} index={index} navigation={navigation} addWallet={addWallet} removeWallet={removeWallet} />
    );
  };


  return (
    <FlatList
      bounces={false}
      data={walletListData}
      renderItem={this._renderItem}
      showsVerticalScrollIndicator={false}
      removeClippedSubviews={false}
      style={styles.container}
      contentContainerStyle={{ paddingBottom: hp(10.5) }}
    />
  );
};

WalletList.defaultProps = {
};

WalletList.propTypes = {
  walletListData: PropTypes.object,
  addressData: PropTypes.object
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginBottom: hp(2)
  }
});

export default WalletList;