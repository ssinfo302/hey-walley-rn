import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View, StyleSheet, Image, TouchableOpacity, TouchableWithoutFeedback, UIManager, LayoutAnimation, ToastAndroid, Alert
} from 'react-native';
import FontText from '../FontText';
import { wp, hp, normalize, isAndroid, isIOS } from '../../helper/responsiveScreen';
import colors from '../../assets/colors';
import AddModelView from '../AddModelView';
import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from 'react-native-popup-menu';
import Clipboard from '@react-native-community/clipboard';
import { addWalletToParams } from '../../helper/toParams';
import AsyncStorage from '@react-native-community/async-storage';
import { removeCheckWalletData, removeAddWalletData } from '../../util/Store';

class WalletListItem extends Component {
  constructor(props) {
    super(props)
    this.state = {
      expandeIndex: -1,
      isAddWallet: false,
      walletAddress: '',
      blockChain: '',
      address: ''
    }
    if (isAndroid) {
      UIManager.setLayoutAnimationEnabledExperimental(true); // USed for collaps animation
    }
  }

  async componentDidMount() {
    this.focusListener = this.props.navigation.addListener("focus", async () => {
      let address = JSON.parse(await AsyncStorage.getItem('address'))
      this.setState({ address: address })
    })

  }

  componentWillUnmount() {
    this.focusListener();
  }

  toggleExpand = (index) => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    if (this.state.expandeIndex == index) {
      this.setState({ expandeIndex: -1 })
    } else {
      this.setState({ expandeIndex: index })
    }
  }

  onInputChange = name => value => {
    this.setState({ walletAddress: value });
  }

  onAddWalletPress = (chain) => {
    this.setState({ isAddWallet: true, blockChain: chain })
  }

  onLableCancelPress = () => {
    this.setState({ isAddWallet: false, walletAddress: '' })
  }

  onAddPress = async () => {
    const { walletAddress } = this.state
    const { data, addWallet } = this.props;
    if (walletAddress != '') {

      let existData = JSON.parse(await AsyncStorage.getItem('check wallet data')).find(x => x.address === walletAddress)

      if (existData) {
        alert('this address already added')
      } else {
        this.setState({ isAddWallet: false })
        addWallet(addWalletToParams(this.props, this.state))
        AsyncStorage.setItem('address', JSON.stringify(walletAddress));
        this.setState({ walletAddress: '', blockChain: '' })
      }
    } else {
      alert('Please Add Wallet Address')
    }
  }

  onCopyPressHandler = (value) => {
    Clipboard.setString(value)
    if (isIOS) {
      alert('Successfully copy address');
    } else {
      ToastAndroid.show('Successfully copy address', ToastAndroid.SHORT);
    }
  }

  onDeletePress = async (index) => {
    const { removeWallet } = this.props;
    const { address } = this.state;

    Alert.alert(
      "",
      "Are you sure to delete Address?",
      [
        {
          text: "Yes", onPress: async () => {
            let walletData = JSON.parse(await AsyncStorage.getItem('check wallet data'));
            let addWalletData = JSON.parse(await AsyncStorage.getItem('add wallet data'));

            const payload = {
              address: walletData[ index ].address,
              chain: walletData[ index ].chain,
              uid: addWalletData[ index ].uid
            }
            removeWallet(payload)
            removeCheckWalletData(index)
            removeAddWalletData(index)

            if (address == walletData[ index ].address) {
              this.setState({ address: JSON.parse(await AsyncStorage.getItem('check wallet data'))[ 0 ].address })
            }
          }
        },
        {
          text: "No",
          onPress: () => console.log("Cancel Pressed"),
        }
      ],
      { cancelable: false }
    );

  }

  onChangeAddressPress = (value) => {
    const { address } = this.state;

    if (address != value) {
      Alert.alert(
        "",
        "Are you sure to change Address?",
        [
          {
            text: "Yes", onPress: () => {
              this.setState({ address: value })
              AsyncStorage.setItem('address', JSON.stringify(value));
            }
          },
          {
            text: "No",
            onPress: () => console.log("Cancel Pressed"),
          }
        ],
        { cancelable: false }
      );
    }
  }

  render() {
    const { expandeIndex, isAddWallet, walletAddress, address } = this.state;
    const { data, navigation, index, addressData } = this.props;


    return (
      <View style={styles.container}>
        <FontText size={normalize(18)} name={'archivo-regular'} color="white" style={{ fontWeight: '700' }} >{data.subTitle}</FontText>

        <TouchableWithoutFeedback onPress={() => this.toggleExpand(index)}>
          <View style={styles.childContainer}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginVertical: hp(2) }}>
              <View style={styles.rowView}>
                <Image source={data.image} resizeMode="contain" />
                <FontText size={normalize(22)} name={'archivo-regular'} color="white" style={{ fontWeight: '600', marginLeft: wp(4) }} >{data.title}</FontText>
              </View>
              <Image source={expandeIndex == index ? require('../../assets/images/up_arrow.png')
                : require('../../assets/images/down_arrow.png')} resizeMode="contain" />
            </View>

            {expandeIndex == index &&
              <View>
                {addressData && addressData.map((item, index) => {
                  return (
                    data.title == (item.chain == 'SOL' ? 'SOLANA' : item.chain == 'DOT' ? 'POLKADOT' : item.chain) &&
                    <View key={index} style={styles.expandView}>
                      <TouchableOpacity
                        onPress={() => this.onChangeAddressPress(item.address)}
                        style={{
                          ...styles.expandChildView,
                          backgroundColor: item.address === address ? colors.black : colors[ 'gray-dark' ],
                          borderWidth: item.address === address ? 1.5 : 0,
                          borderColor: colors.blue,
                          width: wp(67)
                        }}>
                        <FontText size={normalize(14)} name={'archivo-regular'} color="white" style={{ fontWeight: '400' }} >{item.address}</FontText>
                      </TouchableOpacity>
                      <Menu style={{ backgroundColor: colors.transparent }}>
                        <MenuTrigger style={styles.touchView}>
                          <Image source={require('../../assets/images/dot_icon.png')} resizeMode="contain" />
                        </MenuTrigger>

                        <MenuOptions
                          optionsContainerStyle={{ marginTop: hp(3.5), width: wp(65), backgroundColor: colors.transparent, }}
                          style={{ backgroundColor: colors[ 'border-dark' ], borderRadius: wp(4), overflow: 'hidden' }}>

                          <MenuOption onSelect={() => this.onCopyPressHandler(item.address)} style={{ ...styles.menuView, marginBottom: hp(1) }} >
                            <FontText size={normalize(14)} name={'archivo-regular'} color="white" style={{ fontWeight: '500' }} >{'Copy'}</FontText>
                            <Image source={require('../../assets/images/paste.png')} resizeMode="contain" />
                          </MenuOption>
                          <MenuOption onSelect={() => this.onDeletePress(index)} style={styles.menuView} >
                            <FontText size={normalize(14)} name={'archivo-regular'} color="red" style={{ fontWeight: '500' }} >{'Delete'}</FontText>
                            <Image source={require('../../assets/images/delete.png')} resizeMode="contain" />
                          </MenuOption>
                        </MenuOptions>
                      </Menu>
                    </View>
                  )
                }
                )}


                <TouchableOpacity
                  onPress={() => this.onAddWalletPress(data.title)}
                  style={{ ...styles.expandChildView, marginBottom: hp(1.5), paddingVertical: hp(1.5), flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                  <Image source={require('../../assets/images/add_icon.png')} resizeMode="contain" />
                  <FontText size={normalize(16)} name={'archivo-regular'} color="text-light" style={{ fontWeight: '600', marginLeft: wp(4) }} >{`Add ${data.title} Wallet`}</FontText>
                </TouchableOpacity>
              </View>
            }

          </View>

        </TouchableWithoutFeedback>

        <AddModelView
          title={'Wallet'}
          placeholder={'Please Enter Wallet Address'}
          isAddLabel={isAddWallet}
          value={walletAddress}
          onInputChange={(value) => this.onInputChange(value)}
          onLableCancelPress={() => this.onLableCancelPress()}
          onAddPress={() => this.onAddPress()}
          style={styles.modalView}
        />

      </View>
    )
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: hp(3.5)
  },
  childContainer: {
    paddingHorizontal: wp(5),
    backgroundColor: colors[ 'gray-medium' ],
    borderRadius: wp(5),
    marginTop: hp(1),
  },
  rowView: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  expandView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: hp(1.5)
  },
  expandChildView: {
    paddingVertical: hp(2),
    paddingHorizontal: wp(6),
    backgroundColor: colors[ 'gray-dark' ],
    borderRadius: wp(5)
  },
  touchView: {
    padding: wp(3.5),
  },
  modalView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(52, 52, 52, 0.5)',
    paddingHorizontal: wp(8),
  },
  menuView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: wp(5),
    paddingVertical: hp(1.5),
    backgroundColor: colors[ 'gray-medium' ],
  }
});

export default WalletListItem;
