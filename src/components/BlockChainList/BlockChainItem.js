import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, ImageBackground, Image, TouchableOpacity } from 'react-native';
import FontText from '../FontText';
import { wp, hp, normalize } from '../../helper/responsiveScreen';

const BlockChainItem = (props) => {
  const {
    data,
    navigation,
    onItemPress
  } = props;

  return (
    <TouchableOpacity style={styles.container} onPress={() => onItemPress(data)}>
      <Image source={data.image} resizeMode="contain" />
      <FontText size={normalize(18)} name={'archivo-regular'} color="white" pLeft={wp(3)}  >{data.title}</FontText>
    </TouchableOpacity>
  );
};

BlockChainItem.propTypes = {
  data: PropTypes.object
};

const styles = StyleSheet.create({
  container: {
    width: '50%',
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: hp(3)
  },
  image: {
    width: wp(10),
    height: wp(10),
    alignSelf: 'center',
    marginTop: wp(10)
  },
  title: {
    marginTop: wp(1),
    textAlign: 'center'
  },
});

export default BlockChainItem;