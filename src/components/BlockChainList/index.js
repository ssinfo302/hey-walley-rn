import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, FlatList, View } from 'react-native';
import BlockChainItem from './BlockChainItem';
import { wp, hp, normalize } from '../../helper/responsiveScreen';

const BlockChainList = (props) => {
  const {
    blockChainData,
    navigation,
    onItemPress
  } = props;

  _renderItem = ({ item, index }) => {
    return (
      <BlockChainItem data={item} onItemPress={(item) => onItemPress(item)} navigation={navigation} />
    );
  };


  return (
    <FlatList
      bounces={false}
      data={blockChainData}
      renderItem={this._renderItem}
      showsVerticalScrollIndicator={false}
      removeClippedSubviews={false}
      style={styles.container}
      contentContainerStyle={styles.contentContainer}
      numColumns={2}
      scrollEnabled={false}
    />
  );
};

BlockChainList.defaultProps = {
};

BlockChainList.propTypes = {
  blockChainData: PropTypes.object,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: hp(2)
  },
  contentContainer: {
    paddingVertical: hp(1),
  }
});

export default BlockChainList;