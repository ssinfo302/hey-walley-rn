import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, ImageBackground, Image, Slider, TouchableOpacity, ActivityIndicator, Linking } from 'react-native';
import FontText from '../FontText';
import { wp, hp, normalize, isIOS, isX } from '../../helper/responsiveScreen';
import colors from '../../assets/colors';

const SearchListItem = (props) => {
  const {
    data,
    onItemPress,
    tabTitle,
    imagePress,
    onFileDownloadPress,
    allDownloadFile,
    onFilePress,
    audioPress,
    isPlay,
    audioPausePress,
    currentTimeString,
    durationString,
    playSeconds,
    duration,
    onSliderEditStart,
    onSliderEditEnd,
    onSliderEditing,
    isPause,
    audioLoading,
  } = props;

  let downloadStatus = false

  if (allDownloadFile.length != 0 && data.file) {
    let exitData = allDownloadFile.find(x => x === data.file.split('/').splice(-1)[ 0 ])
    if (exitData) {
      downloadStatus = true
    }
  }

  return (
    // address != data.address &&
    tabTitle == 'Media' ?
      <TouchableOpacity style={{ marginBottom: wp(4) }}
        onPress={() => imagePress(data)}>
        <Image
          style={{ width: wp(38), height: wp(38), borderRadius: wp(5), }}
          source={{ uri: data.image[ 0 ] }}
        />
      </TouchableOpacity>

      : tabTitle == 'Files' ?
        <TouchableOpacity style={styles.fileContainer}
          onPress={() => downloadStatus && onFilePress(data.file)}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <TouchableOpacity style={{ ...styles.PlayerCircleView }}
                onPress={() => downloadStatus ? onFilePress(data.file) : onFileDownloadPress(data.file)} >
                <Image source={downloadStatus ? require('../../assets/images/document_icon.png') : require('../../assets/images/download_icon.png')}
                  style={{ width: wp(5), height: wp(5), }} resizeMode="contain" />
              </TouchableOpacity>
              <View style={{ marginLeft: wp(2), width: wp(45), }}>
                <FontText lines={1} size={normalize(14)} name={'archivo-regular'} color="white" style={{ fontWeight: '400' }} >
                  {data.file.split('/').splice(-1)[ 0 ]}</FontText>
                <FontText size={normalize(14)} name={'archivo-regular'} color="white" style={{ fontWeight: '400', marginTop: hp(0.3) }} >{data.fileSize}</FontText>
              </View>

            </View>

            <Image source={require('../../assets/images/eye_icon.png')} style={{ width: wp(5), height: wp(5), tintColor: colors[ 'text-gray' ] }} resizeMode="contain" />
          </View>
        </TouchableOpacity>

        : tabTitle == 'Audio' ?
          <View style={{ ...styles.fileContainer, paddingVertical: hp(1), }}>
            <View style={{ flexDirection: 'row' }}>
              {audioLoading == data.audio ?
                <View style={{ padding: wp(1.5) }}>
                  <View style={styles.PlayerCircleView}>
                    <ActivityIndicator
                      animating={true} size="small" color={colors[ 'text-light' ]} />
                  </View>
                </View>
                :
                <TouchableOpacity
                  onPress={() => isPlay == data.audio ? isPause ? audioPress(data.audio) : audioPausePress() : audioPress(data.audio)} style={{ padding: wp(1.5), }}>
                  <View style={styles.PlayerCircleView}>
                    <Image source={isPlay == data.audio ? isPause ? require('../../assets/images/play.png') : require('../../assets/images/pause.png') :
                      require('../../assets/images/play.png')}
                      resizeMode="contain" style={{ width: wp(4), height: wp(4), }} />
                  </View>
                </TouchableOpacity>
              }
              <Slider
                onTouchStart={onSliderEditStart}
                onTouchEnd={onSliderEditEnd}
                onValueChange={onSliderEditing}
                value={isPlay == data.audio ? playSeconds : 0}
                maximumValue={isPlay == data.audio ? duration : 0}
                maximumTrackTintColor={'gray'}
                minimumTrackTintColor={colors[ 'blue-dark' ]}
                thumbTintColor={colors[ 'blue-dark' ]}
                style={{ flex: 1, alignSelf: 'center' }} />

            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: hp(1), marginTop: hp(-1) }}>
              <FontText lines={1} textAlign={'right'} size={normalize(10)} name={'archivo-regular'} color="text-light"
                style={{ fontWeight: '400', marginLeft: wp(16) }} >{isPlay == data.audio ? currentTimeString : '00:00'}</FontText>

              <FontText lines={1} textAlign={'right'} size={normalize(10)} name={'archivo-regular'} color="text-light"
                style={{ fontWeight: '400', marginRight: isX ? wp(3) : wp(5) }} >{data.totalTime}</FontText>
            </View>
          </View>
          :
          <View style={styles.fileContainer}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
              <FontText onPress={() => Linking.openURL(data.link.includes('https://') ? data.link : `https://${data.link}`)} lines={2}
                size={normalize(14)} name={'archivo-regular'} color="blue" style={{ fontWeight: '400', width: wp(65) }} >{data.link}</FontText>
              <Image source={require('../../assets/images/eye_icon.png')} style={{ width: wp(5), height: wp(5), tintColor: colors[ 'text-gray' ] }} resizeMode="contain" />
            </View>
          </View>
  );
};

SearchListItem.propTypes = {
  data: PropTypes.object
};

const styles = StyleSheet.create({
  fileContainer: {
    marginBottom: hp(2),
    backgroundColor: colors[ 'gray-medium' ],
    borderRadius: wp(5),
    paddingVertical: hp(2),
    paddingHorizontal: wp(3)
  },
  PlayerCircleView: {
    width: wp(11),
    height: wp(11),
    borderRadius: wp(5.5),
    backgroundColor: colors.black,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default SearchListItem;