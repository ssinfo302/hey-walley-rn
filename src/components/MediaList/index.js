import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, FlatList, View, Keyboard } from 'react-native';
import MediaListItem from './MediaListItem';
import { wp, hp, normalize } from '../../helper/responsiveScreen';

const MediaList = (props) => {
  const {
    mediaListData,
    onItemPress,
    tabTitle,
    imagePress,
    onFileDownloadPress,
    allDownloadFile,
    onFilePress,
    audioPress,
    isPlay,
    audioPausePress,
    currentTimeString,
    durationString,
    playSeconds,
    duration,
    onSliderEditStart,
    onSliderEditEnd,
    onSliderEditing,
    isPause,
    audioLoading,
  } = props;


  _renderItem = ({ item, index }) => {
    return (
      <MediaListItem
        data={item}
        tabTitle={tabTitle}
        onItemPress={(item) => onItemPress(item)}
        imagePress={(item) => imagePress(item)}
        onFileDownloadPress={(value) => onFileDownloadPress(value)}
        allDownloadFile={allDownloadFile}
        onFilePress={(value) => onFilePress(value)}
        audioPress={(item) => audioPress(item)}
        isPlay={isPlay}
        audioPausePress={() => audioPausePress()}
        currentTimeString={currentTimeString}
        durationString={durationString}
        playSeconds={playSeconds}
        duration={duration}
        onSliderEditStart={() => onSliderEditStart()}
        onSliderEditEnd={() => onSliderEditEnd()}
        onSliderEditing={(value) => onSliderEditing(value)}
        isPause={isPause}
        audioLoading={audioLoading}
      />
    );
  };

  return (
    tabTitle == 'Media' ?
      <FlatList
        key={'_'}
        keyExtractor={(item, index) => "_" + String(index)}
        bounces={false}
        data={mediaListData}
        renderItem={this._renderItem}
        showsVerticalScrollIndicator={false}
        removeClippedSubviews={false}
        style={styles.container}
        columnWrapperStyle={{ justifyContent: 'space-between' }}
        scrollEnabled={false}
        numColumns={2}
        // keyboardShouldPersistTaps={'never'}
        onLayout={() => Keyboard.dismiss()}
      />
      :
      <FlatList
        key={'#'}
        keyExtractor={(item, index) => "#" + String(index)}
        bounces={false}
        data={mediaListData}
        renderItem={this._renderItem}
        showsVerticalScrollIndicator={false}
        removeClippedSubviews={false}
        style={styles.container}
        scrollEnabled={false}
        // keyboardShouldPersistTaps={'never'}
        onLayout={() => Keyboard.dismiss()}
      />
  );
};

MediaList.defaultProps = {
};

MediaList.propTypes = {
  mediaListData: PropTypes.object,
  isPlay: PropTypes.string,
  currentTimeString: PropTypes.string,
  durationString: PropTypes.string,
  audioLoading: PropTypes.string
};

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

export default MediaList;