import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, FlatList, View } from 'react-native';
import ChatListItem from './ChatListItem';
import { wp, hp, normalize } from '../../helper/responsiveScreen';

const ChatList = (props) => {
  const {
    chatListData,
    navigation,
    onItemPress,
    address,
    socket,
    extraData,
    removePress,
    data
  } = props;


  _renderItem = ({ item, index }) => {
    return (
      <ChatListItem data={item} chatListData={chatListData} address={address} onItemPress={(item) => onItemPress(item)} navigation={navigation} socket={socket} removePress={(item) => removePress(item)} />
    );
  };


  return (
    <FlatList
      extraData={extraData}
      bounces={false}
      data={data}
      renderItem={this._renderItem}
      showsVerticalScrollIndicator={false}
      removeClippedSubviews={false}
      style={styles.container}
      contentContainerStyle={{ paddingBottom: hp(10.5) }}
    />
  );
};

ChatList.defaultProps = {
};

ChatList.propTypes = {
  data: PropTypes.array,
  chatListData: PropTypes.object,
  address: PropTypes.string,
  removePress: PropTypes.func,
};

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

export default ChatList;