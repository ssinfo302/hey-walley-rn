import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import FontText from '../FontText';
import { wp, hp, normalize } from '../../helper/responsiveScreen';
import colors from '../../assets/colors';
import { time } from '../../util/validation';
import { remove_chat } from '../../api/wallet';

const ChatListItem = (props) => {
  const {
    data,
    navigation,
    onItemPress,
    address,
    chatListData,
    removePress,
    socket,
  } = props;




  let existData = data.participants.find(x => x.address != address);
  let labelData = chatListData.label.find(x => x.threadId == data.threadId);
  let image
  if (existData) {
    if (existData.chain == "ETH") {
      image = require('../../assets/images/eth_image.png')
    } else if (existData.chain == "BTC") {
      image = require('../../assets/images/btc_image.png')
    } else if (existData.chain == "BSC") {
      image = require('../../assets/images/bsc_image.png')
    } else if (existData.chain == "DOT") {
      image = require('../../assets/images/dot_image.png')
    } else if (existData.chain == "SOL") {
      image = require('../../assets/images/solana_icon.png')
    }
  }

  if (existData?.type == 'BUSINESS' && existData?.projectLogo) {
    image = { uri: existData?.projectLogo }
  } else if (existData?.type == 'BUSINESS') {
    image = require('../../assets/images/dapps_placeholder.png')
  }

  let readStatus = true
  if (data.lastMessage.senderAddress != address) {
    readStatus = data.lastMessage.chatRead
  }

  if (!data.lastMessage.chatDeliver) {
    if (address != data.lastMessage.senderAddress) {
      socket.emit("ack_deliver", data.lastMessage._id, chat => {
        console.log('ack do....', chat);
      });
    }
  }

  let count = 0

  if (chatListData.unreadMessage.length != 0) {
    let exitData = chatListData.unreadMessage.find(x => x.threadId === data.threadId)
    if (exitData) {
      count = exitData.count
    }
  }

  return (
    existData ?
      <TouchableOpacity style={{
        ...styles.container,
        backgroundColor: !readStatus ? colors.black : colors[ 'gray-medium' ],
        borderWidth: !readStatus ? 1.5 : 0,
        borderColor: colors.border
      }}
        onPress={() => onItemPress(data)}>
        <Image source={image} style={{ width: wp(10), height: wp(10) }} resizeMode="contain" />
        <View style={styles.childContainer}>
          <View style={styles.rowView}>
            <FontText size={normalize(12)} name={'archivo-regular'} color="white" style={{ width: wp(43), }} >{existData?.projectName ? existData?.projectName : existData.address}</FontText>
            {/* <View style={{ flexDirection: 'row', marginRight: wp(-4) }}> */}
            <FontText size={normalize(9)} name={'archivo-regular'} color="text" >{time(data.updatedAt)}</FontText>

            {/* </View> */}
          </View>

          <View style={styles.lableView}>
            {labelData && labelData.label.map((item, index) =>
              <View key={index} style={{ ...styles.lableChildView, backgroundColor: !readStatus ? colors[ 'gray-medium' ] : colors.black }}>
                <FontText size={normalize(10)} name={'archivo-regular'} color={!readStatus ? "text-gray" : "white"} >{item.text}</FontText>
              </View>
            )}
          </View>

          <View style={{ ...styles.rowView, alignItems: 'center', marginBottom: hp(-0.5) }}>
            {data.lastMessage.audio ?
              <Image source={require('../../assets/images/audio_icon.png')} style={{ width: wp(4), height: wp(4) }} resizeMode="contain" />
              :
              data.lastMessage.file ?
                <Image source={require('../../assets/images/document_icon.png')} style={{ width: wp(4), height: wp(4) }} resizeMode="contain" />
                :
                data.lastMessage.image.length != 0 ?
                  <Image source={require('../../assets/images/placeholder_image.png')} style={{ width: wp(4), height: wp(4), tintColor: colors[ 'text-light' ] }} resizeMode="contain" />
                  :
                  <FontText lines={2} size={normalize(14)} name={'archivo-regular'} color="text" style={{ width: wp(50) }} >
                    {data.lastMessage.text ? data.lastMessage.text : data.lastMessage.link}</FontText>
            }
            <View style={{ flexDirection: 'row', alignItems: 'center', marginRight: wp(-3) }}>
              {count != 0 &&
                <View style={{ ...styles.countView, backgroundColor: !readStatus ? colors.blue : colors[ 'text-dark' ] }}>
                  <FontText size={normalize(8)} name={'archivo-regular'} color="white" >{count}</FontText>
                </View>
              }
              <TouchableOpacity style={{ paddingHorizontal: wp(3), paddingVertical: hp(0.5) }}
                onPress={() => removePress(data)}>
                <Image source={require('../../assets/images/delete.png')} style={{ width: wp(4.5), height: wp(4.5) }} resizeMode="contain" />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </TouchableOpacity>
      : null
  );
};

ChatListItem.propTypes = {
  data: PropTypes.object,
  removePress: PropTypes.func,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: wp(5),
    marginBottom: hp(2),
    paddingHorizontal: wp(5),
    paddingVertical: hp(1.5)
  },
  childContainer: {
    flex: 1,
    marginLeft: wp(3)
  },
  lableView: {
    flexWrap: 'wrap',
    flexDirection: 'row',
  },
  lableChildView: {
    marginRight: wp(2),
    paddingHorizontal: wp(3),
    paddingVertical: hp(0.5),
    borderRadius: wp(5),
    marginTop: hp(0.8),
    marginBottom: hp(0.5)
    // marginBottom: hp(1),
  },
  rowView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  countView: {
    width: wp(5),
    height: wp(5),
    borderRadius: wp(2.5),
    alignItems: 'center',
    justifyContent: 'center',
  },

});

export default ChatListItem;