import React, { Component } from 'react';
import { StyleSheet, Image, View, TouchableOpacity, Modal, ScrollView, Keyboard, KeyboardAvoidingView, Alert, BackHandler, Platform, PermissionsAndroid } from 'react-native';
import FontText from '../../components/FontText';
import colors from '../../assets/colors';
import { isX, normalize, hp, wp, isAndroid, isIOS } from '../../helper/responsiveScreen';
import Header from './Header'
import Bottom from './Bottom'
import Bookmark from './Bookmark'
import SmartScrollView from '../../components/SmartScrollView';
import Constant from '../../helper/appConstant';
import MessageList from '../../components/MessageList';
import ImagePicker from 'react-native-image-crop-picker';
import EmojiSelector, { Categories } from "react-native-emoji-selector";
import io from 'socket.io-client';
import AsyncStorage from '@react-native-community/async-storage';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { chatActions, walletActions } from '../../actions/actions'
import Loader from '../../components/Loader';
import {
  addLabelsToParams, removeLabelsToParams, addNotesToParams, removeMessageToParams, editMessageToParams,
  imageSendToParams, audioSendToParams, fileSendToParams, getMediaToParams
} from '../../helper/toParams';
import { routeName as tabNavigationRouteName } from '../../navigation/TabNavigation'
import { resetNavigateTo } from '../../helper/navigationHelper';
import { timeAgoFromNow, time, validURL } from '../../util/validation';
import RNFetchBlob from 'react-native-fetch-blob';
import AudioRecorderPlayer, {
  AVEncoderAudioQualityIOSType,
  AVEncodingOption,
  AudioEncoderAndroidType,
  AudioSet,
  AudioSourceAndroidType,
} from 'react-native-audio-recorder-player';
import Sound from 'react-native-sound';
import DocumentPicker from 'react-native-document-picker';
import FileViewer from "react-native-file-viewer";

const socket = io('https://heywallet.axislabs.company');

export const routeName = 'chatDetail';

class ChatDetail extends Component {

  constructor(props) {
    super(props)
    this.state = {
      // data: props.route.params && props.route.params.data,
      isBookMark: false,
      notes: '',
      labelData: [],
      isAddLabel: false,
      value: '',
      messageData: [],
      message: [],
      isAttachment: false,
      isEmoji: false,
      address: props.route.params && props.route.params.address,
      token: '',
      chain: props.route.params && props.route.params.chain,
      id: props.route.params && props.route.params.id,
      chatReadData: [],
      chatDeliverData: [],
      lastSeen: '',
      timerId: '',
      isEditView: false,
      txtEditMsg: '',
      editMSGID: '',
      isFullImage: false,
      fullImageData: '',
      isSelectImage: false,
      selectImageData: '',
      isLoading: false,
      recordSecs: 0,
      recordTime: '00:00',
      isGranted: false,
      isRecord: false,
      isAudioView: false,
      audioFile: '',
      playTime: '00:00',
      duration: '00:00',
      isPlay: '',
      playSeconds: 0,
      duration: 0,
      isPause: false,
      audioLoading: '',
      isEditNotes: false,
      previousNotes: '',
      allDownloadFile: [],
      allDownloadImages: [],
      getMediaData: [],
      getFileData: [],
      getLinksData: [],
      getAudioData: [],
      isBookContinue: false,
      isSending: false,
      projectName: this.props.route.params && this.props.route.params.projectName,
      walletId: ''
    }
    this.sliderEditing = false;
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this)
    this.audioRecorderPlayer = new AudioRecorderPlayer();
  }

  async componentDidMount() {
    const { chat, chatThreadData } = this.props
    const { address, id } = this.state
    if (isAndroid) {
      setTimeout(() => {
        this.requestStoragePermission();
      }, 500);
    }
    let selectAddress = JSON.parse(await AsyncStorage.getItem('address'))
    let walletData = JSON.parse(await AsyncStorage.getItem('check wallet data')).find(x => x.address === selectAddress);

    let labelData = chatThreadData.label.find(x => x.threadId == id);
    let notesData = chatThreadData.notes.find(x => x.threadId == id);

    if (labelData) {
      this.setState({ labelData: labelData.label })
    }

    if (notesData) {
      this.setState({ notes: notesData.notes[ 0 ].text, previousNotes: notesData.notes[ 0 ].text })
    }

    socket.emit("joinRoom", walletData.token, user => {
      console.log('user', user);
    });


    socket.emit("receiver_lastseen", address, chat => {
      console.log('receiver_lastseen', chat);
      this.setState({ lastSeen: chat.receiverWallet.lastSeen })
      console.log('time....', timeAgoFromNow(chat.receiverWallet.lastSeen))
    });

    let timerId = setInterval(() => {
      socket.emit("receiver_lastseen", address, chat => {
        console.log('receiver_lastseen', chat);
        this.setState({ lastSeen: chat.receiverWallet.lastSeen })
        console.log('time....', timeAgoFromNow(chat.receiverWallet.lastSeen))
      });
    }, 15000);
    this.setState({ timerId: timerId, token: walletData.token, walletId: walletData.id })

    const data = {
      token: walletData.token,
      threadId: id
    }
    chat(data)

    socket.on('chat_receive', this.onSocketMessageHandler);
    socket.on("ack_deliver", this.onSocketAckDeliverHandler)
    socket.on("ack_read", this.onSocketAckReadHandler)
    socket.on("device_chat_update", this.onSocketChatUpdateHandler)

    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    )

    this.timeout = setInterval(() => {
      if (this.sound && this.sound.isLoaded() && this.state.isPlay && !this.sliderEditing) {
        this.sound.getCurrentTime((seconds, isPlaying) => {
          this.setState({ playSeconds: seconds });
        })
      }
    }, 500);

    this.getDownloadFile('File')
    setTimeout(async () => {
      this.getDownloadFile('Images')
    }, 2000);

  }

  async requestStoragePermission() {
    try {
      const granted = await PermissionsAndroid.requestMultiple(
        [ PermissionsAndroid.PERMISSIONS.RECORD_AUDIO ],
      );
      console.log('granted...', granted, Object.values(granted)[ 0 ], PermissionsAndroid.RESULTS.GRANTED)
      if (Object.values(granted)[ 0 ] === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('granted.....')
        this.setState({ isGranted: true })
      } else {
        console.log('denied.....')
      }
    } catch (err) {
      console.warn(err);
    }
  }

  handleBackButtonClick = () => {
    this.onBackPress()
    return true;
  }

  componentWillUnmount() {
    socket.off('chat_receive', this.onSocketMessageHandler);
    socket.off("ack_deliver", this.onSocketAckDeliverHandler)
    socket.off("ack_read", this.onSocketAckReadHandler)
    socket.off("device_chat_update", this.onSocketChatUpdateHandler)
    clearInterval(this.state.timerId)
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    )

    if (this.sound) {
      this.sound.release();
      this.sound = null;
    }
    if (this.timeout) {
      clearInterval(this.timeout);
    }
  }

  onSocketMessageHandler = (data) => {
    console.log('onSocketMessageHandler', data)
    socket.emit("ack_read", data._id, chat => {
      // console.log('ack_read...', chat);
    });
    this.state.messageData.push(data);
    this.forceUpdate()
  }

  onSocketAckDeliverHandler = (data) => {
    // console.log('onSocketAckDeliverHandler', data)
    this.state.chatDeliverData.push(data.data)
    this.forceUpdate()
  }

  onSocketAckReadHandler = (data) => {
    // console.log('onSocketAckReadHandler', data)
    this.state.chatReadData.push(data.data)
    this.forceUpdate()
  }

  onSocketChatUpdateHandler = (data) => {
    console.log('onSocketChatUpdateHandler', data)
    this.state.messageData.push(data);
    this.forceUpdate()
  }

  componentDidUpdate(prevProps) {
    const { processing, error, success, chatData, addLabelData, addLabelSuccess, removeLabelData, addNotesData, iseditSuccess,
      imageSendData, audioData, fileData, getMediaData, getFileData, getAudioData, getLinksData, } = this.props;
    const { address, message, token, id } = this.state;

    if (prevProps.chatData !== chatData) {
      if (chatData.success) {
        console.log('chatData', chatData)


        chatData.data.map((item, i) => {
          if (item.senderAddress == address && !item.chatRead) {
            socket.emit("ack_read", item._id, chat => {
              // console.log('ack_read...!!!!!!!!!', chat);
            });
          }

        })

        this.setState({ messageData: chatData.data })
      } else {
        alert(chatData.message)
      }
    } else if (prevProps.addLabelData !== addLabelData) {
      if (addLabelData.success) {
        this.state.labelData.push(addLabelData.data);
        this.forceUpdate()
      } else {
        alert(addLabelData.message)
      }
    } else if (prevProps.removeLabelData !== removeLabelData) {
      if (removeLabelData.success) {
        console.log('removeLabelData', removeLabelData)
        this.setState({ value: '' })
      } else {
        alert(removeLabelData.message)
      }
    } else if (prevProps.addNotesData !== addNotesData) {
      this.setState({ isEditNotes: false })
      if (addNotesData.success) {
        this.setState({ previousNotes: this.state.notes })
        console.log('addNotesData', addNotesData)
      } else {
        this.setState({ notes: this.state.previousNotes })
        alert(addNotesData.message)
      }
    } else if (prevProps.iseditSuccess !== iseditSuccess && iseditSuccess) {
      this.setState({ isEditView: false, txtEditMsg: '', message: '', editMSGID: '' })
    } else if (prevProps.imageSendData !== imageSendData) {
      if (imageSendData.success) {
        console.log('imageSendData..........', imageSendData, message)
        if (message != '') {
          const payload = { token: token, address: address, text: message }
          console.log('payload!!!!!!!', payload)
          socket.emit("chat", payload, chat => {
            console.log('chat!!!!!!!', chat);
            this.setState({ id: chat.data.threadId, message: '' })
          });

        }
      } else {
        alert(imageSendData.message)
      }
    } else if (prevProps.audioData !== audioData) {
      if (audioData.success) {
        console.log('audioData..........', audioData)
        this.setState({ isAudioView: false, audioFile: '' })
      } else {
        alert(audioData.message)
      }
    } else if (prevProps.fileData !== fileData) {
      if (fileData.success) {
        console.log('fileData..........', fileData)
        this.setState({ isAudioView: false, audioFile: '' })
      } else {
        alert(fileData.message)
      }
    }
    else if (prevProps.getMediaData !== getMediaData) {
      if (getMediaData.success) {
        console.log('getMediaData..........', getMediaData)
        this.setState({ getMediaData: getMediaData })
      } else {
        alert(getMediaData.message)
      }
    } else if (prevProps.getFileData !== getFileData) {
      if (getFileData.success) {
        console.log('getFileData..........', getFileData)
        this.setState({ getFileData: getFileData })
      } else {
        alert(getFileData.message)
      }
    } else if (prevProps.getAudioData !== getAudioData) {
      if (getAudioData.success) {
        console.log('getAudioData..........', getAudioData)
        this.setState({ getAudioData: getAudioData })
      } else {
        alert(getAudioData.message)
      }
    } else if (prevProps.getLinksData !== getLinksData) {
      if (getLinksData.success) {
        console.log('getLinksData..........', getLinksData)
        this.setState({ getLinksData: getLinksData })
      } else {
        alert(getLinksData.message)
      }
    }
    else if (prevProps.error !== error) {
      this.setState({ message: '' })
    }
  }

  onBackPress = () => {
    const { navigation, route } = this.props
    // navigation.goBack()
    if (this.sound) {
      this.sound.stop();
      this.setState({ isPlay: '', playSeconds: 0 });
      this.sound.setCurrentTime(0);
      this.sound.release();
      this.sound = null;
    }
    if (this.timeout) {
      clearInterval(this.timeout);
    }

    const data = { tabTitle: route.params && route.params.tabTitle }
    resetNavigateTo(navigation, tabNavigationRouteName, data)
  }

  onBookmarkPress = () => {
    const { getMedia, getFile, getAudio, getLinks } = this.props
    const { id, token } = this.state
    getMedia(getMediaToParams(this.props, { id, token }));
    getFile(getMediaToParams(this.props, { id, token }));
    getAudio(getMediaToParams(this.props, { id, token }));
    getLinks(getMediaToParams(this.props, { id, token }));
    this.setState({ isBookMark: true })
  }

  onCancelPress = () => {
    const { notes, isEditNotes, previousNotes } = this.state
    const { addNotes } = this.props
    console.log('notes...', notes)
    if (isEditNotes) {
      if (notes != '') {
        if (notes != previousNotes) {
          addNotes(addNotesToParams(this.props, this.state))
        } else {
          this.setState({ isEditNotes: false })
        }
      } else {
        // alert('Please Enter Notes')
        addNotes(addNotesToParams(this.props, this.state))
      }
    }
    this.setState({ isBookMark: false, isEditNotes: false })
  }

  onInputChange = name => value => {
    this.setState({ [ name ]: value });
  }

  onLabelRemovePress = (index) => {
    const { removeLabels } = this.props

    Alert.alert(
      "",
      "Are you sure to remove label?",
      [
        {
          text: "Yes", onPress: () => {
            console.log('label...', this.state.labelData[ index ])
            this.setState({ value: this.state.labelData[ index ].text },
              () => {
                this.state.labelData.splice(index, 1);
                this.forceUpdate()
                removeLabels(removeLabelsToParams(this.props, this.state))
              })
          }
        },
        {
          text: "No",
          onPress: () => console.log("Cancel Pressed"),
        }
      ],
      { cancelable: false }
    );
  }

  onAddLabelPress = () => {
    this.setState({ isAddLabel: true })
  }

  onLableCancelPress = () => {
    this.setState({ isAddLabel: false, value: '' })
  }

  onAddPress = () => {
    const { value } = this.state
    const { addLabels } = this.props
    if (value != '') {
      this.setState({ isAddLabel: false })
      addLabels(addLabelsToParams(this.props, this.state))
      this.setState({ value: '' })
    } else {
      this.setState({ isAddLabel: false, value: '' })
      // alert('Please Add Label')
    }
  }

  onEmojiPress = () => {
    this.setState({ isEmoji: !this.state.isEmoji })
    Keyboard.dismiss()
  }

  onAttachmentPress = () => {
    this.setState({ isAttachment: true })
  }

  onSendPress = () => {
    const { address, message, token, isEditView, editMSGID, isSelectImage, selectImageData } = this.state
    const { chatThread, editMessage } = this.props

    console.log('check url ', validURL(message))
    if (isSelectImage) {
      this.uploadImages(selectImageData)
      this.setState({ isSelectImage: false, selectImageData: '' })
    } else {

      if (isEditView) {
        console.log('edit data.....', message)
        let id = editMSGID
        let value = message
        editMessage(editMessageToParams(this.props, { id, value, token }))
      }
      else {
        if (message != '') {
          if (!this.state.isSending) {
            let payload
            if (validURL(message)) {
              payload = { token: token, address: address, link: message }
              this.setState({ isSending: true })
            } else {
              payload = { token: token, address: address, text: message }
              this.setState({ isSending: true })
            }

            console.log('payload', payload)
            socket.emit("chat", payload, chat => {
              console.log('chat', chat);

              this.setState({ id: chat.data.threadId, message: '', isSending: false })
            });
          }
        }
      }
    }

  }

  onAttachmentCancelPress = () => {
    this.setState({ isAttachment: false })
  }

  onCameraPress = () => {
    this.setState({ isAttachment: false })
    setTimeout(() => {
      ImagePicker.openCamera({
        mediaType: "photo",
        compressImageQuality: 0.9,
      }).then((photo) => {
        this.setState({ isSelectImage: true, selectImageData: photo })
      })
    }, 100);
  }

  onGalleryPress = () => {

    this.setState({ isAttachment: false })
    setTimeout(() => {
      ImagePicker.openPicker({
        mediaType: "photo",
        compressImageQuality: 0.9,
      }).then((photo) => {
        this.setState({ isSelectImage: true, selectImageData: photo })
      })
    }, 100);

  }

  uploadImages = async (res) => {
    const { token, address } = this.state
    const { imageSend } = this.props;

    let uri = isAndroid ? res.path : res.sourceURL;
    let name = isAndroid ? res.path.split('/').splice(-1)[ 0 ] : res.sourceURL.split('/').splice(-1)[ 0 ];
    let type = res.mime
    imageSend(imageSendToParams(this.props, { uri, name, type, token, address }));

  }

  onSavePress = () => {
    const { notes, isEditNotes, previousNotes } = this.state
    const { addNotes } = this.props
    if (isEditNotes) {
      if (notes != '') {
        if (notes != previousNotes) {
          addNotes(addNotesToParams(this.props, this.state))
        } else {
          this.setState({ isEditNotes: false })
        }
      } else {
        addNotes(addNotesToParams(this.props, this.state))
        // alert('Please Enter Notes')
      }
    }
  }

  removePress = (item) => {
    const { removeMessage } = this.props;
    const { token } = this.state;
    Alert.alert(
      "",
      'Are you sure to remove ' + (item.image.length != 0 ? 'image?' : item.file ? 'file?' : 'Message?'),
      [
        {
          text: "Yes", onPress: () => {
            let id = item._id
            removeMessage(removeMessageToParams(this.props, { id, token }))
          }
        },
        {
          text: "No",
          onPress: () => console.log("Cancel Pressed"),
        }
      ],
      { cancelable: false }
    );
  }

  editPress = (item) => {
    this.setState({ isEditView: true, txtEditMsg: item.text, message: item.text, editMSGID: item._id })
  }

  onCloseEditMsg = () => {
    this.setState({ isEditView: false, txtEditMsg: '', message: '', editMSGID: '' })
  }

  imagePress = (item, value) => {
    this.setState({ isFullImage: true, fullImageData: item, isBookMark: false, isEditNotes: false, isBookContinue: value })
  }

  onFileDownloadPress = (data, value) => {
    this.setState({ isLoading: true })
    let pathToWrite = '';
    let fileName = '', file = '';
    if (value == 'File') {
      fileName = data.split('/').splice(-1)[ 0 ]
      file = data
    } else {
      fileName = data.image[ 0 ].split('/').splice(-1)[ 0 ]
      file = data.image[ 0 ]
    }

    if (isAndroid) {
      let path = RNFetchBlob.fs.dirs.DCIMDir
      path = path.substring(0, path.length - 5);
      if (value == 'File') {
        pathToWrite = `${path}/HeyWallet/File/${fileName}`;
      } else {
        pathToWrite = `${path}/HeyWallet/Images/${fileName}`;
      }
    }
    else {
      if (value == 'File') {
        pathToWrite = `${RNFetchBlob.fs.dirs.DocumentDir}/HeyWallet/File/${fileName}`;
      } else {
        pathToWrite = `${RNFetchBlob.fs.dirs.DocumentDir}/HeyWallet/Images/${fileName}`;
      }
    }
    console.log('pathToWrite', pathToWrite)
    RNFetchBlob
      .config({
        path: pathToWrite,
        fileCache: true,
        addAndroidDownloads: {
          useDownloadManager: true,
          title: fileName,
          description: 'An HeyWallet Download Progress.',
          // mime: value == 'file' ? 'application/pdf' : 'image/png',
          path: pathToWrite,
          mediaScannable: true,
          notification: false,
        }
      })
      .fetch('GET', file, {
        //some headers ..
      })
      .then((res) => {
        // the temp file path
        this.getDownloadFile(value)

        this.setState({ isLoading: false }, () => {
          setTimeout(() => {
            Alert.alert("HeyWallet",
              'Download complete',
              [
                { text: 'OK', onPress: () => console.log('') },
              ],
              { cancelable: false }
            )
          }, 100);

        })
      })
      .catch((err) => {
        // scan file error
        console.log('err...', err)
        this.setState({ isLoading: false })
      })
  }

  onStartRecorder = async () => {
    const path = Platform.select({
      ios: 'audio.m4a',
      android: 'sdcard/audio.mp3',
    });
    const audioSet = {
      AudioEncoderAndroid: AudioEncoderAndroidType.AAC,
      AudioSourceAndroid: AudioSourceAndroidType.MIC,
      AVEncoderAudioQualityKeyIOS: AVEncoderAudioQualityIOSType.high,
      AVNumberOfChannelsKeyIOS: 2,
      AVFormatIDKeyIOS: AVEncodingOption.aac,
    };
    const uri = await this.audioRecorderPlayer.startRecorder(path, audioSet);
    this.audioRecorderPlayer.addRecordBackListener((e) => {
      let value = this.audioRecorderPlayer.mmssss(
        Math.floor(e.current_position),
      )
      var lastIndex = value.lastIndexOf(":");
      console.log('recordTime', value.substring(0, lastIndex))
      this.setState({ recordSecs: e.current_position, recordTime: value.substring(0, lastIndex), isRecord: true, isAudioView: false, audioFile: '' })
    });
  }

  onLongPress = async () => {
    if (isAndroid) {
      this.requestStoragePermission();
      if (this.state.isGranted) {
        this.onStartRecorder()
      }
    } else {
      this.onStartRecorder()
    }
  }

  onPressOut = async () => {
    console.log('pressOut....')
    const result = await this.audioRecorderPlayer.stopRecorder();
    this.audioRecorderPlayer.removeRecordBackListener();
    this.setState({
      recordSecs: 0,
      isRecord: false
    });
    console.log('result', result, result.split('/').splice(-1)[ 0 ]);
    if (result.includes(".mp3") || result.includes(".m4a")) {

      this.setState({ isAudioView: true, audioFile: result })
    }
  }

  onCloseAudioView = () => {
    this.setState({ isAudioView: false, audioFile: '' })
  }

  onAudioSendPress = () => {
    const { token, address, audioFile, recordTime } = this.state
    const { audioSend } = this.props;

    audioSend(audioSendToParams(this.props, { audioFile, token, address, recordTime }));
  }

  audioPress = async (url) => {

    console.log('sound.....', this.sound)
    this.setState({ isPause: false, audioLoading: url });
    if (this.sound) {
      if (this.sound._filename == url) {
        this.sound.play(this.playComplete);
        this.setState({ isPlay: url, audioLoading: '' });
      } else {
        this.sound.stop();
        this.setState({ isPlay: '', playSeconds: 0 });
        this.sound.setCurrentTime(0);

        this.playSound(url)
      }
    } else {
      this.playSound(url)
    }
  }

  playSound = (url) => {
    this.sound = new Sound(url, '', (error) => {
      if (error) {
        console.log('failed to load the sound', error);
        this.setState({ isPlay: '', audioLoading: '' });
      } else {
        this.setState({ isPlay: url, duration: this.sound.getDuration(), audioLoading: '' });
        this.sound.play(this.playComplete);
      }
    });

  }

  playComplete = (success) => {
    if (this.sound) {
      if (success) {
        console.log('successfully finished playing');
      } else {
        console.log('playback failed due to audio decoding errors');
      }
      this.setState({ isPlay: '', playSeconds: 0 });
      this.sound.setCurrentTime(0);
    }
  }

  audioPausePress = async () => {
    if (this.sound) {
      this.sound.pause();
    }
    this.setState({ isPause: true });
  }

  getAudioTimeString(seconds) {
    const m = parseInt(seconds % (60 * 60) / 60);
    const s = parseInt(seconds % 60);

    return ((m < 10 ? '0' + m : m) + ':' + (s < 10 ? '0' + s : s));
  }

  onSliderEditStart = () => {
    if (this.state.isPlay)
      this.sliderEditing = true;
  }

  onSliderEditEnd = () => {
    this.sliderEditing = false;
  }

  onSliderEditing = value => {
    if (this.sound) {
      this.sound.setCurrentTime(value);
      this.setState({ playSeconds: value });
    }
  }

  onEditNotesPress = () => {
    this.setState({ isEditNotes: true })
  }

  onDocumentPress = async () => {
    this.setState({ isAttachment: false })
    setTimeout(async () => {
      try {
        const { token, address } = this.state
        const { fileSend } = this.props;

        const res = await DocumentPicker.pick({
          type: [ DocumentPicker.types.pdf, DocumentPicker.types.doc, DocumentPicker.types.docx ],
        });

        let uri = res.uri;
        let name = res.name;
        let type = res.type
        let fileSize = (res.size / Math.pow(1024, 2)).toFixed(2) + 'MB'
        fileSend(fileSendToParams(this.props, { uri, name, type, token, address, fileSize }));
      } catch (err) {
        console.log('err', err)
      }
    }, 100);
  }

  getDownloadFile = (value) => {
    console.log('value........................', value)
    let pathToWrite = ''
    if (isIOS) {
      pathToWrite = `${RNFetchBlob.fs.dirs.DocumentDir}/HeyWallet/${value}/`;
    }
    else {
      let path = RNFetchBlob.fs.dirs.DCIMDir
      path = path.substring(0, path.length - 5);
      pathToWrite = `${path}/HeyWallet/${value}/`;
    }
    RNFetchBlob.fs.ls(pathToWrite)
      .then((files) => {
        // allFiles = files
        if (value == 'File') {
          this.setState({ allDownloadFile: files })
        } else {
          this.setState({ allDownloadImages: files })
        }

      })
  }

  onFilePress = async (value) => {
    let pathToWrite = ''
    if (isIOS) {
      pathToWrite = `${RNFetchBlob.fs.dirs.DocumentDir}/HeyWallet/File/${value.split('/').splice(-1)[ 0 ]}`;
    }
    else {
      let path = RNFetchBlob.fs.dirs.DCIMDir
      path = path.substring(0, path.length - 5);
      pathToWrite = `${path}/HeyWallet/File/${value.split('/').splice(-1)[ 0 ]}`;
    }
    console.log('pathToWrite', pathToWrite)
    await FileViewer.open(pathToWrite);
  }

  onSubscribePress = (dappId, type) => {
    const { subscribeDapp } = this.props
    const { token, walletId, id } = this.state

    const data = {
      token: token,
      dappId: dappId,
      type: type,
      walletId: walletId,
    }
    subscribeDapp(data)
  }

  render() {
    const { isBookMark, notes, labelData, isAddLabel, value, messageData, message, isAttachment, isEmoji, address, chain, chatReadData, chatDeliverData, lastSeen, isEditView, txtEditMsg, isFullImage, fullImageData, isSelectImage, selectImageData, isLoading, recordTime, isRecord, isAudioView, audioFile, isPlay, duration, playSeconds, isPause, audioLoading, isEditNotes, allDownloadFile, allDownloadImages, getMediaData, getFileData, getAudioData, getLinksData, isBookContinue, projectName, id, walletId } = this.state
    const { navigation, processing, loading, chatThreadData, subscribeProcessing } = this.props

    const currentTimeString = this.getAudioTimeString(playSeconds);
    const durationString = this.getAudioTimeString(duration);

    console.log('currentTimeString', currentTimeString, durationString, playSeconds)

    let image
    if (chain == "ETH") {
      image = require('../../assets/images/eth_image.png')
    } else if (chain == "BTC") {
      image = require('../../assets/images/btc_image.png')
    } else if (chain == "BSC") {
      image = require('../../assets/images/bsc_image.png')
    } else if (chain == "DOT") {
      image = require('../../assets/images/dot_image.png')
    } else if (chain == "SOL") {
      image = require('../../assets/images/solana_icon.png')
    }

    if (this.props.route.params && this.props.route.params.projectLogo) {
      image = { uri: this.props.route.params.projectLogo }
    } else if (this.props.route.params && (this.props.route.params.projectLogo == null || this.props.route.params.projectLogo == undefined)) {
      image = require('../../assets/images/dapps_placeholder.png')
    }

    let isDownloadImage = false
    if (fullImageData && allDownloadImages.length != 0) {
      let exitData = allDownloadImages.find(x => x === fullImageData.image[ 0 ].split('/').splice(-1)[ 0 ])
      if (exitData) {
        console.log('exitData', exitData)
        isDownloadImage = true
      }
    }

    return (
      <View style={{ ...styles.container, paddingHorizontal: wp(5) }}>

        <KeyboardAvoidingView style={{ flex: 1 }}
          behavior='padding'
          keyboardVerticalOffset={isAndroid && hp(-100)}
        >

          <Header
            image={image}
            title={projectName ? projectName : address}
            onBackPress={() => this.onBackPress()}
            onBookmarkPress={() => this.onBookmarkPress()}
            lastSeen={lastSeen != '' ? timeAgoFromNow(lastSeen) : ''}
          />

          <MessageList
            extraData={this.state}
            messageData={messageData}
            socket={socket}
            address={address}
            navigation={navigation}
            chatReadData={chatReadData}
            chatDeliverData={chatDeliverData}
            removePress={(item) => this.removePress(item)}
            editPress={(item) => this.editPress(item)}
            imagePress={(item) => this.imagePress(item, false)}
            audioPress={(item) => this.audioPress(item)}
            isPlay={isPlay}
            audioPausePress={() => this.audioPausePress()}
            currentTimeString={currentTimeString}
            durationString={durationString}
            playSeconds={playSeconds}
            duration={duration}
            onSliderEditStart={() => this.onSliderEditStart()}
            onSliderEditEnd={() => this.onSliderEditEnd()}
            onSliderEditing={(value) => this.onSliderEditing(value)}
            isPause={isPause}
            audioLoading={audioLoading}
            onFileDownloadPress={(value) => this.onFileDownloadPress(value, 'File')}
            allDownloadFile={allDownloadFile}
            onFilePress={(value) => this.onFilePress(value)} />

          <Bookmark
            isBookMark={isBookMark}
            isAddLabel={isAddLabel}
            value={value}
            onCancelPress={() => this.onCancelPress()}
            notes={notes}
            labelData={labelData}
            onInputChange={(value) => this.onInputChange(value)}
            onLabelRemovePress={(index) => this.onLabelRemovePress(index)}
            onAddLabelPress={() => this.onAddLabelPress()}
            onLableCancelPress={() => this.onLableCancelPress()}
            onAddPress={() => this.onAddPress()}
            onSavePress={() => this.onSavePress()}
            onEditNotesPress={() => this.onEditNotesPress()}
            isEditNotes={isEditNotes}
            processing={processing}
            getMediaData={getMediaData}
            getFileData={getFileData}
            getAudioData={getAudioData}
            getLinksData={getLinksData}
            imagePress={(item) => this.imagePress(item, true)}
            onFileDownloadPress={(value) => this.onFileDownloadPress(value, 'File')}
            allDownloadFile={allDownloadFile}
            onFilePress={(value) => this.onFilePress(value)}
            isLoading={isLoading}
            audioPress={(item) => this.audioPress(item)}
            isPlay={isPlay}
            audioPausePress={() => this.audioPausePress()}
            currentTimeString={currentTimeString}
            durationString={durationString}
            playSeconds={playSeconds}
            duration={duration}
            onSliderEditStart={() => this.onSliderEditStart()}
            onSliderEditEnd={() => this.onSliderEditEnd()}
            onSliderEditing={(value) => this.onSliderEditing(value)}
            isPause={isPause}
            audioLoading={audioLoading}
            loading={loading}
            chatThreadData={chatThreadData}
            address={address}
            id={id}
            walletId={walletId}
            subscribeProcessing={subscribeProcessing}
            onSubscribePress={(id, type) => this.onSubscribePress(id, type)}
          />
          <View>
            {
              isEditView &&
              <View style={styles.editViewContainer}>
                <FontText size={normalize(16)} name={'archivo-regular'} color="white" style={{ fontWeight: '400' }} >{txtEditMsg}</FontText>
                <TouchableOpacity onPress={() => this.onCloseEditMsg()} style={{ padding: wp(1.5), marginRight: wp(1) }}>
                  <Image source={require('../../assets/images/cancel.png')} resizeMode="contain" />
                </TouchableOpacity>
              </View>
            }

            {
              isAudioView &&
              <View style={{
                ...styles.editViewContainer, paddingVertical: hp(1)
              }}>
                <View style={styles.bottomChildContainer}>
                  <Image source={require('../../assets/images/audio_icon.png')} resizeMode="contain" style={{ marginRight: wp(3) }} />
                  <FontText size={normalize(16)} name={'archivo-regular'} color="white" style={{ fontWeight: '400' }} >{`${audioFile.split('/').splice(-1)[ 0 ]} (${recordTime})`}</FontText>
                </View>
                <View style={styles.bottomChildContainer}>
                  <TouchableOpacity onPress={() => this.onAudioSendPress()} style={{ padding: wp(1.5), marginRight: wp(3) }}>
                    <View style={{ ...styles.circleView, backgroundColor: colors.black }}>
                      <Image source={require('../../assets/images/send.png')} resizeMode="contain" style={{ tintColor: colors.white }} />
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this.onCloseAudioView()} style={{ padding: wp(1.5) }}>
                    <View style={{ ...styles.circleView, backgroundColor: colors.black }}>
                      <Image source={require('../../assets/images/cancel.png')} resizeMode="contain" />
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
            }


            <Bottom
              message={message}
              onEmojiPress={() => this.onEmojiPress()}
              onAttachmentPress={() => this.onAttachmentPress()}
              onSendPress={() => this.onSendPress()}
              onInputChange={(value) => this.onInputChange(value)}
              onFocus={() => this.setState({ isEmoji: false })}
              isSelectImage={isSelectImage}
              onPressOut={() => this.onPressOut()}
              onLongPress={() => this.onLongPress()}
              recordTime={recordTime}
              isRecord={isRecord}
              isAudioView={isAudioView}
            />
            {isEmoji &&
              <ScrollView style={{ height: hp(35), backgroundColor: colors.black }}>
                <EmojiSelector
                  category={Categories.emotion}
                  showTabs={true}
                  showSearchBar={false}
                  showHistory={true}
                  showSectionTitles={false}
                  columns={9}
                  onEmojiSelected={emoji => this.setState({ message: message + emoji })}
                />

              </ScrollView>
            }
          </View>

          <Modal
            transparent={true}
            animationType={'none'}
            onRequestClose={() => this.onAttachmentCancelPress()}
            visible={isAttachment} >
            <View style={styles.modelContainer}>
              <View style={{ ...styles.modelChildContainer, paddingBottom: hp(2), }}>
                <TouchableOpacity onPress={() => this.onAttachmentCancelPress()} style={styles.cancelView}>
                  <View style={styles.circleView}>
                    <Image source={require('../../assets/images/cancel.png')} resizeMode="contain" />
                  </View>
                </TouchableOpacity>

                <View style={styles.bottomView}>
                  <TouchableOpacity onPress={() => this.onCameraPress()}
                    style={{ ...styles.modelChildContainer, borderColor: colors[ 'gray-medium' ], marginRight: wp(3), paddingVertical: hp(1.5) }}>
                    <FontText size={normalize(14)} name={'archivo-regular'} color="text-light" style={{ fontWeight: '400' }} >{'Camera'}</FontText>
                  </TouchableOpacity>

                  <TouchableOpacity onPress={() => this.onGalleryPress()}
                    style={{ ...styles.modelChildContainer, borderColor: colors[ 'gray-medium' ], paddingVertical: hp(1.5), marginRight: wp(3), }}>
                    <FontText size={normalize(14)} name={'archivo-regular'} color="text-light" style={{ fontWeight: '400' }} >{'Gallery'}</FontText>
                  </TouchableOpacity>

                  <TouchableOpacity onPress={() => this.onDocumentPress()}
                    style={{ ...styles.modelChildContainer, borderColor: colors[ 'gray-medium' ], paddingVertical: hp(1.5) }}>
                    <FontText size={normalize(14)} name={'archivo-regular'} color="text-light" style={{ fontWeight: '400' }} >{'Document'}</FontText>
                  </TouchableOpacity>

                </View>
              </View>
            </View>
          </Modal>

          {fullImageData != '' &&
            <Modal
              transparent={true}
              animationType={'none'}
              onRequestClose={() => this.setState({ isFullImage: false, fullImageData: '', isBookMark: isBookContinue })}
              visible={isFullImage} >
              <View style={styles.fullImageContainer}>
                <Image
                  style={{ width: wp(100), height: hp(88) }}
                  resizeMode='cover'
                  source={{ uri: fullImageData.image[ 0 ] }}
                />
                <TouchableOpacity onPress={() => this.setState({ isFullImage: false, fullImageData: '', isBookMark: isBookContinue })}
                  style={styles.fullImageCancelView}>
                  <View style={styles.circleView}>
                    <Image source={require('../../assets/images/cancel.png')} resizeMode="contain" />
                  </View>
                </TouchableOpacity>

                <View style={styles.bottomContainer}>
                  <View style={styles.bottomChildContainer}>
                    <Image source={image} resizeMode="contain" />
                    <View style={styles.headerTitleView}>
                      <FontText lines={1} size={normalize(18)} name={'archivo-regular'} color="white" style={{ fontWeight: '700' }} >{address}</FontText>
                      <FontText lines={1} size={normalize(10)} name={'archivo-regular'} color="text-gray" style={{ fontWeight: '400' }} >{time(fullImageData.updatedAt)}</FontText>
                    </View>
                  </View>

                  <View style={styles.bottomChildContainer}>
                    <TouchableOpacity onPress={() => console.log('')} style={{ padding: wp(2), marginRight: wp(3) }}>
                      <Image source={require('../../assets/images/eye_icon.png')} resizeMode="contain" />
                    </TouchableOpacity>
                    {!isDownloadImage &&
                      <TouchableOpacity onPress={() => this.onFileDownloadPress(fullImageData, 'Images')} style={{ padding: wp(2) }}>
                        <Image source={require('../../assets/images/download_icon.png')} resizeMode="contain" />
                      </TouchableOpacity>
                    }
                  </View>
                </View>
                <Loader
                  loading={isLoading}
                />
              </View>
            </Modal>
          }

          <Modal
            transparent={true}
            animationType={'none'}
            onRequestClose={() => this.setState({ isSelectImage: false, selectImageData: '' })}
            visible={isSelectImage} >
            <View style={styles.container}>
              <KeyboardAvoidingView style={{ flex: 1 }}
                behavior='padding'
                keyboardVerticalOffset={isAndroid && hp(-100)}>
                <View style={styles.selectImageContainer}>
                  <Image
                    style={{ flex: 1 }}
                    resizeMode='cover'
                    source={{ uri: isAndroid ? selectImageData.path : selectImageData.sourceURL }}
                  />
                  <TouchableOpacity onPress={() => this.setState({ isSelectImage: false, selectImageData: '' })} style={styles.fullImageCancelView}>
                    <View style={styles.circleView}>
                      <Image source={require('../../assets/images/cancel.png')} resizeMode="contain" />
                    </View>
                  </TouchableOpacity>
                </View>

                <View style={{ paddingHorizontal: wp(5) }}>

                  <Bottom
                    message={message}
                    onEmojiPress={() => this.onEmojiPress()}
                    onAttachmentPress={() => this.onAttachmentPress()}
                    onSendPress={() => this.onSendPress()}
                    onInputChange={(value) => this.onInputChange(value)}
                    onFocus={() => this.setState({ isEmoji: false })}
                    isSelectImage={isSelectImage}
                    onPressOut={() => console.log('')}
                    onLongPress={() => console.log('')}
                  />
                  {isEmoji &&
                    <ScrollView style={{ height: hp(35), backgroundColor: colors.black }}>
                      <EmojiSelector
                        category={Categories.emotion}
                        showTabs={true}
                        showSearchBar={false}
                        showHistory={true}
                        showSectionTitles={false}
                        columns={9}
                        onEmojiSelected={emoji => this.setState({ message: message + emoji })}
                      />

                    </ScrollView>
                  }
                </View>
              </KeyboardAvoidingView>
            </View>
          </Modal>

          <Loader
            loading={processing}
          />
          {fullImageData == '' &&
            <Loader
              loading={isLoading}
            />
          }

        </KeyboardAvoidingView>
      </View>
    );
  }
}

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    // paddingVertical: hp(1),
    backgroundColor: colors.black
  },
  modelContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(52, 52, 52, 0.5)',
    paddingHorizontal: wp(5),
  },
  modelChildContainer: {
    paddingHorizontal: wp(5),
    borderRadius: wp(5),
    borderWidth: 1,
    backgroundColor: colors.black,
    borderColor: colors[ 'border-light' ]
  },
  cancelView: {
    alignSelf: 'flex-end',
    padding: wp(1.5),
    marginVertical: hp(1)
  },
  circleView: {
    width: wp(8),
    height: wp(8),
    borderRadius: wp(4),
    backgroundColor: colors[ 'gray-light' ],
    justifyContent: 'center',
    alignItems: 'center',
  },
  bottomView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: hp(1)
  },
  fullImageContainer: {
    flex: 1,
    paddingTop: isX ? hp(4) : hp(0),
    backgroundColor: colors.black,
  },
  fullImageCancelView: {
    position: 'absolute',
    top: 0,
    right: 0,
    padding: wp(1.5),
    marginTop: isX ? hp(5) : hp(1),
    marginRight: wp(3),
  },
  bottomContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: wp(4)
  },
  bottomChildContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  headerTitleView: {
    marginLeft: wp(3),
    width: wp(50),
  },
  selectImageContainer: {
    flex: 1,
    paddingTop: isX ? hp(4) : hp(0),
    backgroundColor: colors.black,
  },
  editViewContainer: {
    paddingVertical: hp(2),
    paddingHorizontal: wp(5),
    marginHorizontal: wp(-5),
    flexDirection: 'row',
    backgroundColor: colors[ 'gray-medium' ],
    alignItems: 'center',
    justifyContent: 'space-between'
  },
});

const mapStateToProps = state => {
  const {
    chat: {
      processing,
      error,
      success,
      chatData,
      chatThreadData,
      addLabelData,
      addLabelSuccess,
      removeLabelData,
      addNotesData,
      iseditSuccess,
      imageSendData,
      audioData,
      fileData,
      getMediaData,
      getFileData,
      getAudioData,
      getLinksData,
      loading,
      subscribeProcessing
    },
  } = state;
  return {
    processing,
    error,
    success,
    chatData,
    chatThreadData,
    addLabelData,
    addLabelSuccess,
    removeLabelData,
    addNotesData,
    iseditSuccess,
    imageSendData,
    audioData,
    fileData,
    getMediaData,
    getFileData,
    getAudioData,
    getLinksData,
    loading,
    subscribeProcessing
  };
}

const mapDispatchToProps = dispatch => bindActionCreators({

  chat: chatActions.chat,
  addLabels: chatActions.addLabels,
  chatThread: chatActions.chatThread,
  removeLabels: chatActions.removeLabels,
  addNotes: chatActions.addNotes,
  removeMessage: chatActions.removeMessage,
  editMessage: chatActions.editMessage,
  imageSend: chatActions.imageSend,
  audioSend: chatActions.audioSend,
  fileSend: chatActions.fileSend,
  getMedia: chatActions.getMedia,
  getFile: chatActions.getFile,
  getAudio: chatActions.getAudio,
  getLinks: chatActions.getLinks,
  subscribeDapp: walletActions.subscribeDapp
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ChatDetail);