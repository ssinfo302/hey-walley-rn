import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import FontText from '../../components/FontText';
import { hp, isX, wp, normalize } from '../../helper/responsiveScreen';
import colors from '../../assets/colors';
import Input from '../../components/Input';

class Bottom extends Component {

  render() {
    const { message, onEmojiPress, onInputChange, onAttachmentPress, onSendPress, onFocus, isSelectImage, onLongPress, onPressOut, recordTime, isRecord,
      isAudioView } = this.props;
    return (
      <View style={styles.bottomView}>
        {isRecord ?
          <View style={{ marginLeft: wp(3), flexDirection: 'row', alignItems: 'center' }}>
            <Image style={{ width: wp(4), height: wp(4), borderRadius: wp(2), backgroundColor: colors.red, marginRight: wp(2) }} />
            <FontText size={normalize(16)} name={'archivo-regular'} color="text-light" style={{ fontWeight: '400' }} >{recordTime}</FontText>
          </View>
          :
          !isAudioView ?
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <TouchableOpacity onPress={() => onEmojiPress()} style={styles.touchView}>
                <Image source={require('../../assets/images/emoji.png')} resizeMode="contain" style={styles.logoImage} />
              </TouchableOpacity>
              <View style={styles.horizontalView} />
              <Input
                placeholder={'Type something ...'}
                defaultValue={message}
                keyboardType={'default'}
                returnKeyType={'done'}
                blurOnSubmit={true}
                onChangeText={onInputChange('message')}
                fontSize={normalize(18)}
                inputStyle={[
                  styles.input,
                ]}
                placeholderTextColor="text-light"
                color="text-light"
                onFocus={() => onFocus()}
              />
            </View>
            :
            <View />
          // </View>
        }

        <View style={{ flexDirection: 'row', alignItems: 'center' }}>

          {!isRecord ?
            !isAudioView &&
            <TouchableOpacity onPress={() => onAttachmentPress()} style={{ ...styles.touchView, marginRight: wp(1) }}>
              <Image source={require('../../assets/images/attachment.png')} resizeMode="contain" style={styles.logoImage} />
            </TouchableOpacity>
            : null
          }

          <TouchableOpacity
            onPress={() => (message.length != 0 || isSelectImage) ? onSendPress() : alert('Hold to record, release to send')} style={styles.touchView}
            onLongPress={() => (message.length != 0 || isSelectImage) ? null : onLongPress()}
            onPressOut={() => onPressOut()}>
            <View style={styles.circleView}>
              <Image source={(message.length != 0 || isSelectImage) ? require('../../assets/images/send.png') : require('../../assets/images/audio_icon.png')} resizeMode="contain" />
            </View>
          </TouchableOpacity>
        </View>

      </View>


    )
  }
}

const styles = StyleSheet.create({
  bottomView: {
    paddingVertical: hp(1.5),
    // paddingHorizontal: wp(4),
    flexDirection: 'row',
    backgroundColor: colors.black,
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  touchView: {
    padding: wp(1.5),
  },
  horizontalView: {
    width: 1,
    backgroundColor: colors.blue,
    height: hp(3.5),
    marginHorizontal: wp(1.5)
  },
  input: {
    width: wp(57),
    fontWeight: '500',
    backgroundColor: colors.transparent,
    paddingLeft: 0,
    paddingTop: 0,
  },
  circleView: {
    width: wp(10),
    height: wp(10),
    borderRadius: wp(5),
    backgroundColor: colors[ 'gray-dark' ],
    justifyContent: 'center',
    alignItems: 'center',
  },
});


export default Bottom;
