import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, Modal, Keyboard, TouchableWithoutFeedback, ScrollView, ActivityIndicator } from 'react-native';
import FontText from '../../components/FontText';
import { hp, isX, wp, normalize, isIOS } from '../../helper/responsiveScreen';
import colors from '../../assets/colors';
import Input from '../../components/Input';
import SmartScrollView from '../../components/SmartScrollView';
import AddModelView from '../../components/AddModelView';
import Loader from '../../components/Loader';
import MediaTabView from '../../components/MessageList/MediaTabView';
import MediaList from '../../components/MediaList';
import DatePicker from 'react-native-datepicker';
import fonts from '../../assets/fonts';
import moment from "moment";
import { getMediaToParams } from '../../helper/toParams';

class Bookmark extends Component {

    constructor(props) {
        super(props)
        this.state = {
            tabTitle: 'Media',
            mediaDate: '',
            filesDate: '',
            audioDate: '',
            linksDate: ''
        }
    }

    onChangeDate = (date) => {
        const { tabTitle } = this.state
        console.log('date......', date)

        if (tabTitle == 'Media') {
            this.setState({ mediaDate: date })
        } else if (tabTitle == 'Files') {
            this.setState({ filesDate: date })
        } else if (tabTitle == 'Audio') {
            this.setState({ audioDate: date })
        } else if (tabTitle == 'Links') {
            this.setState({ linksDate: date })
        }

    }

    onDateClear = (tabTitle) => {

        if (tabTitle == 'Media') {
            this.setState({ mediaDate: '' })
        } else if (tabTitle == 'Files') {
            this.setState({ filesDate: '' })
        } else if (tabTitle == 'Audio') {
            this.setState({ audioDate: '' })
        } else if (tabTitle == 'Links') {
            this.setState({ linksDate: '' })
        }
    }

    render() {
        const { tabTitle, mediaDate, filesDate, audioDate, linksDate } = this.state
        const { isBookMark, onCancelPress, notes, onInputChange, onLabelRemovePress, onAddLabelPress, labelData, isAddLabel, value, onLableCancelPress,
            onAddPress, onSavePress, onEditNotesPress, isEditNotes, processing, getMediaData, getFileData, getAudioData, getLinksData, imagePress,
            onFileDownloadPress, allDownloadFile, onFilePress, isLoading, audioPress, isPlay, audioPausePress, currentTimeString, durationString, playSeconds,
            duration, onSliderEditStart, onSliderEditEnd, onSliderEditing, isPause, audioLoading, loading, address, chatThreadData, id, walletId,
            onSubscribePress, subscribeProcessing } = this.props;

        let mediaData = [], selectDate = ''

        if (tabTitle == 'Media') {
            if (getMediaData && getMediaData.data) {
                if (mediaDate != '') {
                    let exitData = getMediaData.data.filter(x => moment.utc(x.createdAt).local().format('DD/MM/YYYY') === mediaDate)
                    mediaData = exitData
                } else {
                    mediaData = getMediaData.data
                }
            }
            selectDate = mediaDate
        } else if (tabTitle == 'Files') {
            if (getMediaData && getMediaData.data) {
                if (filesDate != '') {
                    let exitData = getFileData.data.filter(x => moment.utc(x.createdAt).local().format('DD/MM/YYYY') === filesDate)
                    mediaData = exitData
                } else {
                    mediaData = getFileData.data
                }
            }
            selectDate = filesDate
        } else if (tabTitle == 'Audio') {
            if (getMediaData && getMediaData.data) {
                if (audioDate != '') {
                    let exitData = getAudioData.data.filter(x => moment.utc(x.createdAt).local().format('DD/MM/YYYY') === audioDate)
                    mediaData = exitData
                } else {
                    mediaData = getAudioData.data
                }
            }
            selectDate = audioDate
        } else if (tabTitle == 'Links') {
            if (getMediaData && getMediaData.data) {
                if (linksDate != '') {
                    let exitData = getLinksData.data.filter(x => moment.utc(x.createdAt).local().format('DD/MM/YYYY') === linksDate)
                    mediaData = exitData
                } else {
                    mediaData = getLinksData.data
                }
            }
            selectDate = linksDate
        }

        let participantsData = chatThreadData.data.find(x => x.threadId == id).participants.find(data => data.address == address)
        let subscribeStatus = participantsData.subscribeDapp?.find(x => x === walletId)

        console.log('subscribeStatus', subscribeStatus)

        // console.log('selectDate', selectDate, mediaData)

        return (
            <Modal
                transparent={true}
                animationType={'none'}
                onRequestClose={() => onCancelPress()}
                visible={isBookMark}>
                <View style={{ ...styles.modelContainer, paddingHorizontal: wp(5) }}>

                    <TouchableWithoutFeedback onPress={() => { onSavePress(), Keyboard.dismiss() }}>
                        <View >
                            <TouchableOpacity onPress={() => onCancelPress()} style={styles.cancelView}>
                                <View style={styles.circleView}>
                                    <Image source={require('../../assets/images/cancel.png')} resizeMode="contain" />
                                </View>
                            </TouchableOpacity>
                        </View>
                    </TouchableWithoutFeedback>

                    <View style={{ ...styles.modelChildContainer, height: hp(80), paddingVertical: hp(2), borderColor: colors[ 'border-light' ] }}>
                        <ScrollView showsVerticalScrollIndicator={false}>
                            <TouchableWithoutFeedback onPress={() => { onSavePress(), Keyboard.dismiss() }}>
                                <View>

                                    {participantsData?.type == 'BUSINESS' &&
                                        <TouchableOpacity
                                            onPress={() => onSubscribePress(participantsData._id, subscribeStatus ? 'UNSUBSCRIBE' : 'SUBSCRIBE')}
                                            style={{
                                                backgroundColor: subscribeStatus ? colors[ 'blue-light' ] : colors[ 'blue-dark' ],
                                                borderWidth: subscribeStatus ? 1 : 0,
                                                ...styles.subscribeView,
                                            }}>
                                            {subscribeProcessing ?
                                                <ActivityIndicator
                                                    animating={subscribeProcessing} size="small" color='#ffffff' />
                                                :
                                                <FontText size={normalize(15)} name={'archivo-regular'} color="white"  >{subscribeStatus ? 'Unsubscribe' : 'Subscribe'}</FontText>
                                            }
                                        </TouchableOpacity>
                                    }

                                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                                        <FontText lines={1} size={normalize(24)} name={'archivo-regular'} color="white" style={{ fontWeight: 'bold', marginRight: wp(2) }} >
                                            {'Notes'}</FontText>
                                        {!isEditNotes ?
                                            <TouchableOpacity style={{ padding: wp(1.5) }}
                                                onPress={() => { onEditNotesPress(), setTimeout(() => this.NotesInput.focus(), 150) }} >
                                                <Image source={require('../../assets/images/edit_icons.png')} resizeMode="contain" style={{ width: wp(5), height: wp(5) }} />
                                            </TouchableOpacity>
                                            :
                                            <View style={{ padding: wp(1.5) }} >
                                                <Image source={require('../../assets/images/save_icon.png')} resizeMode="contain" style={{ width: wp(5), height: wp(5) }} />
                                            </View>
                                        }
                                    </View>
                                    <View style={{
                                        ...styles.inputView, paddingVertical: hp(1.2), borderRadius: isEditNotes ? wp(5) : wp(0),
                                        backgroundColor: isEditNotes ? colors[ 'gray-medium' ] : null,
                                        paddingHorizontal: isEditNotes ? wp(5) : wp(0)
                                    }}>
                                        <Input
                                            ref={input => this.NotesInput = input}
                                            placeholder={'Please Enter Notes'}
                                            defaultValue={notes}
                                            keyboardType={'default'}
                                            // returnKeyType={'done'}
                                            multiline={true}
                                            editable={isEditNotes ? true : false}
                                            // blurOnSubmit={true}
                                            // autoFocus={true}
                                            onChangeText={onInputChange('notes')}
                                            fontSize={normalize(14)}
                                            inputStyle={[
                                                styles.input,
                                            ]}
                                            multilineHeight={hp(12)}
                                            placeholderTextColor="white"
                                            color="white"
                                        // onSubmitEditing={() => { Keyboard.dismiss() }}
                                        />
                                    </View>

                                    {/* <TouchableOpacity onPress={() => onSavePress()} style={{ ...styles.modelChildContainer, alignSelf: 'flex-end', paddingVertical: hp(0.7), borderColor: colors[ 'gray-medium' ] }}>
                                    <FontText size={normalize(14)} name={'archivo-regular'} color="text-light" style={{ fontWeight: '400' }} >{'Save'}</FontText>
                                </TouchableOpacity> */}

                                    <View style={styles.labelHeaderView}>
                                        <FontText lines={1} size={normalize(24)} name={'archivo-regular'} color="white" style={{ fontWeight: 'bold' }} >{'Labels'}</FontText>
                                        {labelData.length != 0 && <FontText size={normalize(12)} name={'archivo-regular'} color="white" style={styles.countText} >{labelData.length}</FontText>}
                                    </View>

                                    {labelData.length != 0 &&
                                        <View style={styles.labelView}>
                                            {labelData.map((item, index) =>
                                                <View key={index} style={styles.labelChildView}>
                                                    <FontText size={normalize(14)} name={'archivo-regular'} color="white" style={styles.labelText} >{item.text}</FontText>
                                                    <TouchableOpacity onPress={() => onLabelRemovePress(index)} style={{ padding: wp(1.5) }}>
                                                        <View style={styles.labelCancelView}>
                                                            <Image source={require('../../assets/images/cancel.png')} resizeMode="contain" />
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                            )}
                                        </View>
                                    }

                                    <TouchableOpacity onPress={() => { onAddLabelPress(), setTimeout(() => this.LabelInput.focus(), 150) }}
                                        style={{ ...styles.modelChildContainer, alignSelf: 'flex-start', paddingVertical: hp(1.2), borderColor: colors[ 'gray-medium' ] }}>
                                        <FontText size={normalize(14)} name={'archivo-regular'} color="text-light" style={{ fontWeight: '400' }} >{'+ Add Label'}</FontText>
                                    </TouchableOpacity>

                                    <View style={styles.mediaTabView}>
                                        <MediaTabView
                                            onTabPress={() => this.setState({ tabTitle: 'Media' })}
                                            title={'Media'}
                                            tabTitle={tabTitle}
                                        />
                                        <MediaTabView
                                            onTabPress={() => this.setState({ tabTitle: 'Files' })}
                                            title={'Files'}
                                            tabTitle={tabTitle}
                                        />
                                        <MediaTabView
                                            onTabPress={() => this.setState({ tabTitle: 'Audio' })}
                                            title={'Audio'}
                                            tabTitle={tabTitle}
                                        />
                                        <MediaTabView
                                            onTabPress={() => this.setState({ tabTitle: 'Links' })}
                                            title={'Links'}
                                            tabTitle={tabTitle}
                                        />
                                    </View>

                                    {/* <TouchableOpacity style={styles.dateContainer}
                                        onPress={() => console.log('press..')}>

                                        <FontText lines={1} size={normalize(14)} name={'archivo-regular'} color="text-light" style={{ fontWeight: '400' }} >
                                            {'Select date'}</FontText>

                                        <Image source={require('../../assets/images/calendar_icon.png')} resizeMode="contain" style={{ width: wp(4), height: wp(4) }} />
                                    </TouchableOpacity> */}

                                    <View style={{ flexDirection: 'row', marginBottom: hp(2), alignItems: 'center', justifyContent: 'space-between' }}>
                                        <DatePicker
                                            style={{ width: selectDate ? wp(67) : wp(79), paddingVertical: hp(0.7), backgroundColor: colors[ 'gray-medium' ], borderRadius: wp(4) }}
                                            date={selectDate}
                                            maxDate={new Date()}
                                            mode="date"
                                            androidMode='spinner'
                                            iconSource={require('../../assets/images/calendar_icon.png')}
                                            placeholder="Select date"
                                            format="DD/MM/YYYY"
                                            confirmBtnText="Ok"
                                            cancelBtnText="Cancel"
                                            customStyles={{
                                                dateIcon: {
                                                    position: 'absolute',
                                                    right: 0,
                                                    height: wp(4),
                                                    width: wp(4),
                                                    marginRight: wp(4),
                                                },
                                                dateInput: {
                                                    paddingHorizontal: wp(4),
                                                    borderWidth: wp(0),
                                                    alignItems: 'flex-start',

                                                },
                                                dateText: {
                                                    fontSize: normalize(16),
                                                    color: colors[ 'text-light' ],
                                                    fontFamily: fonts[ 'archivo-regular' ]
                                                },
                                                datePicker: {
                                                    justifyContent: 'center',
                                                },
                                                placeholderText: {
                                                    fontSize: normalize(16),
                                                    color: colors[ 'text-light' ],
                                                    fontFamily: fonts[ 'archivo-regular' ]
                                                },
                                                btnTextConfirm: {
                                                    color: colors[ 'blue-dark' ]
                                                }
                                            }}
                                            onDateChange={(date) => this.onChangeDate(date)}
                                        />

                                        <TouchableOpacity onPress={() => this.onDateClear(tabTitle)}>
                                            <View style={{ ...styles.cancelView, marginVertical: hp(0.5) }} >
                                                <View style={styles.circleView}>
                                                    <Image source={require('../../assets/images/cancel.png')} resizeMode="contain" />
                                                </View>
                                            </View>
                                        </TouchableOpacity>

                                    </View>

                                    <MediaList
                                        mediaListData={mediaData}
                                        tabTitle={tabTitle}
                                        onItemPress={(item) => this.onItemPress(item)}
                                        imagePress={(item) => imagePress(item)}
                                        onFileDownloadPress={(value) => onFileDownloadPress(value)}
                                        allDownloadFile={allDownloadFile}
                                        onFilePress={(value) => onFilePress(value)}
                                        audioPress={(item) => audioPress(item)}
                                        isPlay={isPlay}
                                        audioPausePress={() => audioPausePress()}
                                        currentTimeString={currentTimeString}
                                        durationString={durationString}
                                        playSeconds={playSeconds}
                                        duration={duration}
                                        onSliderEditStart={() => onSliderEditStart()}
                                        onSliderEditEnd={() => onSliderEditEnd()}
                                        onSliderEditing={(value) => onSliderEditing(value)}
                                        isPause={isPause}
                                        audioLoading={audioLoading} />


                                    <AddModelView

                                        title={'Label'}
                                        placeholder={'Please Enter Label'}
                                        isAddLabel={isAddLabel}
                                        value={value}
                                        onInputChange={(value) => onInputChange(value)}
                                        onLableCancelPress={() => onLableCancelPress()}
                                        onAddPress={() => onAddPress()}
                                        ref={input => this.LabelInput = input}
                                    />

                                    <Loader
                                        loading={processing}
                                    />

                                    <Loader
                                        loading={isLoading}
                                    />
                                    <Loader
                                        loading={loading}
                                    />

                                </View>
                            </TouchableWithoutFeedback>
                        </ScrollView>
                    </View>
                </View>
            </Modal>
        )
    }
}

const styles = StyleSheet.create({
    modelContainer: {
        flex: 1,
        paddingTop: isX ? hp(4) : hp(0),
        flexDirection: 'column',
        backgroundColor: 'rgba(52, 52, 52, 0.5)',
        // paddingBottom: hp(7)
    },
    cancelView: {
        alignSelf: 'flex-end',
        padding: wp(1.5),
        marginVertical: hp(1.5)
    },
    circleView: {
        width: wp(9),
        height: wp(9),
        borderRadius: wp(4.5),
        backgroundColor: colors[ 'gray-light' ],
        justifyContent: 'center',
        alignItems: 'center',
    },
    modelChildContainer: {
        paddingHorizontal: wp(5),
        borderRadius: wp(5),
        borderWidth: 1,
        backgroundColor: colors.black
    },
    inputView: {
        marginBottom: hp(2.5),
        marginTop: hp(1),
    },
    input: {
        fontWeight: '400',
        backgroundColor: colors.transparent,
        paddingLeft: 0,
        paddingTop: 0
    },
    labelHeaderView: {
        flexDirection: 'row',
        marginBottom: hp(2),
    },
    countText: {
        fontWeight: '700',
        marginLeft: wp(2),
        alignSelf: 'flex-end',
        marginBottom: hp(0.2),
        opacity: 0.3
    },
    labelView: {
        flexWrap: 'wrap',
        flexDirection: 'row',
        marginBottom: hp(1)
    },
    labelChildView: {
        flexDirection: 'row',
        borderRadius: wp(5),
        backgroundColor: colors[ 'gray-medium' ],
        marginRight: wp(3),
        marginBottom: hp(1),
        alignItems: 'center',
        alignSelf: 'flex-start'
    },
    labelCancelView: {
        width: wp(6),
        height: wp(6),
        borderRadius: wp(3),
        backgroundColor: colors.black,
        justifyContent: 'center',
        alignItems: 'center',
    },
    labelText: {
        fontWeight: '400',
        marginLeft: wp(3),
        marginRight: wp(1)
    },
    mediaTabView: {
        flexDirection: 'row',
        marginVertical: hp(1.5),
    },
    dateContainer: {
        marginBottom: hp(3),
        backgroundColor: colors[ 'gray-medium' ],
        borderRadius: wp(4),
        paddingVertical: hp(2),
        paddingHorizontal: wp(5),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    subscribeView: {
        borderColor: colors[ 'blue-dark' ],
        width: wp(29),
        paddingVertical: wp(2.2),
        height: wp(10),
        borderRadius: wp(5),
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'flex-end',
        marginBottom: hp(1)
    }
});


export default Bookmark;
