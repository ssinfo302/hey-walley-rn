import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import FontText from '../../components/FontText';
import { hp, isX, wp, normalize } from '../../helper/responsiveScreen';
import colors from '../../assets/colors';

class Header extends Component {

    render() {
        const { image, title, onBackPress, onBookmarkPress, lastSeen } = this.props;
        let time

        if (lastSeen == 'in 1 minutes') {
            time = 'online'
        } else if (lastSeen == 'in a few second') {
            time = 'Last seen online a few second ago'
        } else {
            time = `Last seen online ${lastSeen}`
        }

        return (
            <View style={styles.container}>
                <View style={styles.childContainer}>
                    <TouchableOpacity onPress={() => onBackPress()} style={{ padding: wp(1.5), marginRight: wp(2) }}>
                        <View style={styles.circleView}>
                            <Image source={require('../../assets/images/left_arrow.png')} resizeMode="contain" />
                        </View>
                    </TouchableOpacity>
                    <Image source={image} style={{ width: wp(10), height: wp(10) }} resizeMode="contain" />
                    <View style={styles.headerTitleView}>
                        <FontText lines={1} size={normalize(18)} name={'archivo-regular'} color="white" style={{ fontWeight: '700' }} >{title}</FontText>
                        <FontText lines={1} size={normalize(10)} name={'archivo-regular'} color="text-gray" style={{ fontWeight: '400' }} >{time}</FontText>
                    </View>
                </View>

                <TouchableOpacity onPress={() => onBookmarkPress()} style={{ padding: wp(1.5) }}>
                    <View style={styles.circleView}>
                        <Image source={require('../../assets/images/bookmark.png')} resizeMode="contain" />
                    </View>
                </TouchableOpacity>
            </View>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingBottom: hp(1.5),
        backgroundColor: colors.black,
        paddingTop: isX ? hp(5.5) : hp(1.5),
        // paddingHorizontal: wp(5),
    },
    childContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    circleView: {
        width: wp(9),
        height: wp(9),
        borderRadius: wp(4.5),
        backgroundColor: colors[ 'gray-light' ],
        justifyContent: 'center',
        alignItems: 'center',
    },
    headerTitleView: {
        marginLeft: wp(3),
        width: wp(50),
    }
});


export default Header;
