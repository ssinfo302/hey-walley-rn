import React, { Component } from 'react';
import { StyleSheet, Image, View, Dimensions, TouchableOpacity, ToastAndroid, Alert } from 'react-native';
import FontText from '../../components/FontText';
import colors from '../../assets/colors';
import { isX, normalize, hp, wp, isIOS } from '../../helper/responsiveScreen';
import Constant from '../../helper/appConstant';
import BlockChainList from '../../components/BlockChainList';
import SmartScrollView from '../../components/SmartScrollView';
import Input from '../../components/Input';
import Button from '../../components/Button';
import Clipboard from '@react-native-community/clipboard';
import { routeName as tabNavigationRouteName } from '../../navigation/TabNavigation'
import { resetNavigateTo } from '../../helper/navigationHelper';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { addWalletToParams, checkWalletToParams } from '../../helper/toParams';
import { walletActions } from '../../actions/actions'
import Loader from '../../components/Loader';
import AsyncStorage from '@react-native-community/async-storage';
import { storeCheckWalletData, storeAddWalletData } from '../../util/Store';


export const routeName = 'welcome';

class Welcome extends Component {

  constructor(props) {
    super(props)
    this.state = {
      blockChainData: Constant.blockChainData,
      walletAddress: '',
      isAccountAddress: false,
      blockChain: '',
      placeholder: 'Select blockchain first',
      uid: '',
      receivingAddress: '',
      amount: ''
    }
  }


  async componentDidMount() {
    let walletData = await AsyncStorage.getItem('check wallet data');
    console.log('walletData', walletData.length, JSON.parse(walletData), JSON.stringify(walletData), walletData)

    if (walletData != null && JSON.stringify(walletData) != '"[]"') {

      let address = JSON.parse(await AsyncStorage.getItem('address'))
      console.log('address!!', address)
      let parsed = JSON.parse(walletData).find(x => x.address === address);
      console.log('walletData...', parsed)
      if (parsed) {
        if (!parsed.token) {
          let uid = JSON.parse(await AsyncStorage.getItem('uid'))
          let addWalletData = JSON.parse(await AsyncStorage.getItem('add wallet data')).find(x => x.uid === uid)
          console.log('addWalletData...', addWalletData)
          this.setState({
            isAccountAddress: true, uid: addWalletData.uid, receivingAddress: addWalletData.receivingAddress, amount: addWalletData.amount,
            blockChain: addWalletData.chain, walletAddress: parsed.address
          })
        }
      } else {
        let address = JSON.parse(await AsyncStorage.getItem('address'))
        let uid = JSON.parse(await AsyncStorage.getItem('uid'))
        let addWalletData = JSON.parse(await AsyncStorage.getItem('add wallet data')).find(x => x.uid === uid)
        console.log('addWalletData', addWalletData)
        this.setState({
          isAccountAddress: true, uid: addWalletData.uid, receivingAddress: addWalletData.receivingAddress, amount: addWalletData.amount,
          blockChain: addWalletData.chain, walletAddress: address
        })
      }
    }

  }

  componentDidUpdate(prevProps) {
    const { processing, error, success, data, wallet, checkWalletSuccess, navigation } = this.props;
    console.log('prevProps', JSON.stringify(prevProps))
    console.log('success', success, data, wallet)

    if (prevProps.wallet !== wallet) {
      if (wallet.success) {
        console.log('token', wallet.wallet.token)
        // AsyncStorage.setItem('wallet Data', JSON.stringify(wallet.wallet));
        storeCheckWalletData(wallet.wallet)
        if (wallet.wallet.token) {
          setTimeout(() => {
            resetNavigateTo(navigation, tabNavigationRouteName)
          }, 500);
        } else {
          alert('Please wait for this address to claim wallet')
        }
      } else {
        alert(wallet.message)
      }
    } else if (prevProps.data !== data) {
      if (data.success) {
        // AsyncStorage.setItem('add wallet data', JSON.stringify(data.wallet));
        storeAddWalletData(data.wallet)
        this.setState({ isAccountAddress: true, uid: data.wallet.uid, receivingAddress: data.wallet.receivingAddress, amount: data.wallet.amount, blockChain: data.wallet.chain })
      } else {
        alert(data.message)
      }
    } else if (prevProps.error !== error) {
      alert(error.message)
    }
  }

  onItemPress = (item) => {
    console.log('item', item)
    this.setState({ blockChain: item.title, placeholder: `Please enter ${item.title} address` })
  }

  onConfirmPressHandler = () => {
    const { blockChain, walletAddress, isAccountAddress, placeholder, receivingAddress } = this.state
    const { navigation, addWallet, checkWallet } = this.props
    if (blockChain == '') {
      alert('Please select blockchain')
    } else if (walletAddress == '' && receivingAddress == '') {
      alert(placeholder)
    } else {
      if (isAccountAddress) {
        // resetNavigateTo(navigation, tabNavigationRouteName)
        checkWallet(checkWalletToParams(this.props, this.state))
      } else {
        addWallet(addWalletToParams(this.props, this.state))
        // this.setState({ isAccountAddress: true })
      }
    }
  }

  onInputChange = name => value => {
    this.setState({ [ name ]: value });
  }

  onCopyPressHandler = (value) => {
    Clipboard.setString(value)
    if (isIOS) {
      alert('Successfully copy address');
    } else {
      ToastAndroid.show('Successfully copy address', ToastAndroid.SHORT);
    }
  }

  render() {
    const { blockChainData, walletAddress, isAccountAddress, placeholder, receivingAddress, amount, blockChain } = this.state
    const { navigation, processing, data } = this.props

    return (
      <SmartScrollView
        showsVerticalScrollIndicator={false}
        style={styles.container}
        applyKeyboardCheck={isIOS ? true : false}
        disabled={false}
        alwaysBounceVertical={false} >
        <Image source={require('../../assets/images/logo.png')} resizeMode="contain" style={styles.logoImage} />
        <View style={{ ...styles.headerContainer, alignItems: 'center', paddingVertical: hp(3) }}>
          <FontText size={normalize(24)} name={'archivo-regular'} color="gray" >{`Hey Wallet`}</FontText>
          <FontText size={normalize(18)} textAlign={'center'} name={'archivo-regular'} color="gray" style={styles.subTitle} >{`First wallet focused messaging app.`}</FontText>
          <Image source={require('../../assets/images/chat.png')} resizeMode="contain" style={styles.image} />
        </View>

        <FontText size={normalize(24)} name={'archivo-regular'} color="white" style={{ opacity: 0.7 }} >{isAccountAddress ? `Pay ${amount} ${blockChain} to this address to claim wallet` : `Select blockchain type of your choice and claim a wallet`}</FontText>

        {!isAccountAddress ?
          <View>
            <BlockChainList blockChainData={blockChainData} onItemPress={(item) => this.onItemPress(item)} navigation={navigation} />
            <View style={{ ...styles.headerContainer, paddingVertical: hp(1.5), paddingHorizontal: wp(5) }}>
              <FontText size={normalize(14)} name={'archivo-regular'} color="white" >{`Your wallet address`}</FontText>
              <Input
                placeholder={placeholder}
                defaultValue={walletAddress}
                keyboardType={'default'}
                returnKeyType={'done'}
                blurOnSubmit={true}
                onChangeText={this.onInputChange('walletAddress')}
                fontSize={normalize(18)}
                inputStyle={[
                  styles.input,
                ]}
                placeholderTextColor="white"
                color="white"
              />
            </View>
          </View>
          :
          <View style={{ marginVertical: hp(6) }}>
            <FontText size={normalize(14)} name={'archivo-regular'} color="white" style={{ fontWeight: '400' }} >{`Generated Account Address`}</FontText>
            <View style={styles.walletAddressView}>
              <FontText numberOfLines={2} size={normalize(18)} name={'archivo-regular'} color="white" style={{ fontWeight: '700', maxWidth: wp(75), }} >{receivingAddress}</FontText>
              <TouchableOpacity onPress={() => this.onCopyPressHandler(receivingAddress)} style={{ padding: wp(2), marginLeft: wp(2) }} >
                <Image source={require('../../assets/images/paste.png')} resizeMode="contain" />
              </TouchableOpacity>
            </View>
          </View>
        }

        <Button
          height={isX ? hp(6) : hp(8)}
          borderRadius={wp(10)}
          bgColor={"blue"}
          style={styles.btnConfirm}
          onPress={this.onConfirmPressHandler}
        >
          <FontText color={"white"} name={'archivo-regular'} size={normalize(15)}>{`Confirm`}</FontText>
        </Button>

        <Loader
          loading={processing}
        />
      </SmartScrollView>
    );
  }
}

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.black,
    paddingHorizontal: wp(5),
    paddingTop: isX ? hp(4) : hp(0),
  },
  logoImage: {
    marginVertical: hp(2.5),
    width: wp(10),
    height: wp(10)
  },
  headerContainer: {
    marginBottom: hp(5),
    borderRadius: wp(5),
    backgroundColor: colors[ 'gray-dark' ],
  },
  subTitle: {
    marginHorizontal: wp(20),
    marginBottom: hp(3),
  },
  image: {
    width: '100%',
    height: hp(25)
  },
  input: {
    fontWeight: '700',
    backgroundColor: colors.transparent,
    paddingLeft: 0,
    paddingTop: 0
  },
  btnConfirm: {
    marginBottom: isX ? hp(6) : hp(4),
  },
  walletAddressView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: hp(1)
  }
});

const mapStateToProps = state => {
  const {
    addWallet: {
      address,
      chain,
      processing,
      error,
      success,
      data,
      wallet,
      checkWalletSuccess
    },
  } = state;
  return {
    address,
    chain,
    processing,
    error,
    success,
    data,
    wallet,
    checkWalletSuccess
  };
}

const mapDispatchToProps = dispatch => bindActionCreators({

  addWallet: walletActions.addWallet,
  checkWallet: walletActions.checkWallet

}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Welcome);