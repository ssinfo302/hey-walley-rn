import React, { Component } from 'react';
import { StyleSheet, ImageBackground, Image, View, } from 'react-native';
import FontText from '../../components/FontText';
import colors from '../../assets/colors';
import { isX, normalize, hp, wp } from '../../helper/responsiveScreen';
import Constant from '../../helper/appConstant';
import WalletList from '../../components/WalletList';
import AsyncStorage from '@react-native-community/async-storage';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { walletActions } from '../../actions/actions'
import Loader from '../../components/Loader';
export const routeName = 'wallet';
import { storeAddWalletData } from '../../util/Store';
import { routeName as welcomeRouteName } from '../Welcome';
import { resetNavigateTo } from '../../helper/navigationHelper';
import { CommonActions } from '@react-navigation/native'

class Wallet extends Component {

  constructor(props) {
    super(props)
    this.state = {
      walletListData: Constant.blockChainData,
      addressData: [],
      currentAddress: ''
    }
  }

  componentDidMount() {
    this.focusListener = this.props.navigation.addListener("focus", async () => {
      let walletData = JSON.parse(await AsyncStorage.getItem('check wallet data'));
      let address = JSON.parse(await AsyncStorage.getItem('address'))
      this.setState({ addressData: walletData, currentAddress: address })
    })
  }

  componentWillUnmount() {
    this.focusListener();
  }

  async componentDidUpdate(prevProps) {
    const { processing, error, success, data, navigation, removeData } = this.props;

    if (prevProps.data !== data) {
      if (data.success) {
        console.log('add Wallet...............', data)
        storeAddWalletData(data.wallet)
        setTimeout(() => {
          resetNavigateTo(navigation, welcomeRouteName)
        }, 300);
      } else {
        alert(data.message)
        AsyncStorage.setItem('address', JSON.stringify(this.state.currentAddress));
      }
    } else if (prevProps.removeData !== removeData) {
      if (removeData.success) {
        console.log('remove Wallet...............', removeData)

        let walletData = JSON.parse(await AsyncStorage.getItem('check wallet data'));

        console.log('walletData!!!!!!', walletData, walletData.length)
        if (walletData.length != 0) {
          let address = JSON.parse(await AsyncStorage.getItem('address'))
          let existData = walletData.find(x => x.address === address)
          if (!existData) {
            AsyncStorage.setItem('address', JSON.stringify(walletData[ 0 ].address));
          }
          setTimeout(() => {
            this.setState({ addressData: walletData })
          }, 300);
        } else {
          setTimeout(() => {
            this.setState({ addressData: walletData })
            resetNavigateTo(navigation, welcomeRouteName)
          }, 300);
        }

      } else {
        alert(removeData.message)
      }
    }
    else if (prevProps.error !== error) {
      alert(error.message)
    }
  }

  render() {
    const { walletListData, addressData } = this.state
    const { navigation, addWallet, processing, removeWallet } = this.props
    return (
      <View style={styles.container}>
        <FontText size={normalize(24)} name={'archivo-regular'} color="white" style={{ marginTop: hp(2) }} >{`Wallets`}</FontText>
        <WalletList walletListData={walletListData} addressData={addressData} navigation={navigation} addWallet={addWallet} removeWallet={removeWallet} />
        <Loader
          loading={processing}
        />
      </View>
    );
  }
}

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: isX ? hp(4) : hp(0),
    paddingHorizontal: wp(5),
    backgroundColor: colors.black
  },
});

const mapStateToProps = state => {
  const {
    addWallet: {
      address,
      chain,
      processing,
      error,
      success,
      data,
      removeData
    },
  } = state;
  return {
    address,
    chain,
    processing,
    error,
    success,
    data,
    removeData
  };
}

const mapDispatchToProps = dispatch => bindActionCreators({

  addWallet: walletActions.addWallet,
  removeWallet: walletActions.removeWallet,

}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Wallet);