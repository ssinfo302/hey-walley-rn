import React, { Component } from 'react';
import { StyleSheet, Image, View, BackHandler, Alert, TouchableOpacity, Keyboard } from 'react-native';
import FontText from '../../components/FontText';
import colors from '../../assets/colors';
import { isX, normalize, hp, wp } from '../../helper/responsiveScreen';
import Constant from '../../helper/appConstant';
import ChatList from '../../components/ChatList';
import { routeName as chatDetailRouteName } from '../ChatDetail';
import AsyncStorage from '@react-native-community/async-storage';
import { removeChatToParams } from '../../helper/toParams';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { chatActions, walletActions } from '../../actions/actions'
import Loader from '../../components/Loader';
import io from 'socket.io-client';
import { resetNavigateTo } from '../../helper/navigationHelper';
import { routeName as welcomeRouteName } from '../Welcome';
import Input from '../../components/Input';
import DappList from '../../components/DappList';

const socket = io('https://heywallet.axislabs.company');
export const routeName = 'home';

class Home extends Component {

  constructor(props) {
    super(props)
    this.state = {
      chatListData: [],
      address: '',
      isRefresh: false,
      token: '',
      timerId: '',
      search: '',
      dappListData: null,
      walletId: '',
      isFocus: false,
      tabTitle: this.props.route.params && this.props.route.params.tabTitle ? this.props.route.params.tabTitle : 'All Chats',
    }
  }

  async componentDidMount() {

    this.focusListener = this.props.navigation.addListener("focus", async () => {
      let address = JSON.parse(await AsyncStorage.getItem('address'))
      let walletData = JSON.parse(await AsyncStorage.getItem('check wallet data')).find(x => x.address === address);
      console.log('walletData', JSON.parse(await AsyncStorage.getItem('check wallet data')))
      this.setState({ chatListData: [], walletId: walletData.id, address: walletData.address, token: walletData.token, isRefresh: false }, () => this.getThreadCall())

      socket.emit("joinRoom", walletData.token, user => {
        console.log('user', user);
      });
      socket.on("new_thread", this.onSocketNewThreadHandler)
      socket.on('chat_receive', this.onSocketMessageHandler);

      socket.emit("active_wallet", address, chat => {
        console.log('active_wallet...', chat);
      });

      let timerId = setInterval(() => {
        socket.emit("active_wallet", address, chat => {
          console.log('active_wallet...', chat);
        });
      }, 15000);
      this.setState({ timerId: timerId })
    })
  }

  onSocketNewThreadHandler = (data) => {
    console.log('onSocketNewThreadHandler', data)
    this.getThreadCall()
    this.setState({ isRefresh: true })
  }

  onSocketMessageHandler = async (data) => {
    console.log('onSocketMessageHandler!!!!!', data)
    this.getThreadCall()
    this.setState({ isRefresh: true })
  }

  getThreadCall = async () => {
    const { chatThread } = this.props
    chatThread(this.state.token)
  }

  componentDidUpdate(prevProps) {
    const { processing, error, success, chatThreadData, navigation, searchDappData, subscribeData } = this.props;

    if (prevProps.chatThreadData !== chatThreadData) {
      if (chatThreadData.success) {
        this.setState({ chatListData: chatThreadData })
      } else {
        if (chatThreadData.message.toLowerCase().includes("invalid token")) {
          AsyncStorage.removeItem('check wallet data');
          AsyncStorage.removeItem('address');
          AsyncStorage.removeItem('add wallet data');
          AsyncStorage.removeItem('uid');
          resetNavigateTo(navigation, welcomeRouteName)
        } else {
          alert(chatThreadData.message)
        }
      }
    } else if (prevProps.searchDappData !== searchDappData) {
      if (searchDappData.success) {
        console.log('serchdata', searchDappData)
        this.setState({ dappListData: searchDappData.data })
      } else {
        this.setState({ dappListData: null })
        alert(searchDappData.message)
      }
    } else if (prevProps.subscribeData !== subscribeData) {
      if (subscribeData.success) {
        console.log('subscribeData', subscribeData)
      } else {
        alert(subscribeData.message)
      }
    } else if (prevProps.error !== error) {
      if (error.message.toLowerCase().includes("invalid token")) {
        AsyncStorage.removeItem('check wallet data');
        AsyncStorage.removeItem('address');
        AsyncStorage.removeItem('add wallet data');
        AsyncStorage.removeItem('uid');
        resetNavigateTo(navigation, welcomeRouteName)
      } else {
        alert(error.message)
      }
      this.setState({ dappListData: null })
    }
  }

  componentWillUnmount() {
    this.focusListener();
    clearInterval(this.state.timerId)
    socket.off("new_thread", this.onSocketNewThreadHandler)
    socket.off('chat_receive', this.onSocketMessageHandler);
  }

  onItemPress = (item) => {
    const { navigation } = this.props
    const { address, tabTitle } = this.state

    let existData = item.participants.find(x => x.address != address);
    socket.off("new_thread", this.onSocketNewThreadHandler)
    socket.off('chat_receive', this.onSocketMessageHandler);
    navigation.push(chatDetailRouteName, {
      address: existData.address,
      chain: existData.chain,
      id: item.threadId,
      projectName: existData?.projectName,
      projectLogo: existData?.projectLogo,
      tabTitle: tabTitle
    })
    // resetNavigateTo(navigation, chatDetailRouteName)

  }

  onDappsPress = (item) => {
    const { navigation } = this.props
    const { tabTitle } = this.state

    navigation.push(chatDetailRouteName, {
      address: item.address,
      chain: item.chain,
      id: item._id,
      projectName: item?.projectName,
      projectLogo: item?.projectLogo,
      tabTitle: tabTitle
    })
  }

  removePress = (item) => {
    const { removeChat } = this.props;
    const { token } = this.state;

    Alert.alert(
      "",
      "Are you sure to delete Chat?",
      [
        {
          text: "Yes", onPress: async () => {
            console.log("yes Pressed", item)
            let id = item.threadId

            removeChat(removeChatToParams(this.props, { id, token }))
          }
        },
        {
          text: "No",
          onPress: () => console.log("Cancel Pressed"),
        }
      ],
      { cancelable: false }
    );
  }

  onInputChange = name => value => {
    const { dappListData } = this.state
    const { searchDappData } = this.props
    this.setState({ [ name ]: value });

    if (value.length != 0) {
      let exitData = searchDappData?.data.filter(x => x?.projectName && x?.projectName.toLowerCase().includes(value.toLowerCase()))
      this.setState({ dappListData: exitData })
    } else {
      this.setState({ dappListData: searchDappData.data })
    }
  }

  onClearSearchbar = () => {
    this.setState({ search: '', dappListData: null, isFocus: false })
    Keyboard.dismiss()
  }

  onSerchPress = () => {
    const { search, isFocus } = this.state
    if (search == '') {
      this.setState({ isFocus: false, dappListData: null })
    }
  }

  onFocusPress = () => {
    const { dappList } = this.props
    const { token, dappListData } = this.state
    this.setState({ isFocus: true })
    console.log('dappListData', dappListData)
    if (dappListData == null) {
      console.log('dappListData', dappListData)
      dappList(token)
    }
  }

  onSubScribePress = (item, status) => {
    console.log('status', status)
    if (status) {
      this.subscribeCall('UNSUBSCRIBE', item._id)
    } else {
      this.subscribeCall('SUBSCRIBE', item._id)
    }
  }

  subscribeCall = (type, dappId) => {
    const { subscribeDapp } = this.props
    const { token, walletId } = this.state

    const data = {
      token: token,
      dappId: dappId,
      type: type,
      walletId: walletId
    }
    subscribeDapp(data)
  }

  render() {
    const { chatListData, address, isRefresh, tabTitle, search, dappListData, walletId, isFocus } = this.state
    const { navigation, chatThreadProcessing, searchDappLoading, processing } = this.props

    let data = []
    if (chatListData?.data) {
      if (tabTitle == 'Personal') {
        data = chatListData.data.filter(x => x.participants.find(data => data.address != address && data.type == 'PERSONAL'))
      } else if (tabTitle == 'DApps') {
        data = chatListData.data.filter(x => x.participants.find(data => data.address != address && data.type == 'BUSINESS'))
      } else {
        data = chatListData.data
      }
    }

    return (
      <View style={styles.container}>
        <View style={{ flexDirection: 'row', marginVertical: hp(1) }}>
          <TouchableOpacity style={{ paddingVertical: hp(2) }} onPress={() => this.setState({ tabTitle: 'All Chats', dappListData: null })}>
            <FontText size={normalize(20)} name={'archivo-regular'} color={tabTitle == 'All Chats' ? "white" : "text-light"} >{`All Chats`}</FontText>
          </TouchableOpacity>

          <TouchableOpacity style={{ paddingVertical: hp(2) }} onPress={() => this.setState({ tabTitle: 'Personal', dappListData: null })}>
            <FontText size={normalize(20)} name={'archivo-regular'} color={tabTitle == 'Personal' ? "white" : "text-light"} style={{ marginLeft: wp(5) }} >{`Personal`}</FontText>
          </TouchableOpacity>

          <TouchableOpacity style={{ paddingVertical: hp(2) }} onPress={() => this.setState({ tabTitle: 'DApps', search: '', dappListData: null })}>
            <FontText size={normalize(20)} name={'archivo-regular'} color={tabTitle == 'DApps' ? "white" : "text-light"} style={{ marginLeft: wp(5) }}>{`DApps`}</FontText>
          </TouchableOpacity>
          {/* {chatListData.data && chatListData.data.length != 0 &&
            <FontText size={normalize(12)} name={'archivo-regular'} color="white" style={styles.countText} >{chatListData.data.length}</FontText>
          } */}
        </View>

        {tabTitle == 'DApps' &&
          <View style={styles.searchView}>
            <Image source={require('../../assets/images/search_gray_image.png')} resizeMode="contain" />
            <Input
              placeholder={'Search all dApps'}
              defaultValue={search}
              keyboardType={'default'}
              returnKeyType={'done'}
              blurOnSubmit={true}
              onChangeText={this.onInputChange('search')}
              fontSize={normalize(16)}
              inputStyle={[
                styles.input,
              ]}
              // onEndEditing={() => this.onSerchPress()}
              onFocus={() => this.onFocusPress()}
              onSubmitEditing={() => this.onSerchPress()}
              placeholderTextColor="white"
              color="white"
            />

            {(search.length != 0 || isFocus) &&
              <TouchableOpacity onPress={() => this.onClearSearchbar()} style={{ padding: wp(2), marginLeft: wp(1), }}>
                <Image source={require('../../assets/images/cancel.png')} resizeMode="contain" style={{ tintColor: colors[ 'text-light' ] }} />
              </TouchableOpacity>
            }
          </View>
        }

        {dappListData ?
          <DappList dappListData={dappListData} processing={processing} walletId={walletId} onDappsPress={(item) => this.onDappsPress(item)}
            onItemPress={(item, status) => this.onSubScribePress(item, status)} />
          :
          <ChatList extraData={this.state} data={data} chatListData={chatListData} address={address} onItemPress={(item) => this.onItemPress(item)} navigation={navigation} socket={socket} removePress={(item) => this.removePress(item)} />
        }

        {
          !isRefresh &&
          <Loader
            loading={chatThreadProcessing}
          />
        }
        <Loader
          loading={searchDappLoading}
        />
      </View >
    );
  }
}

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: isX ? hp(4) : hp(0),
    paddingHorizontal: wp(5),
    backgroundColor: colors.black
  },
  countText: {
    fontWeight: '700',
    marginLeft: wp(1),
    alignSelf: 'flex-end',
    opacity: 0.3
  },
  searchView: {
    flexDirection: 'row',
    marginBottom: hp(2),
    alignItems: 'center',
    // justifyContent: 'space-between',
    borderRadius: wp(4),
    paddingLeft: wp(4),
    paddingRight: wp(3),
    paddingVertical: hp(1),
    backgroundColor: colors[ 'gray-light' ]
  },
  input: {
    fontWeight: '400',
    width: wp(68),
    backgroundColor: colors.transparent,
    paddingLeft: 0,
    paddingTop: 0,
    marginLeft: wp(3)
  },
});

const mapStateToProps = state => {
  const {
    chat: {
      chatThreadProcessing,
      error,
      success,
      chatThreadData,
      removeChatData
    },
    addWallet: {
      searchDappLoading,
      searchDappData,
      subscribeData,
      processing
    },
  } = state;
  return {
    chatThreadProcessing,
    error,
    success,
    chatThreadData,
    removeChatData,
    searchDappData,
    searchDappLoading,
    subscribeData,
    processing
  };
}

const mapDispatchToProps = dispatch => bindActionCreators({

  chatThread: chatActions.chatThread,
  removeChat: chatActions.removeChat,
  searchDapp: walletActions.searchDapp,
  subscribeDapp: walletActions.subscribeDapp,
  dappList: walletActions.dappList
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Home);