import React, { Component } from 'react';
import { StyleSheet, ImageBackground, Image, View, Animated } from 'react-native';
import { resetNavigateTo } from '../../helper/navigationHelper';
import FontText from '../../components/FontText';
import colors from '../../assets/colors';
import { routeName as welcomeRouteName } from '../Welcome';
import { routeName as tabNavigationRouteName } from '../../navigation/TabNavigation'
import { isiPAD, normalize, hp, wp } from '../../helper/responsiveScreen';
import AsyncStorage from '@react-native-community/async-storage';

export const routeName = 'splash';

class Splash extends Component {

  constructor(props) {
    super(props)
    this.state = {
      progressStatus: 0,
    }
  }

  anim = new Animated.Value(0);

  componentDidMount() {
    this.onAnimate();
    setTimeout(() => {
      this.onLoadingDone()
    }, 5000);
  }

  onAnimate = () => {
    this.anim.addListener(({ value }) => {
      this.setState({ progressStatus: parseInt(value, 10) });
    });
    Animated.timing(this.anim, {
      toValue: 100,
      duration: 5000,
    }).start();
  }

  onLoadingDone = async () => {
    const { navigation } = this.props;
    let walletData = await AsyncStorage.getItem('check wallet data');
    console.log('walletData', JSON.stringify(walletData))

    if (walletData != null) {
      let address = JSON.parse(await AsyncStorage.getItem('address'))
      let parsed = JSON.parse(walletData).find(x => x.address === address);

      console.log('parsed...', parsed)
      if (parsed) {
        if (parsed.token) {
          resetNavigateTo(navigation, tabNavigationRouteName)
        } else {
          resetNavigateTo(navigation, welcomeRouteName)
        }
      } else {
        resetNavigateTo(navigation, welcomeRouteName)
      }
    } else {
      resetNavigateTo(navigation, welcomeRouteName)
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.childContainer}>
          <FontText size={normalize(28)} name={'archivo-regular'} color="gray" >{`Hey Wallet`}</FontText>
          <FontText size={normalize(22)} textAlign={'center'} name={'archivo-regular'} color="gray" style={styles.subTitle} >{`First wallet focused messaging app.`}</FontText>
          <Image source={require('../../assets/images/chat.png')} resizeMode="contain" />
        </View>
        <View style={styles.bottomContainer}>
          <Animated.View
            style={[
              styles.inner, { width: this.state.progressStatus + "%" },
            ]}
          />
        </View>
      </View>
    );
  }
}

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: null,
    width: null,
    justifyContent: 'space-between',
    backgroundColor: colors.black
  },
  childContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  subTitle: {
    marginHorizontal: wp(20),
    marginTop: hp(2),
    marginBottom: hp(5)
  },
  bottomContainer: {
    marginVertical: hp(5),
    marginHorizontal: wp(10)
  },
  inner: {
    width: "100%",
    height: hp(0.6),
    borderRadius: 15,
    backgroundColor: colors.blue,
  }
});

export default Splash;