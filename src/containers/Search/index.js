import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, Image, View, BackHandler } from 'react-native';
import FontText from '../../components/FontText';
import colors from '../../assets/colors';
import { isX, normalize, hp, wp, isIOS } from '../../helper/responsiveScreen';
import Input from '../../components/Input';
import SmartScrollView from '../../components/SmartScrollView';
import Constant from '../../helper/appConstant';
import SearchList from '../../components/SearchList';
import TabView from '../../components/SearchList/tabView';
import { routeName as chatDetailRouteName } from '../ChatDetail';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { walletActions } from '../../actions/actions'
import Loader from '../../components/Loader';
import AsyncStorage from '@react-native-community/async-storage';

export const routeName = 'search';

class Search extends Component {

  constructor(props) {
    super(props)
    this.state = {
      search: '',
      searchListData: {},
      tabTitle: 'Users',
      address: ''
    }
  }

  async componentDidMount() {

    this.focusListener = this.props.navigation.addListener("focus", async () => {
      let address = JSON.parse(await AsyncStorage.getItem('address'))

      this.setState({
        search: '',
        searchListData: {},
        address: address
      })
    })

  }

  componentWillUnmount() {
    this.focusListener();
  }

  componentDidUpdate(prevProps) {
    const { processing, success, navigation, searchListData, error } = this.props;

    if (prevProps.searchListData !== searchListData) {
      if (searchListData.success) {
        console.log('searchListData.......', searchListData)
        this.setState({ searchListData: searchListData, search: '' })
        if (searchListData.users.length != 0) {
          this.setState({ tabTitle: 'Users' })
        } else if (searchListData.labels.length != 0) {
          this.setState({ tabTitle: 'Labels' })
        } else {
          this.setState({ tabTitle: 'Messages' })
        }
      } else {
        alert(searchListData.message)
      }
    } else if (prevProps.error !== error) {
      alert(error.message)
    }
  }

  onInputChange = name => value => {
    this.setState({ [ name ]: value });
  }

  onItemPress = (item) => {
    const { navigation, chatThreadData } = this.props
    const { tabTitle } = this.state
    console.log('item', item, chatThreadData)

    if (tabTitle != 'Users') {
      let exitData = chatThreadData.data.find(x => x.threadId == item.threadId);
      if (tabTitle == 'Labels') {
        var walletAddress = exitData.participants.find(x => x.address != item.wallet.address);
      } else {
        var walletAddress = exitData.participants.find(x => x.address != item.senderAddress);
      }
    }

    navigation.navigate(chatDetailRouteName, {
      address: tabTitle == 'Users' ? item.address : walletAddress.address,
      chain: tabTitle == 'Users' ? item.chain : tabTitle == 'Labels' ? item.wallet.chain : item.senderWallet.chain,
      id: tabTitle == 'Users' ? item._id : item.threadId
    })
  }

  onSerchPress = async () => {
    const { searchWallet } = this.props
    let selectAddress = JSON.parse(await AsyncStorage.getItem('address'))
    let walletData = JSON.parse(await AsyncStorage.getItem('check wallet data')).find(x => x.address === selectAddress);

    if (this.state.search != '') {
      const data = {
        token: walletData.token,
        search: this.state.search
      }
      searchWallet(data)
    }
  }

  render() {
    const { search, searchListData, tabTitle, address } = this.state
    const { navigation, processing } = this.props

    let searchData

    if (tabTitle == 'Users') {
      searchData = searchListData.users
    } else if (tabTitle == 'Labels') {
      searchData = searchListData.labels
    } else if (tabTitle == 'Messages') {
      searchData = searchListData.messages
    }

    return (
      // <SmartScrollView
      //   showsVerticalScrollIndicator={false}
      //   style={styles.container}
      //   applyKeyboardCheck={isIOS ? true : false}
      //   disabled={false}
      //   alwaysBounceVertical={false} >
      <View style={styles.container}>

        <FontText size={normalize(24)} name={'archivo-regular'} color="white" style={{ marginVertical: hp(2) }} >{`Search`}</FontText>

        <View style={styles.searchView}>
          <Input
            placeholder={'Search something'}
            defaultValue={search}
            keyboardType={'default'}
            returnKeyType={'done'}
            blurOnSubmit={true}
            onChangeText={this.onInputChange('search')}
            fontSize={normalize(16)}
            inputStyle={[
              styles.input,
            ]}
            onSubmitEditing={() => this.onSerchPress()}
            placeholderTextColor="white"
            color="white"
          />
          <TouchableOpacity onPress={() => this.onSerchPress()} style={styles.serchIconView}>
            <Image source={require('../../assets/images/search_gray_image.png')} resizeMode="contain" />
          </TouchableOpacity>
        </View>

        {Object.entries(searchListData).length != 0 &&
          <View style={styles.tabView}>
            {/* {searchListData.users.filter(item => item.address != address).length != 0 && */}
            {searchListData.usersCount != 0 &&
              <TabView
                onTabPress={() => this.setState({ tabTitle: 'Users' })}
                title={'Users'}
                // count={searchListData.users.filter(item => item.address != address).length}
                count={searchListData.usersCount}
                titleStyle={{ fontWeight: '700', opacity: tabTitle == 'Users' ? 1 : 0.4 }}
              />
            }
            {searchListData.labelsCount != 0 &&
              <TabView
                onTabPress={() => this.setState({ tabTitle: 'Labels' })}
                title={'Labels'}
                count={searchListData.labelsCount}
                titleStyle={{ fontWeight: '700', opacity: tabTitle == 'Labels' ? 1 : 0.4 }}
              />
            }
            {searchListData.messagesCount != 0 &&
              <TabView
                onTabPress={() => this.setState({ tabTitle: 'Messages' })}
                title={'Messages'}
                count={searchListData.messagesCount}
                titleStyle={{ fontWeight: '700', opacity: tabTitle == 'Messages' ? 1 : 0.4 }}
              />
            }
          </View>
        }

        <SearchList searchListData={searchData} tabTitle={tabTitle} address={address} onItemPress={(item) => this.onItemPress(item)} navigation={navigation} />
        <Loader
          loading={processing}
        />
      </View>
    );
  }
}

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.black,
    paddingTop: isX ? hp(4) : hp(0),
    paddingHorizontal: wp(5),
  },
  searchView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderRadius: wp(4),
    paddingLeft: wp(5),
    paddingRight: wp(3),
    paddingVertical: hp(1.2),
    backgroundColor: colors[ 'gray-light' ]
  },
  input: {
    fontWeight: '400',
    width: wp(68),
    backgroundColor: colors.transparent,
    paddingLeft: 0,
    paddingTop: 0,
  },
  serchIconView: {
    width: wp(12),
    height: wp(12),
    borderRadius: wp(6),
    backgroundColor: colors[ 'text-dark' ],
    alignItems: 'center',
    justifyContent: 'center'
  },
  tabView: {
    flexDirection: 'row',
    marginVertical: hp(1.5),
  },
  tabChildView: {
    flexDirection: 'row',
    width: wp(25),
    marginRight: wp(3),
    paddingVertical: hp(1),
  }
});


const mapStateToProps = state => {
  const {
    addWallet: {
      processing,
      error,
      success,
      searchListData,
    },
    chat: {
      chatThreadData
    },
  } = state;
  return {
    processing,
    error,
    success,
    searchListData,
    chatThreadData
  };
}

const mapDispatchToProps = dispatch => bindActionCreators({

  searchWallet: walletActions.searchWallet,

}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Search);