import moment from "moment";

export function errorToMessage(e, defaultMsg = 'Unknown Error') {
  let errorMsg = typeof e === 'object' ? e.message : typeof e === 'string' ? e : defaultMsg;
  errorMsg = errorMsg.includes('Unable to resolve host') || errorMsg.includes('Network Error') ? 'The Internet connection appears to be offline.' : errorMsg;
  if (typeof errorMsg === 'object') errorMsg = errorMsg.message || defaultMsg;
  return errorMsg;
}


export function timeAgoFromNow(dateStrUTC) {
  let ago = moment.utc(dateStrUTC).local().fromNow();
  ago = ago.replace(/just now/, ' now');
  ago = ago.replace(/\sseconds|\ssecond/, ' second');
  ago = ago.replace(/a minute/, '1 minute');
  ago = ago.replace(/\sminutes|\sminute/, ' minutes');
  ago = ago.replace(/an hour/, '1 hour');
  ago = ago.replace(/\shours|\shour/, ' hour');
  ago = ago.replace(/a day/, '1 day');
  ago = ago.replace(/\sdays|\sday/, ' day');
  ago = ago.replace(/a month/, '1 months');
  ago = ago.replace(/\smonths|\smonth/, ' months');
  ago = ago.replace(/a year/, '1 year');
  ago = ago.replace(/\syears|\syear/, ' year');
  return ago;
}

export function time(dateStrUTC) {
  let date = moment.utc(dateStrUTC).local().format('YYYY-MM-DD');
  let time

  if (date == moment().format('YYYY-MM-DD')) {
    time = moment.utc(dateStrUTC).local().format('h:mm A')
  } else if (date == moment().subtract('days', 1).format('YYYY-MM-DD')) {
    time = 'Yesterday, ' + moment.utc(dateStrUTC).local().format('h:mm A')
  } else {
    time = date
  }

  return time;
}

export function validURL(str) {
  var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
    '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
  return !!pattern.test(str);
}

