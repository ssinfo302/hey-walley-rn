export function removeInArray(array = [], value, key = 'id') {

  const index = array.findIndex((item) => {
    if (typeof item === 'object') return item[ key ] === value;
    return item === value;
  });
  if (index > -1) return array.splice(index, 1);
  return [];

}