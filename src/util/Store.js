import * as React from 'react';
import AsyncStorage from '@react-native-community/async-storage';

let checkWalletData = [];
let addWalletData = [];

export async function storeCheckWalletData(data) {
    try {
        checkWalletData = JSON.parse(await AsyncStorage.getItem('check wallet data'));
        console.log('checkWalletData..............', checkWalletData)

        let existData
        if (checkWalletData == null) {
            existData = ''
            checkWalletData = []
        } else {
            existData = checkWalletData.find(x => x.address === data.address);
        }
        if (!existData) {
            checkWalletData.push(data);
        } else {
            if (!existData.token) {
                let index = checkWalletData.findIndex(x => x.address === data.address);
                console.log('index...', index)
                checkWalletData.splice(index, 1);
                checkWalletData.push(data);
            }
        }
        console.log('checkWalletData..............!!!!!!!', checkWalletData)
        AsyncStorage.setItem('check wallet data', JSON.stringify(checkWalletData));
        AsyncStorage.setItem('address', JSON.stringify(data.address));
        return checkWalletData;
    } catch (error) {
        console.error(error.message);
    }
}

export async function removeCheckWalletData(index) {
    try {
        checkWalletData = JSON.parse(await AsyncStorage.getItem('check wallet data'));
        checkWalletData.splice(index, 1);
        AsyncStorage.setItem('check wallet data', JSON.stringify(checkWalletData));
        return checkWalletData;
    } catch (error) {
        console.error(error.message);
    }
}

export async function storeAddWalletData(data) {
    try {
        addWalletData = JSON.parse(await AsyncStorage.getItem('add wallet data'));
        console.log('addWalletData..............', addWalletData)

        let existData
        if (addWalletData == null) {
            existData = ''
            addWalletData = []
        } else {
            existData = addWalletData.find(x => x.uid === data.uid);
        }
        if (!existData) {
            addWalletData.push(data);
        }
        console.log('addWalletData..............!!!!!!!!!', addWalletData)
        AsyncStorage.setItem('add wallet data', JSON.stringify(addWalletData));
        AsyncStorage.setItem('uid', JSON.stringify(data.uid));
        return addWalletData;
    } catch (error) {
        console.error(error.message);
    }
}

export async function removeAddWalletData(index) {
    try {
        addWalletData = JSON.parse(await AsyncStorage.getItem('add wallet data'));
        addWalletData.splice(index, 1);
        AsyncStorage.setItem('add wallet data', JSON.stringify(addWalletData));
        return addWalletData;
    } catch (error) {
        console.error(error.message);
    }
}