import { takeEvery, call, put } from 'redux-saga/effects';
import { chatActions } from '../actions/actions';
import { errorToMessage } from '../util/validation';
import * as chatActionType from '../actions/chatActionType';
import * as walletApi from '../api/wallet';


function* chatThread({ payload }) {
  const { request } = walletApi.chat_thread(payload);
  try {
    const { data } = yield call(request);
    console.log('data!!!!!', data)

    yield put(chatActions.chatThreadSuccess(data));

  } catch (e) {
    console.log('e....', e, errorToMessage(e))
    yield put(chatActions.chatThreadFailed({ message: errorToMessage(e) }));
  }
}

function* chat({ payload }) {
  const { request } = walletApi.get_chat(payload);
  try {
    const { data } = yield call(request);

    yield put(chatActions.chatSuccess(data));

  } catch (e) {
    yield put(chatActions.chatFailed({ message: errorToMessage(e) }));
  }
}

function* addLabels({ payload }) {
  const { request } = walletApi.add_labels(payload);
  try {
    const { data } = yield call(request);

    yield put(chatActions.addLabelsSuccess(data));

  } catch (e) {
    yield put(chatActions.addLabelsFailed({ message: errorToMessage(e) }));
  }
}

function* removeLabels({ payload }) {
  const { request } = walletApi.remove_labels(payload);
  try {
    const { data } = yield call(request);

    yield put(chatActions.removeLabelsSuccess(data));

  } catch (e) {
    yield put(chatActions.removeLabelsFailed({ message: errorToMessage(e) }));
  }
}

function* addNotes({ payload }) {
  const { request } = walletApi.add_notes(payload);
  try {
    const { data } = yield call(request);

    yield put(chatActions.addNotesSuccess(data));

  } catch (e) {
    yield put(chatActions.addNotesFailed({ message: errorToMessage(e) }));
  }
}

function* removeChat({ payload }) {
  const { request } = walletApi.remove_chat(payload);
  try {
    const { data } = yield call(request);

    yield put(chatActions.removeChatSuccess({ ...data, threadId: payload.threadId }));

  } catch (e) {
    yield put(chatActions.removeChatFailed({ message: errorToMessage(e) }));
  }
}

function* removeMessage({ payload }) {
  const { request } = walletApi.remove_message(payload);
  try {
    const { data } = yield call(request);
    console.log('data.........', data)

    yield put(chatActions.removeMessageSuccess({ ...data, messageId: payload.messageId }));

  } catch (e) {
    yield put(chatActions.removeMessageFailed({ message: errorToMessage(e) }));
  }
}

function* editMessage({ payload }) {
  const { request } = walletApi.edit_message(payload);
  try {
    const { data } = yield call(request);

    yield put(chatActions.editMessageSuccess({ ...data, messageId: payload.messageId }));

  } catch (e) {
    yield put(chatActions.editMessageFailed({ message: errorToMessage(e) }));
  }
}

function* imageSend({ payload }) {
  const { request } = walletApi.image_send(payload);
  try {
    const { data } = yield call(request);

    yield put(chatActions.imageSendSuccess(data));

  } catch (e) {
    yield put(chatActions.imageSendFailed({ message: errorToMessage(e) }));
  }
}

function* audioSend({ payload }) {
  const { request } = walletApi.audio_send(payload);
  try {
    const { data } = yield call(request);

    yield put(chatActions.audioSendSuccess(data));

  } catch (e) {
    yield put(chatActions.audioSendFailed({ message: errorToMessage(e) }));
  }
}

function* fileSend({ payload }) {
  const { request } = walletApi.file_send(payload);
  try {
    const { data } = yield call(request);

    yield put(chatActions.fileSendSuccess(data));

  } catch (e) {
    yield put(chatActions.fileSendFailed({ message: errorToMessage(e) }));
  }
}

function* getMedia({ payload }) {
  const { request } = walletApi.get_media(payload);
  try {
    const { data } = yield call(request);

    yield put(chatActions.getMediaSuccess(data));

  } catch (e) {
    yield put(chatActions.getMediaFailed({ message: errorToMessage(e) }));
  }
}

function* getFile({ payload }) {
  const { request } = walletApi.get_file(payload);
  try {
    const { data } = yield call(request);

    yield put(chatActions.getFileSuccess(data));

  } catch (e) {
    yield put(chatActions.getFileFailed({ message: errorToMessage(e) }));
  }
}

function* getAudio({ payload }) {
  const { request } = walletApi.get_audio(payload);
  try {
    const { data } = yield call(request);

    yield put(chatActions.getAudioSuccess(data));

  } catch (e) {
    yield put(chatActions.getAudioFailed({ message: errorToMessage(e) }));
  }
}

function* getLinks({ payload }) {
  const { request } = walletApi.get_links(payload);
  try {
    const { data } = yield call(request);

    yield put(chatActions.getLinksSuccess(data));

  } catch (e) {
    yield put(chatActions.getLinksFailed({ message: errorToMessage(e) }));
  }
}


function* chatSagas() {
  yield takeEvery(chatActionType.CHAT_THREAD, chatThread);
  yield takeEvery(chatActionType.CHAT, chat);
  yield takeEvery(chatActionType.ADD_LABELS, addLabels);
  yield takeEvery(chatActionType.REMOVE_LABELS, removeLabels);
  yield takeEvery(chatActionType.ADD_NOTES, addNotes);
  yield takeEvery(chatActionType.REMOVE_CHAT, removeChat);
  yield takeEvery(chatActionType.REMOVE_MESSAGE, removeMessage);
  yield takeEvery(chatActionType.EDIT_MESSAGE, editMessage);
  yield takeEvery(chatActionType.IMAGE_SEND, imageSend);
  yield takeEvery(chatActionType.AUDIO_SEND, audioSend);
  yield takeEvery(chatActionType.FILE_SEND, fileSend);
  yield takeEvery(chatActionType.GET_MEDIA, getMedia);
  yield takeEvery(chatActionType.GET_FILE, getFile);
  yield takeEvery(chatActionType.GET_AUDIO, getAudio);
  yield takeEvery(chatActionType.GET_LINKS, getLinks);

}

export default chatSagas;