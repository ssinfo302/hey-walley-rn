import { all } from 'redux-saga/effects';

import addWalletSagas from './addWallet';
import chatSagas from './chat';


export default function* rootSagas() {
  yield all([

    addWalletSagas(),
    chatSagas(),
  ]);
}