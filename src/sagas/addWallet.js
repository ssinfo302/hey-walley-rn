import { takeEvery, call, put } from 'redux-saga/effects';
import { walletActions } from '../actions/actions';
import { errorToMessage } from '../util/validation';
import * as walletActionType from '../actions/walletActionType';
import * as walletApi from '../api/wallet';


function* addWallet({ payload }) {
  const { request } = walletApi.add_wallet(payload);
  try {
    const { data } = yield call(request);
    console.log('data!!!!!', data)

    yield put(walletActions.addWalletSuccess(data));

  } catch (e) {
    console.log('e....', e, errorToMessage(e))
    yield put(walletActions.addWalletFailed({ message: errorToMessage(e) }));
  }
}

function* checkWallet({ payload }) {
  const { request } = walletApi.check_wallet(payload);
  try {
    const { data } = yield call(request);

    yield put(walletActions.checkWalletSuccess(data));

  } catch (e) {
    yield put(walletActions.checkWalletFailed({ message: errorToMessage(e) }));
  }
}

function* searchWallet({ payload }) {
  const { request } = walletApi.search_wallet(payload);
  try {
    const { data } = yield call(request);

    yield put(walletActions.searchWalletSuccess(data));

  } catch (e) {
    yield put(walletActions.searchWalletFailed({ message: errorToMessage(e) }));
  }
}

function* removeWallet({ payload }) {
  const { request } = walletApi.remove_wallet(payload);
  try {
    const { data } = yield call(request);
    // Object.assign(data, { address: payload.address });
    yield put(walletActions.removeWalletSuccess(data));

  } catch (e) {
    yield put(walletActions.removeWalletFailed({ message: errorToMessage(e) }));
  }
}

function* searchDapp({ payload }) {
  const { request } = walletApi.search_dapp(payload);
  try {
    const { data } = yield call(request);

    yield put(walletActions.searchDappSuccess(data));

  } catch (e) {
    yield put(walletActions.searchDappFailed({ message: errorToMessage(e) }));
  }
}

function* subscribeDapp({ payload }) {
  const { request } = walletApi.subscribe_dapp(payload);
  try {
    const { data } = yield call(request);

    Object.assign(data, { dappId: payload.dappId, type: payload.type, walletId: payload.walletId });

    yield put(walletActions.subscribeDappSuccess(data));

  } catch (e) {
    yield put(walletActions.subscribeDappFailed({ message: errorToMessage(e) }));
  }
}

function* dappList({ payload }) {
  const { request } = walletApi.dapp_list(payload);

  try {
    const { data } = yield call(request);

    yield put(walletActions.dappListSuccess(data));

  } catch (e) {
    yield put(walletActions.dappListFailed({ message: errorToMessage(e) }));
  }
}

function* addWalletSagas() {
  yield takeEvery(walletActionType.ADD_WALLET, addWallet);
  yield takeEvery(walletActionType.CHECK_WALLET, checkWallet);
  yield takeEvery(walletActionType.SEARCH_WALLET, searchWallet);
  yield takeEvery(walletActionType.REMOVE_WALLET, removeWallet);
  yield takeEvery(walletActionType.SEARCH_DAPP, searchDapp);
  yield takeEvery(walletActionType.SUBSCRIBE_DAPP, subscribeDapp);
  yield takeEvery(walletActionType.DAPP_LIST, dappList);
}

export default addWalletSagas;