import { combineReducers } from 'redux';
import addWalletReducer from './addWallet';
import chatReducer from './chat';

export default combineReducers({

  addWallet: addWalletReducer,
  chat: chatReducer,
})