import { handleActions } from 'redux-actions';
import * as walletActionType from '../actions/walletActionType';
import { removeCheckWalletData, removeAddWalletData } from '../util/Store';
import AsyncStorage from '@react-native-community/async-storage';

const initialState = {
  address: '',
  chain: '',
  processing: false,
  error: null,
  success: false,
  data: '',
  wallet: '',
  checkWalletSuccess: false,
  searchListData: '',
  removeData: '',
  searchDappData: null,
  searchDappLoading: false,
  subscribeData: null,
};

const addWalletReducer = handleActions(
  {
    [ walletActionType.SET_ADD_WALLET_PARAMS ](state, { payload }) {
      return { ...state, ...payload };
    },
    [ walletActionType.ADD_WALLET ](state, { payload }) {
      return { ...state, processing: true };
    },
    [ walletActionType.ADD_WALLET_SUCCESS ](state, { payload }) {
      return { ...state, processing: false, success: true, data: payload };
    },
    [ walletActionType.ADD_WALLET_FAILED ](state, { payload }) {
      return { ...state, processing: false, error: payload };
    },
    [ walletActionType.SET_CHECK_WALLET_PARAMS ](state, { payload }) {
      return { ...state, ...payload };
    },
    [ walletActionType.CHECK_WALLET ](state, { payload }) {
      return { ...state, processing: true };
    },
    [ walletActionType.CHECK_WALLET_SUCCESS ](state, { payload }) {
      return { ...state, processing: false, checkWalletSuccess: true, wallet: payload };
    },
    [ walletActionType.CHECK_WALLET_FAILED ](state, { payload }) {
      return { ...state, processing: false, error: payload };
    },
    [ walletActionType.SET_SEARCH_WALLET_PARAMS ](state, { payload }) {
      return { ...state, ...payload };
    },
    [ walletActionType.SEARCH_WALLET ](state, { payload }) {
      return { ...state, processing: true };
    },
    [ walletActionType.SEARCH_WALLET_SUCCESS ](state, { payload }) {
      return { ...state, processing: false, success: true, searchListData: payload };
    },
    [ walletActionType.SEARCH_WALLET_FAILED ](state, { payload }) {
      return { ...state, processing: false, error: payload };
    },
    [ walletActionType.SET_REMOVE_WALLET_PARAMS ](state, { payload }) {
      return { ...state, ...payload };
    },
    [ walletActionType.REMOVE_WALLET ](state, { payload }) {
      return { ...state, processing: true };
    },
    [ walletActionType.REMOVE_WALLET_SUCCESS ](state, { payload }) {
      // (async () => {
      //   console.log('payload remove....', payload)
      //   let walletData = JSON.parse(await AsyncStorage.getItem('check wallet data'));
      //   console.log('walletData....', walletData)
      //   const index = walletData.findIndex(item => item.address === payload.address);
      //   console.log('index....', index, payload)
      //   removeCheckWalletData(index)
      //   removeAddWalletData(index)
      // })();

      return { ...state, processing: false, success: true, removeData: payload };
    },
    [ walletActionType.REMOVE_WALLET_FAILED ](state, { payload }) {
      return { ...state, processing: false, error: payload };
    },
    [ walletActionType.SET_SEARCH_DAPP_PARAMS ](state, { payload }) {
      return { ...state, ...payload };
    },
    [ walletActionType.SEARCH_DAPP ](state, { payload }) {
      return { ...state, searchDappLoading: true };
    },
    [ walletActionType.SEARCH_DAPP_SUCCESS ](state, { payload }) {
      return { ...state, searchDappLoading: false, success: true, searchDappData: payload };
    },
    [ walletActionType.SEARCH_DAPP_FAILED ](state, { payload }) {
      return { ...state, searchDappLoading: false, error: payload };
    },
    [ walletActionType.SET_SUBSCRIBE_DAPP_PARAMS ](state, { payload }) {
      return { ...state, ...payload };
    },
    [ walletActionType.SUBSCRIBE_DAPP ](state, { payload }) {
      return { ...state, processing: true };
    },
    [ walletActionType.SUBSCRIBE_DAPP_SUCCESS ](state, { payload }) {
      let { searchDappData } = state;
      if (searchDappData) {
        let index = searchDappData.data.findIndex(x => x._id === payload.dappId)

        if (payload.type == 'SUBSCRIBE') {
          searchDappData.data[ index ].subscribeDapp.push(payload.walletId);
        } else {
          const specificIndex = searchDappData.data[ index ].subscribeDapp.indexOf(payload.walletId);
          if (specificIndex > -1) {
            searchDappData.data[ index ].subscribeDapp.splice(specificIndex, 1);
          }
        }
      }

      return { ...state, processing: false, success: true, subscribeData: payload };
    },
    [ walletActionType.SUBSCRIBE_DAPP_FAILED ](state, { payload }) {
      return { ...state, processing: false, error: payload };
    },
    [ walletActionType.SET_DAPP_LIST_PARAMS ](state, { payload }) {
      return { ...state, ...payload };
    },
    [ walletActionType.DAPP_LIST ](state, { payload }) {
      return { ...state, searchDappLoading: true };
    },
    [ walletActionType.DAPP_LIST_SUCCESS ](state, { payload }) {
      return { ...state, searchDappLoading: false, success: true, searchDappData: payload };
    },
    [ walletActionType.DAPP_LIST_FAILED ](state, { payload }) {
      return { ...state, searchDappLoading: false, error: payload };
    },
  },
  initialState,
);

export default addWalletReducer;
