import { handleActions } from 'redux-actions';
import * as chatActionType from '../actions/chatActionType';
import * as walletActionType from '../actions/walletActionType';
import { removeInArray } from '../util/array'
const initialState = {
  processing: false,
  error: null,
  success: false,
  chatThreadData: '',
  chatData: '',
  addLabelData: '',
  addLabelSuccess: false,
  removeLabelData: '',
  addNotesData: '',
  chatThreadProcessing: false,
  removeChatData: '',
  iseditSuccess: false,
  imageSendData: '',
  audioData: '',
  fileData: '',
  getMediaData: '',
  getFileData: '',
  getAudioData: '',
  getLinksData: '',
  loading: false,
  subscribeProcessing: false,
};

const chatReducer = handleActions(
  {
    [ chatActionType.SET_CHAT_THREAD_PARAMS ](state, { payload }) {
      return { ...state, ...payload };
    },
    [ chatActionType.CHAT_THREAD ](state, { payload }) {
      return { ...state, chatThreadProcessing: true };
    },
    [ chatActionType.CHAT_THREAD_SUCCESS ](state, { payload }) {
      return { ...state, chatThreadProcessing: false, success: true, chatThreadData: payload };
    },
    [ chatActionType.CHAT_THREAD_FAILED ](state, { payload }) {
      return { ...state, chatThreadProcessing: false, error: payload };
    },
    [ chatActionType.SET_CHAT_PARAMS ](state, { payload }) {
      return { ...state, ...payload };
    },
    [ chatActionType.CHAT ](state, { payload }) {
      return { ...state, processing: true };
    },
    [ chatActionType.CHAT_SUCCESS ](state, { payload }) {
      return { ...state, processing: false, success: true, chatData: payload };
    },
    [ chatActionType.CHAT_FAILED ](state, { payload }) {
      return { ...state, processing: false, error: payload };
    },
    [ chatActionType.SET_ADD_LABELS_PARAMS ](state, { payload }) {
      return { ...state, ...payload };
    },
    [ chatActionType.ADD_LABELS ](state, { payload }) {
      return { ...state, processing: true };
    },
    [ chatActionType.ADD_LABELS_SUCCESS ](state, { payload }) {
      return { ...state, processing: false, addLabelSuccess: true, addLabelData: payload };
    },
    [ chatActionType.ADD_LABELS_FAILED ](state, { payload }) {
      return { ...state, processing: false, error: payload };
    },
    [ chatActionType.SET_REMOVE_LABELS_PARAMS ](state, { payload }) {
      return { ...state, ...payload };
    },
    [ chatActionType.REMOVE_LABELS ](state, { payload }) {
      return { ...state, processing: true };
    },
    [ chatActionType.REMOVE_LABELS_SUCCESS ](state, { payload }) {
      return { ...state, processing: false, success: true, removeLabelData: payload };
    },
    [ chatActionType.REMOVE_LABELS_FAILED ](state, { payload }) {
      return { ...state, processing: false, error: payload };
    },
    [ chatActionType.SET_ADD_NOTES_PARAMS ](state, { payload }) {
      return { ...state, ...payload };
    },
    [ chatActionType.ADD_NOTES ](state, { payload }) {
      return { ...state, processing: true };
    },
    [ chatActionType.ADD_NOTES_SUCCESS ](state, { payload }) {
      return { ...state, processing: false, success: true, addNotesData: payload };
    },
    [ chatActionType.ADD_NOTES_FAILED ](state, { payload }) {
      return { ...state, processing: false, error: payload };
    },
    [ chatActionType.REMOVE_CHAT ](state, { payload }) {
      return { ...state, chatThreadProcessing: true };
    },
    [ chatActionType.REMOVE_CHAT_SUCCESS ](state, { payload }) {
      const { threadId, data } = payload;
      let { chatThreadData } = state;
      if (threadId) {
        removeInArray(chatThreadData.data, threadId, 'threadId')
        console.log('chatthread data...', chatThreadData)
      }
      return { ...state, chatThreadProcessing: false, success: true, chatThreadData };
    },
    [ chatActionType.REMOVE_CHAT_FAILED ](state, { payload }) {
      return { ...state, chatThreadProcessing: false, error: payload };
    },
    [ chatActionType.REMOVE_MESSAGE ](state, { payload }) {
      return { ...state, processing: true };
    },
    [ chatActionType.REMOVE_MESSAGE_SUCCESS ](state, { payload }) {
      const { messageId, data } = payload;
      let { chatData } = state;
      if (messageId) {
        removeInArray(chatData.data, messageId, '_id')
        console.log('chat data...', chatData)
      }
      return { ...state, processing: false, success: true, chatData };
    },
    [ chatActionType.REMOVE_MESSAGE_FAILED ](state, { payload }) {
      return { ...state, processing: false, error: payload };
    },
    [ chatActionType.EDIT_MESSAGE ](state, { payload }) {
      return { ...state, processing: true, iseditSuccess: false };
    },
    [ chatActionType.EDIT_MESSAGE_SUCCESS ](state, { payload }) {
      const { messageId, text } = payload;
      let { chatData } = state;
      if (messageId) {
        const index = chatData.data.findIndex((item) => {
          if (typeof item === 'object') return item[ '_id' ] === messageId;
          return item === messageId;
        });

        if (index != -1) {
          chatData.data[ index ].text = text
        }
      }
      console.log("payload data....", payload)
      return { ...state, processing: false, iseditSuccess: true, success: true, chatData };
    },
    [ chatActionType.EDIT_MESSAGE_FAILED ](state, { payload }) {
      return { ...state, processing: false, iseditSuccess: false, error: payload };
    },
    [ chatActionType.SET_IMAGE_SEND_PARAMS ](state, { payload }) {
      return { ...state, ...payload };
    },
    [ chatActionType.IMAGE_SEND ](state, { payload }) {
      return { ...state, processing: true };
    },
    [ chatActionType.IMAGE_SEND_SUCCESS ](state, { payload }) {
      return { ...state, processing: false, success: true, imageSendData: payload };
    },
    [ chatActionType.IMAGE_SEND_FAILED ](state, { payload }) {
      return { ...state, processing: false, error: payload };
    },
    [ chatActionType.SET_AUDIO_SEND_PARAMS ](state, { payload }) {
      return { ...state, ...payload };
    },
    [ chatActionType.AUDIO_SEND ](state, { payload }) {
      return { ...state, processing: true };
    },
    [ chatActionType.AUDIO_SEND_SUCCESS ](state, { payload }) {
      return { ...state, processing: false, success: true, audioData: payload };
    },
    [ chatActionType.AUDIO_SEND_FAILED ](state, { payload }) {
      return { ...state, processing: false, error: payload };
    },
    [ chatActionType.SET_FILE_SEND_PARAMS ](state, { payload }) {
      return { ...state, ...payload };
    },
    [ chatActionType.FILE_SEND ](state, { payload }) {
      return { ...state, processing: true };
    },
    [ chatActionType.FILE_SEND_SUCCESS ](state, { payload }) {
      return { ...state, processing: false, success: true, fileData: payload };
    },
    [ chatActionType.FILE_SEND_FAILED ](state, { payload }) {
      return { ...state, processing: false, error: payload };
    },
    [ chatActionType.SET_GET_MEDIA_PARAMS ](state, { payload }) {
      return { ...state, ...payload };
    },
    [ chatActionType.GET_MEDIA ](state, { payload }) {
      return { ...state, loading: true };
    },
    [ chatActionType.GET_MEDIA_SUCCESS ](state, { payload }) {
      return { ...state, loading: false, success: true, getMediaData: payload };
    },
    [ chatActionType.GET_MEDIA_FAILED ](state, { payload }) {
      return { ...state, loading: false, error: payload };
    },
    [ chatActionType.SET_GET_FILE_PARAMS ](state, { payload }) {
      return { ...state, ...payload };
    },
    [ chatActionType.GET_FILE ](state, { payload }) {
      return { ...state, loading: true };
    },
    [ chatActionType.GET_FILE_SUCCESS ](state, { payload }) {
      return { ...state, loading: false, success: true, getFileData: payload };
    },
    [ chatActionType.GET_FILE_FAILED ](state, { payload }) {
      return { ...state, loading: false, error: payload };
    },
    [ chatActionType.SET_GET_AUDIO_PARAMS ](state, { payload }) {
      return { ...state, ...payload };
    },
    [ chatActionType.GET_AUDIO ](state, { payload }) {
      return { ...state, loading: true };
    },
    [ chatActionType.GET_AUDIO_SUCCESS ](state, { payload }) {
      return { ...state, loading: false, success: true, getAudioData: payload };
    },
    [ chatActionType.GET_AUDIO_FAILED ](state, { payload }) {
      return { ...state, loading: false, error: payload };
    },
    [ chatActionType.SET_GET_LINKS_PARAMS ](state, { payload }) {
      return { ...state, ...payload };
    },
    [ chatActionType.GET_LINKS ](state, { payload }) {
      return { ...state, loading: true };
    },
    [ chatActionType.GET_LINKS_SUCCESS ](state, { payload }) {
      return { ...state, loading: false, success: true, getLinksData: payload };
    },
    [ chatActionType.GET_LINKS_FAILED ](state, { payload }) {
      return { ...state, loading: false, error: payload };
    },
    [ walletActionType.SET_SUBSCRIBE_DAPP_PARAMS ](state, { payload }) {
      return { ...state, ...payload };
    },
    [ walletActionType.SUBSCRIBE_DAPP ](state, { payload }) {
      return { ...state, subscribeProcessing: true };
    },
    [ walletActionType.SUBSCRIBE_DAPP_SUCCESS ](state, { payload }) {
      let { chatThreadData } = state;

      if (chatThreadData) {
        let participantsData = chatThreadData.data.find(x => x.participants.find(x => x._id === payload.dappId))?.participants.find(x => x._id === payload.dappId)

        if (participantsData) {
          if (payload.type == 'SUBSCRIBE') {
            participantsData.subscribeDapp.push(payload.walletId);
          } else {
            const specificIndex = participantsData.subscribeDapp.indexOf(payload.walletId);
            if (specificIndex > -1) {
              participantsData.subscribeDapp.splice(specificIndex, 1);
            }
          }
        }

      }

      return { ...state, subscribeProcessing: false, success: true };
    },
    [ walletActionType.SUBSCRIBE_DAPP_FAILED ](state, { payload }) {
      return { ...state, subscribeProcessing: false, error: payload };
    },
  },
  initialState,
);

export default chatReducer;
